import setuptools

from distutils.command.install import INSTALL_SCHEMES
for scheme in INSTALL_SCHEMES.values():
    scheme['data'] = scheme['purelib']

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setuptools.setup(
    name="pglx",
    version="4.7.56.110-4",
    author="Sydney Gems",
    author_email="sydney.gems@openmailbox.org",
    description="Small ERP for CS (Centre de Secours)",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/daufinsyd/pglx",
    install_requires=requirements,
    packages=setuptools.find_packages(),
    package_data={'pglx': ['data/*.txt']},
    include_package_data=True,
    entry_points={
        'gui_scripts': [
            'pglx = pglx.__main__:main'
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX :: Linux",
    ],
)
