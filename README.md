Website : https://sydney.gems.technology/main/work.php


# Screenshots

Main screen 

![main](/uploads/59beeeb7878f3f6668b96945ed2412e2/main.png)


Administration

![admin_blured](/uploads/1b698c2ad4e086e22f0237d4d1171371/admin_blured.png)


Edit/create new Intervention

![edit_inter_blured](/uploads/da4190eaabaac312adfe05f1f573c89a/edit_inter_blured.png)


List past interventions

![list_inter](/uploads/d476fe7c1138155fc925c82a70b401b8/list_inter.png)


List firemen

![staff_blured](/uploads/5fdf121f6dfd6c3bf9703857b6670870/staff_blured.png)


Edit firemen's profile

![profile_blured](/uploads/43b088949c161504e710090da8fe0fcb/profile_blured.png)

