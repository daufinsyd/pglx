# Plots a radar chart.

from math import pi
import matplotlib.pyplot as plt
import pglx.models as models
from pglx.pglx_qt import TYPE_INTER, TYPE_FMA


# Set data
def get_data_poly_vehicles():
	data_poly = []
	for i in models.VehicleVL.select():
		cat.append(i.toString())
	for i in models.VehicleVTU.select():
		cat.append(i.toString())
	for i in models.VehicleFPT.select():
		cat.append(i.toString())
	return data_poly

def get_data_poly_inter_type():
    return TYPE_INTER

def get_data_poly_fma_type():
    return TYPE_FMA


def get_data_inter_type(year, types_i=[0,1,2,3,4,5]):
    # ensure that typs_i is ordred !
    types_i = sorted(types_i)
    #inters = models.Interventions.filter(models.Interventions.typeInter << types_i, date_appel.year == year)
    data = [0] * len(TYPE_INTER)
    total = 0
    for t in types_i:
        data[t] = models.Interventions.filter(models.Interventions.typeInter == t, models.Interventions.date_appel.year == year).count() * 10
        total += data[t]

    data = list(map(lambda x: x/total *100, data)) 

    return data

def display_radar_char(cat, values):
    N = len(cat)

    x_as = [n / float(N) * 2 * pi for n in range(N)]

    # Because our chart will be circular we need to append a copy of the first 
    # value of each list at the end of each list with data
    values += values[:1]
    x_as += x_as[:1]


    # Set color of axes
    plt.rc('axes', linewidth=0.5, edgecolor="#888888")


    # Create polar plot
    ax = plt.subplot(111, polar=True)


    # Set clockwise rotation. That is:
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)


    # Set position of y-labels
    ax.set_rlabel_position(0)


    # Set color and linestyle of grid
    ax.xaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.5)
    ax.yaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.5)


    # Set number of radial axes and remove labels
    plt.xticks(x_as[:-1], [])

    # Set yticks
    plt.yticks([20, 40, 60, 80, 100], ["20", "40", "60", "80", "100"])


    # Plot data
    ax.plot(x_as, values, linewidth=3, linestyle='solid', zorder=3)

    # Fill area
    ax.fill(x_as, values, 'b', alpha=0.3)


    # Set axes limits
    plt.ylim(0, 100)


    # Draw ytick labels to make sure they fit properly
    for i in range(N):
        angle_rad = i / float(N) * 2 * pi

        if angle_rad == 0:
            ha, distance_ax = "center", 10
        elif 0 < angle_rad < pi:
            ha, distance_ax = "left", 1
        elif angle_rad == pi:
            ha, distance_ax = "center", 1
        else:
            ha, distance_ax = "right", 1

        ax.text(angle_rad, 100 + distance_ax, cat[i], size=10, horizontalalignment=ha, verticalalignment="center")


    # Show polar plot
    plt.show()


cat = get_data_poly_inter_type() 
values = get_data_inter_type(2019)
display_radar_char(cat, values)


