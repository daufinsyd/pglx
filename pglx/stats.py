import datetime
from pglx.models import *


def get_fma(start_year, nb_years):
    """

    :param start_year: first year
    :param nb_years: >= 1 nb of year to search for
    :return: dict with total stats values for the year
    """

    # since nb_years >= 1 use < instead of <=
    fma = Fma.filter(Fma.dateDebut.year >= start_year, Fma.dateDebut.year < start_year + nb_years)

    total_centre = datetime.timedelta(0)
    total_spv = datetime.timedelta(0)
    for i in fma:
        total_centre += i.dateFin - i.dateDebut
        total_spv += (i.dateFin - i.dateDebut) * (i.get_number_spvs())

    values = {'total_centre': {'value': total_centre, 'name': "Total (Centre)", 'subvalue': False},
              'total': {'value': total_spv, 'name': "Total (Réel)", 'subvalue': False}
              }

    return values


def get_inter(start_year, nb_years):
    """

    :param start_year: first year
    :param nb_years: >= 1 nb of year to search for
    :return: dict with total stats values for the year
    """

    # since nb_years >= 1 use < instead of <=
    inters = Interventions.filter(Interventions.date_appel.year >= start_year, Interventions.date_appel.year < start_year + nb_years)

    total_std = datetime.timedelta(0)
    for i in inters:
        total_std += i.date_fin - i.date_appel

    total_vl = datetime.timedelta(0)
    for i in inters:
        cpt = 0
        for vl in i.VL_inter:
            for attr in ["con", "ca", "equ", "stag"]:
                if getattr(vl, attr).id != 1:
                    cpt += 1
            total_vl += cpt * (i.date_fin - i.date_appel)

    total_vtu = datetime.timedelta(0)
    for i in inters:
        cpt = 0
        for vtu in i.VTU_inter:
            for attr in ["con", "ca", "equ", "stag"]:
                if getattr(vtu, attr).id != 1:
                    cpt += 1
            total_vtu += cpt * (i.date_fin - i.date_appel)

    total_fpt = datetime.timedelta(0)
    for i in inters:
        cpt = 0
        for fpt in i.FPTL_inter:
            for attr in ["con", "ca", "ce", "equ", "ce2", "equ2", "stag"]:
                if getattr(fpt, attr).id != 1:
                    cpt += 1
            total_fpt += cpt * (i.date_fin - i.date_appel)

    total = total_vl + total_vtu + total_fpt
    values = {'total_std': {'value': total_std, 'name': "Total (Centre)", 'subvalue': False},
              'total': {'value': total, 'name': "Total (Réel)", 'subvalue': False},
              'vehicles': {'value': {
                  'total_vl': {'value': total_vl, 'name': "Total VL"},
                  'total_vtu': {'value': total_vtu, 'name': "Total VTU"},
                  'total_fpt': {'value': total_fpt, 'name': "Total FPT"}
              }, 'name': "Par véhicule", 'subvalue': True}
              }

    return values
