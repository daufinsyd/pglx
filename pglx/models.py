#!/usr/bin/python3
#PGLX
from peewee import *
import os
import bcrypt
import logging

logger = logging.getLogger(__name__)

from PyQt5.QtCore import QDate

from pglx import DB_VERSION

path_to_pglx = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..") #TODO bug in python itself ?
db = SqliteDatabase(os.path.join(path_to_pglx, 'pglx', 'db.sqlite'))
user_db = SqliteDatabase(os.path.join(path_to_pglx, 'pglx', 'user_db.sqlite'))


def create_tables():
    if not (BDMeta.table_exists() and BDUserMeta.table_exists() and VL.table_exists() and VTU.table_exists() and FPTL.table_exists() and Renforts.table_exists() and PremierDepart.table_exists() and Interventions.table_exists() and Fma.table_exists() and FmaPersonnel.table_exists() and
                      StatsFmaPersonnel.table_exists() and StatsIntersPersonnel.table_exists() and Caserne.table_exists() and Commune.table_exists() and VehicleFPT.table_exists() and VehicleVL.table_exists() and VehicleVTU):
        db.create_tables([BDMeta, BDUserMeta, VL, VTU, FPTL, Renforts, PremierDepart, Interventions, Fma, FmaPersonnel,
                      StatsFmaPersonnel, StatsIntersPersonnel, Caserne, Commune, VehicleFPT, VehicleVL, VehicleVTU], safe=True)
    if not (Personnel.table_exists() and UserPreferences.table_exists()):
        user_db.create_tables([Personnel, UserPreferences], safe=True)

    if Personnel.select().count() == 0:  # crée l'utilisateur nobody et admin si la base est vide
        logger.info("[DB] Create nobody and admin users")
        hash = bcrypt.hashpw("".encode(), bcrypt.gensalt())  # hash
        spv = Personnel.create(grade=-1, matricule=-1, type=-1, firstName='Aucun', lastName='Aucun', COD1=True, COD2=True,
                         VL=True, password=hash, rank=0, matriculeCS=-2)
        UserPreferences.create(user=spv)

        hash = bcrypt.hashpw("a".encode(), bcrypt.gensalt())  # hash
        spv_admin = Personnel.create(grade=-1, matricule=-2, type=-1, firstName='Administrateur', lastName='', COD1=False, COD2=False,
                         VL=False, password=hash, rank=3, matriculeCS=-1)
        UserPreferences.create(user=spv)

    if Caserne.select().count() == 0:
        logger.info("[DB] Create Caserne")
        """
        Per default prevent allowing Caserne user to bypass perms beeing the default chef or admin: ensure admin is both
        """
        Caserne.create(name="Caserne", type=0, passwd1="passwd1", pathInter=os.getcwd() + '/Inter',
                       pathFma=os.getcwd() + 'FMA', pathCasernement=os.getcwd() + 'Casernement', chef=spv_admin,
                       adjoint=spv_admin)

    if not BDMeta.select().count():
        logger.info("[DB] Create BDMeta")
        BDMeta.create()

    if not BDUserMeta.select().count():
        logger.info("[DB] Create BDUserMeta")
        BDUserMeta.create()


class BDMeta(Model):
    version = IntegerField(default=DB_VERSION)

    class Meta:
        database = db  # This model uses the "people.db" database.


class BDUserMeta(Model):
    version = IntegerField(default=DB_VERSION)

    class Meta:
        database = user_db  # This model uses the "people.db" database.


class Personnel(Model):
    grade = IntegerField()
    """
    cf pglx_qt.py GRADES{}
    """
    matricule = IntegerField()
    type = IntegerField()
    """
    0 actif
    1 vétérant
    2 administratif
    3 mutation
    4 démission
    """
    firstName = CharField(max_length=30)
    lastName = CharField(max_length=30)
    COD1 = BooleanField(default=False)
    COD2 = BooleanField(default=False)
    VL = BooleanField(default=False)
    involvementDate = DateTimeField(null=True)
    birthDate = DateTimeField(null=True)

    rank = IntegerField(default=1)
    """
    -1 ~ ne peut rien faire
     0 peut voir rapports,
     1 peut écrire rapports,
     2 peut éditer diverses choses (lesquelles ?) (modérateur),
     3 administrateur
    """

    password = CharField() # UTILSIATEUR (devrait être dans une classe à part)
    heavyVehicleLicenceDate = DateTimeField(null=True)
    matriculeCS = IntegerField(unique=True)
    num_bip = CharField(max_length=16, null=True)

    """
    save: should the object be saved immediatly after setting the new date ?
    """
    def set_involvement_date(self, qdate, save=False):
        self.involvementDate = qdate.toString("yyyy-M-d")
        if save:
            self.save()

    def get_involvement_date(self):
        date = None
        if self.involvementDate:
            date = QDate(self.involvementDate.year, self.involvementDate.month, self.involvementDate.day)
        return date

    """
    save: should the object be saved immediatly after setting the new date ?
    """
    def set_heavy_vehicle_licence_date(self, qdate, save=False):
        self.heavyVehicleLicenceDate = qdate.toString("yyyy-M-d")
        if save:
            self.save()

    def get_heavy_vehicle_licence_date(self):
        date = None
        if self.heavyVehicleLicenceDate:
            date = QDate(self.heavyVehicleLicenceDate.year, self.heavyVehicleLicenceDate.month, self.heavyVehicleLicenceDate.day)
        return date

    def toString(self):
        return (self.firstName.title() + " " + self.lastName.upper())

    class Meta:
        database = user_db  # This model uses the "people.db" database.


class UserPreferences(Model):
    """
    Store preferences of the logged in user
    """
    user = ForeignKeyField(Personnel, related_name="UserPreferences_User", null=False)
    vehicleManagementShowAll = BooleanField(default=False)

    class Meta:
        database = user_db


class Renforts(Model):
    cff = IntegerField(null=True)
    epa = IntegerField(null=True)
    epsa = IntegerField(null=True)
    fpt = IntegerField(null=True)
    fptl = IntegerField(null=True)
    fptsr = IntegerField(null=True)
    vl = IntegerField(null=True)
    vlhr = IntegerField(null=True)
    vpi = IntegerField(null=True)
    vsav = IntegerField(null=True)
    vsr = IntegerField(null=True)
    vtu = IntegerField(null=True)
    smur = IntegerField(null=True)
    heliSmur = IntegerField(null=True)
    ccf = IntegerField(null=True)
    vpb = IntegerField(null=True)

    def alone(self):
        """
        Return true if there is no vehicle in it
        :return: is there renfort for the current inter
        """
        logger.debug("Check whether RENFORTS is alone")
        alone = True
        for attr, value in vars(self).items():
            """
            vars(a) > give dic with all iterable items
            _data is the dic of all attributes
            items() transform in a iterable dic of two elements (attr and its value)
            """
            if attr != "id" and value:
                alone = False
        logger.debug("Checked :", alone)
        return alone 

    class Meta:
        database = db  # This model uses the "people.db" database.


class PremierDepart(Model):
    cff = IntegerField(null=True)
    epa = IntegerField(null=True)
    epsa = IntegerField(null=True)
    fpt = IntegerField(null=True)
    fptl = IntegerField(null=True)
    fptsr = IntegerField(null=True)
    vl = IntegerField(null=True)
    vlhr = IntegerField(null=True)
    vpi = IntegerField(null=True)
    vsav = IntegerField(null=True)
    vsr = IntegerField(null=True)
    vtu = IntegerField(null=True)
    smur = IntegerField(null=True)
    heliSmur = IntegerField(null=True)
    ccf = IntegerField(null=True)
    vpb = IntegerField(null=True)

    def alone(self):
        """
        Return true if there is no vehicle in it
        :return: is there renfort for the current inter
        """
        logger.debug("Check whether PREMIER DEPART is alone")
        alone = True
        for attr, value in vars(self).items():
            """
            vars(a) > give dic with all iterable items
            _data is the dic of all attributes
            items() transform in a iterable dic of two elements (attr and its value)
            """
            if attr != "id" and value:
                alone = False
        logger.debug("Checked :", alone)
        return alone

    class Meta:
        database = db # This model uses the "people.db" database.


class Commune(Model):
    name = CharField(max_length=50)
    codeCommune = IntegerField()

    def commonName(self):
        """
        Return string to display
        """
        return self.name + " (" + str(self.codeCommune) + ")"

    class Meta:
        database = db


class Interventions(Model):
    name = CharField(max_length=200, null=True)
    nInter = IntegerField(default=1)
    ncodis = IntegerField()
    typeInter = IntegerField()
    natureInter = CharField(max_length=160)
    localisation = CharField(max_length=220)
    demandeur = CharField(max_length=120)
    date_appel = DateTimeField()
    date_depart = DateTimeField()
    date_fin = DateTimeField()
    gendarmerie = BooleanField(default=False)
    erdf = BooleanField(default=False)
    grdf = BooleanField(default=False)
    brigadeVerte = BooleanField(default=False)
    servicesEaux = BooleanField(default=False)
    maire = BooleanField(default=False)
    renforts = ForeignKeyField(Renforts, related_name='Intervention_renforts', unique=True)
    premierDepart = ForeignKeyField(PremierDepart, related_name='Intervention_premierDepart', unique=True)
    rapport = TextField()
    #spvCaserne et spvSll sont supprimés depuis la version 4.1.9
    spvCaserne = CharField(max_length=50, null=True)
    spvSll = CharField(max_length=50, null=True)

    dateRedaction = DateTimeField()
    redacteur = ForeignKeyField(Personnel, related_name='Intervention_redacteur', null=True)
    commune = ForeignKeyField(Commune, related_name='Intervention_commune', null=True)

    class Meta:
        database = db  # This model uses the "people.db" database.

    @staticmethod
    def get_first_year():
        """

        :return: year of the older intervention
        """
        return Interventions.select().order_by(Interventions.date_appel)[0].date_appel.year


class Fma(Model):
    # todo: ajouter nbFormanteurs, nbSpv, nbVehicules
    # pas obligatoirement, on peut utilsier autresFormateurs, ...
    name = CharField(max_length=200, null=True)
    nFma = IntegerField()
    type = IntegerField(null=False)
    dateDebut = DateTimeField()
    dateFin = DateTimeField()
    dateRedaction = DateTimeField()
    theme = CharField(300)
    name = TextField()
    rapport = TextField()
    redacteur = ForeignKeyField(Personnel, related_name='Fma_redacteur', null=True)
    # Afin de faciliter l'ajout ultérieure de caractéristiques toute les données formateur, formés et véhicules sont
    #  stockées en ligne texte séparée par des ';'
    # nom;formation
    autresFormateurs = TextField()
    autresSpvFormes = TextField()
    lieu = TextField()
    vehicules = TextField()
    commune = ForeignKeyField(Commune, related_name='Fma_commune', null=True)

    class Meta:
        database = db

    @staticmethod
    def get_first_year():
        """

        :return: year of the older intervention
        """
        return Fma.select().order_by(Fma.dateDebut)[0].dateDebut.year

    def get_number_trainees(self):
        """

        :return: number of spv formes
        """
        return len(self.autresSpvFormes.replace("[","").replace("]", "").split(','))

    def get_number_instructors(self):
        """

        :return: number of formateurs
        """
        return len(self.autresFormateurs.replace("[","").replace("]", "").split(','))

    def get_number_spvs(self):
        """

        :return: number of spv (formes & formateurs)
        """
        return self.get_number_instructors() + self.get_number_trainees()


class FmaPersonnel(Model):
    spv = ForeignKeyField(Personnel, related_name='FmaPersonnel_spv')
    fma = ForeignKeyField(Fma, related_name='FmaPersonnel_fma')

    class Meta:
        database = db


# VEHICLE CLASSES

class Vehicle(Model):
    """
    Vehicle class
    """
    status = IntegerField()
    """
    0: un/deactivated
    1 : ok
    """
    nickName = CharField(max_length=40, null=True)
    licensePlate = CharField(max_length=20, null=True)
    km = IntegerField(default=0)
    currentKm = IntegerField(default=0)

    def getChild(self):
        self.Children.get()

    class Meta:
        database = db


class VehicleFPT(Vehicle):
    """
    Vehicle class
    """
    casID = IntegerField(unique=True)

    def toString(self):
        return "FPT-" + str(self.casID)

    class Meta:
        database = db


class VehicleVL(Vehicle):
    """
    Vehicle class
    """
    casID = IntegerField(unique=True)

    def toString(self):
        return "VL-" + str(self.casID)

    class Meta:
        database = db


class VehicleVTU(Vehicle):
    """
    Vehicle class
    """
    casID = IntegerField(unique=True)

    def toString(self):
        return "VTU-" + str(self.casID)

    class Meta:
        database = db


# INTERVENTIONS CLASSES

class VL(Model):
    con = ForeignKeyField(Personnel, related_name='VL_con')
    ca = ForeignKeyField(Personnel, related_name='VL_ca')
    equ = ForeignKeyField(Personnel, related_name='VL_equ')
    stag = ForeignKeyField(Personnel, related_name='VL_stag')
    inter = ForeignKeyField(Interventions, related_name='VL_inter')
    vehicle = ForeignKeyField(VehicleVL, related_name="VL_vehicle")

    class Meta:
        database = db  # This model uses the "people.db" database.


class VTU(Model):
    con = ForeignKeyField(Personnel, related_name='VTU_con')
    ca = ForeignKeyField(Personnel, related_name='VTU_ca')
    equ = ForeignKeyField(Personnel, related_name='VTU_equ')
    stag = ForeignKeyField(Personnel, related_name='VTU_stag')
    inter = ForeignKeyField(Interventions, related_name='VTU_inter')
    vehicle = ForeignKeyField(VehicleVTU, related_name="VTU_vehicle")

    class Meta:
        database = db  # This model uses the "people.db" database.


class FPTL(Model):
    con = ForeignKeyField(Personnel, related_name='FTPL_con')
    ca = ForeignKeyField(Personnel, related_name='FPTL_ca')
    ce = ForeignKeyField(Personnel, related_name='FPTL_ce')
    ce2 = ForeignKeyField(Personnel, related_name='FPTL_ce2')
    equ = ForeignKeyField(Personnel, related_name='FPTL_equ')
    equ2 = ForeignKeyField(Personnel, related_name='FPTL_equ2')
    stag = ForeignKeyField(Personnel, related_name='FPTL_stag')
    inter = ForeignKeyField(Interventions, related_name='FPTL_inter')
    vehicle = ForeignKeyField(VehicleFPT, related_name="FPTL_vehicle")

    class Meta:
        database = db  # This model uses the "people.db" database.


class StatsIntersPersonnel(Model):
    """
    TODO: garder ?
    Easier to reverse search inter from user (otherwise we had to use reverse search for each function of each vehicle for each inter)
    """
    spv = ForeignKeyField(Personnel, related_name='StatsIntersPersonnel_spv')
    inter = ForeignKeyField(Interventions, related_name='StatsIntersPersonnel_inter', null=True)
    typeVehicule = IntegerField()  # permet le filtre selon les véhicules
    fonction = IntegerField()  # idem

    class Meta:
        database = db


class StatsFmaPersonnel(Model):
    spv = ForeignKeyField(Personnel, related_name='StatsFmaPersonnel_spv')
    fma = ForeignKeyField(Fma, related_name='StatsFmaPersonnel_fma', null=True)
    typeVehicule = IntegerField()  # permet le filtre selon les véhicules
    fonction = IntegerField()  # formateur ou formé

    class Meta:
        database = db


class Caserne(Model):
    name = CharField(max_length=60)
    type = IntegerField()
    chef = ForeignKeyField(Personnel, related_name='Caserne_chef', unique=True, null=True)
    adjoint = ForeignKeyField(Personnel, related_name='Caserne_adjoint', unique=True, null=True)
    passwd1 = CharField(max_length=40)
    pathInter = TextField()
    pathFma = TextField()
    pathCasernement = TextField()
    pathInterByYear = BooleanField(default=True)
    pathFmaByYear = BooleanField(default=True)
    pathCasernementByYear = BooleanField(default=True)

    class Meta:
        database = db

"""
    Pompier-GLX
    Copyright (C) 2013-2018  Sydney Rodolphe Torcuato Gems

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
