#!/usr/bin/python3
# PGLX Copyright - Sydney Gems GPLv3

"""
add ccf and vpb to premDep and renforts tables
"""

import os
from pglx.models import *
from playhouse.migrate import *


def migrate_001():
    print("Migrating ... 001")

    NEW_DB_VERSION = 2

    path_to_db = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'db.sqlite')
    print(path_to_pglx)
    my_db = SqliteDatabase(path_to_db)
    migrator = SqliteMigrator(my_db)
    ccf = IntegerField(null=True, default=0)
    vpb = IntegerField(null=True, default=0)

    migrate(
        migrator.add_column('Renforts', 'ccf', ccf),
        migrator.add_column('Renforts', 'vpb', vpb),
        migrator.add_column('PremierDepart', 'ccf', ccf),
        migrator.add_column('PremierDepart', 'vpb', vpb),
    )

    for renfort in Renforts.select():
        renfort.ccf = 0
        renfort.vpb = 0
        renfort.save()

    for premierDep in PremierDepart.select():
        premierDep.ccf = 0
        premierDep.vpb = 0
        premierDep.save()

    bdm = BDMeta.get(id=1)
    bdm.version = NEW_DB_VERSION
    bdm.save()

    bdum = BDUserMeta.get(id=1)
    bdum.version = NEW_DB_VERSION
    bdum.save()
