"""
Add num_bip to Personnel
"""

from playhouse.migrate import *
from pglx.models import BDMeta, BDUserMeta, user_db

NEW_DB_VERSION = 3


def migrate_005():
    print("Migrating 005 ... : add num_bip to Personnel")

    migrator = SqliteMigrator(user_db)

    num_bip = CharField(max_length=16, null=True)

    try:
        migrate(
            migrator.add_column('Personnel', 'num_bip', num_bip)
        )
        print("Success")

        bdm = BDMeta.get(id=1)
        bdm.version = NEW_DB_VERSION
        bdm.save()
        bdum = BDUserMeta.get(id=1)
        bdum.version = NEW_DB_VERSION
        bdum.save()

        print("New DB Version (User and BD) : " +str(NEW_DB_VERSION))

    except Exception as e:
        print("[EE] Failed to migrate_005 num_bip : " + str(e))
    finally:
        print("Migration finished")
