from playhouse.migrate import *
from pglx.models import Personnel


def migrate_003():
        db = SqliteDatabase('user_db.sqlite')
        migrator = SqliteMigrator(db)

        heavyVehicleLicenceDate = DateTimeField(null=True)
        matriculeCS = IntegerField(unique=True, null=True)

        migrate(
                migrator.add_column('Personnel', 'heavyVehicleLicenceDate', heavyVehicleLicenceDate),
                migrator.add_column('Personnel', 'matriculeCS', matriculeCS)
        )
