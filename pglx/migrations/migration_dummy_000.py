#!/usr/bin/python3
# PGLX Copyright - Sydney Gems GPLv3

"""
Dummy transition for SISPAM database
"""

from pglx.models import *

def migrateAllInter():
    interList = Interventions.select()
    if interList:
        print("[II] Migrating interventions ...\n")
        for inter in interList:
            try:
                migrateInter(inter)
            except Exception as e:
                print("[EE] Error while migrating interventions: ", str(e))
        bdMeta = BDMeta.get(id=1)
        bdMeta.version = 1
        bdMeta.save()
        print("[II] Successfully migrated the interventions")
    else:
        print("[II] No pending migration ... END\n")


def migrateInter(inter):
    """
    Migrate old inter (specify vehicle field)
    """
    print("[II] Migration: Inter id:",  inter.id, "\n")
    VlList = VL.filter(VL.inter == inter)
    if VlList:
        for vl in VlList:
            print("     VL:", vl.id, "\n")
            vl.vehicle = VehicleVL.get(id=1)
            vl.save()
    VtuList = VTU.filter(VTU.inter == inter)
    if VtuList:
        for vtu in VtuList:
            print("     VTU:", vtu.id, "\n")
            vtu.vehicle = VehicleVTU.get(id=1)
            vtu.save()
    FptList = FPTL.filter(FPTL.inter == inter)
    if FptList:
        for fpt in FptList:
            print("     FPT:", fpt.id, "\n")
            fpt.vehicle = VehicleFPT.get(id=1)
            fpt.save()


def migrate_000():
    migrateAllInter()
