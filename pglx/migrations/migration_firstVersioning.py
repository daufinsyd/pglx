#!/usr/bin/python3
# PGLX Copyright - Sydney Gems GPLv3

"""
Dummy transition for SISPAM database
"""

from playhouse.migrate import *
from pglx.models import VehicleVL, VehicleVTU, VehicleFPT


def migrate_004():
	db = SqliteDatabase('db.sqlite')
	migrator = SqliteMigrator(db)

	print("[II] Migration: add columns to existing tables")

	# For add_column from playhouse migration, to_field must be explicity specified for foreignkey
	vehicleVlField = ForeignKeyField(VehicleVL, related_name="VL_vehicle", null=True, to_field=VehicleVL.id)
	vehicleVtuField = ForeignKeyField(VehicleVTU, related_name="VTU_vehicle", null=True, to_field=VehicleVTU.id)
	vehicleFptlField = ForeignKeyField(VehicleFPT, related_name="FPT_vehicle", null=True, to_field=VehicleFPT.id)

	migrate(
		migrator.add_column('VL', 'vehicle_id', vehicleVlField),
		migrator.add_column('VTU', 'vehicle_id', vehicleVtuField),
		migrator.add_column('FPTL', 'vehicle_id', vehicleFptlField),
	)

	print("[II] Migration: Ok. Create first required vehicles before continuing the migration ...")
