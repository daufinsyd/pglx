import pglx.pglx_qt
from pglx.widgets.my_main_window import Ui_MyMainWindow
from PyQt5 import QtWidgets
from pglx import logger
import os


class Credits(Ui_MyMainWindow):
    def __init__(self, parent=None):
        super(Credits, self).__init__(parent)
        self.initUI()
        self.center()

    def initUI(self):

        self.textEdit = QtWidgets.QTextEdit()
        self.setCentralWidget(self.textEdit)
        self.statusBar()

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Pompier-GLX - Crédits')
        self.show()

        try:
            with open(os.path.join(pglx.pglx_qt.path_to_data, 'credits.txt'), 'r') as file:
                self.textEdit.setText(file.read())
        except FileNotFoundError:
            QtWidgets.QMessageBox.critical(self, "No credits", "The file credits.txt wasn't found.")
        except Exception as e:
            logger.exception("Failed to open credits.txt: ")
            QtWidgets.QMessageBox.critical(self, "No credits", "The file credits.txt wasn't found.\n" + str(e))
        self.textEdit.setReadOnly(True)
