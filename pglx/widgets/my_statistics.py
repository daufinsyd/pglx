from pglx.widgets.my_main_window import Ui_MyMainWindow
import datetime
from pglx.ui_py.stats import Ui_Statistics
import pglx.stats as stats
from pglx.models import Interventions, Fma


from pglx.functions_streams import display_dict
import pglx.pglx_qt as pglx_qt
import pglx.graphs as graphs


class Ui_MyStatistics(Ui_MyMainWindow, Ui_Statistics):
    def __init__(self, parent=None, **kwargs):
        super(Ui_MyStatistics, self).__init__(parent, **kwargs)
        if pglx_qt.fonctions.userHasPerm(perms=2, allow_chef=True, allow_adjoint=True, allow_admin=True):
            self.setupUi(self)
            self.post_ui()
            self.set_values()
        else:
            raise PermissionError

    def post_ui(self):
        self.spinBox_i_start_year.setMaximum(datetime.datetime.now().year)
        self.spinBox_i_start_year.setMinimum(Interventions.get_first_year())
        self.spinBox_f_start_year.setMaximum(datetime.datetime.now().year)
        self.spinBox_f_start_year.setMinimum(Fma.get_first_year())
        self.set_max_spinbox_nb_years()

    def set_max_spinbox_nb_years(self):
        # max nb years (min: 1)
        self.spinBox_i_nb_years.setMaximum(datetime.datetime.now().year - self.get_inter_year() + 1)
        self.spinBox_f_nb_years.setMaximum(datetime.datetime.now().year - self.get_fma_year() + 1)

    def set_values(self):
        self.spinBox_i_start_year.setValue(datetime.datetime.now().year)
        self.spinBox_f_start_year.setValue(datetime.datetime.now().year)

    def get_fma_year(self):
        return self.spinBox_f_start_year.value()

    def get_fma_during_time(self):
        return self.spinBox_f_nb_years.value()

    def get_inter_year(self):
        return self.spinBox_i_start_year.value()

    def get_inter_during_time(self):
        return self.spinBox_i_nb_years.value()

    def on_pushButton_fma_process_pressed(self):
        values = stats.get_fma(self.get_fma_year(), self.get_fma_during_time())

        txt = "Aide :\n" \
              "  Le total \"Réel\" indique le nombre d'heures de travail cumulées par les Sapeurs-Pompiers ;\n" \
              "  Le total \"Simulé\" indique le nombre d'heures des FMAs non cumulées ;\n" \
              "\n\n" \
              "Année : " + str(self.get_fma_year()) + " sur " + str(self.get_fma_during_time()) + " an(s)\n" \
                                                      "\n"
        txt += display_dict(values)
        self.textBrowser_fma.setText(txt)

    def on_pushButton_inter_process_pressed(self):
        values = stats.get_inter(self.get_inter_year(), self.get_inter_during_time())

        txt = "Aide :\n" \
              "  Le total \"Réel\" indique le nombre d'heures de travail cumulées par les Sapeurs-Pompiers ;\n" \
              "  Le total \"Simulé\" indique le nombre d'heures des interventions non cumulées ;\n" \
              "\n\n" \
              "Année : " + str(self.get_inter_year()) + " sur " + str(self.get_inter_during_time()) + " an(s)\n" \
                                                        "\n"
        txt += display_dict(values)
        self.textBrowser_inter.setText(txt)

    def on_pushButton_i_graph_generate_pressed(self):
        radar_inter_type = graphs.RadarChart_interType(year=self.get_inter_year(), nb_years=self.get_inter_during_time(), subplot_pos=121)
        radar_inter_vehicle = graphs.RadarChart_interVehicle(year=self.get_inter_year(), nb_years=self.get_inter_during_time(), subplot_pos=122)
        radar_inter_type.show()
        radar_inter_vehicle.show()

    def on_spinBox_f_start_year_valueChanged(self):
        self.set_max_spinbox_nb_years()

    def on_spinBox_i_start_year_valueChanged(self):
        self.set_max_spinbox_nb_years()
