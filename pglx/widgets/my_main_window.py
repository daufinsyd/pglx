from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal


class Ui_MyMainWindow(QtWidgets.QMainWindow):
    closed = pyqtSignal()

    def __init__(self, parent=None, on_closed_=None):
        super(Ui_MyMainWindow, self).__init__(parent)
        #self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        if on_closed_:
            print("Con")
            self.closed.connect(on_closed_)

    def center(self):
        qr = self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def closeEvent(self, event):
        # To be overwritten if another behaviour is required
        self.closed.emit()
        event.accept()
