import logging
import os
import sys
import faulthandler

name="pglx"


# DB
CURRENT_DB_VERSION = 3
db_is_up_to_date = True

# PATH
path_to_pglx = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
path_to_data = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")


# LOGGING
LOGGING_LEVEL = logging.INFO
DB_VERSION = 3

logger = logging.getLogger(__name__)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

logging.basicConfig(filename=path_to_pglx + "/pglx.log", level=LOGGING_LEVEL, format='%(levelname)s:%(asctime)s %(message)s')

faulthandler.enable()
