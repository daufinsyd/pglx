from math import pi
import matplotlib.pyplot as plt
import pglx.models as models
import pglx.pglx_qt as pglx_qt


class RadarChart:
    def __init__(self, cat, values, min_val=None, max_val=None, subplot_pos=111):
        """

        :param cat:
        :param values:
        :param min_val: min axis limit
        :param max_val: max axis limit
        """
        if min_val is None:
            min_val = 0
            print("e")
        if max_val is None:
            max_val = 100
            print('r')

        n = len(cat)

        x_as = [N / float(n) * 2 * pi for N in range(n)]

        # Because our chart will be circular we need to append a copy of the first
        # value of each list at the end of each list with data
        values += values[:1]
        x_as += x_as[:1]

        # Set color of axes
        plt.rc('axes', linewidth=0.5, edgecolor="#888888")

        # Create polar plot
        ax = plt.subplot(subplot_pos, polar=True)

        # Set clockwise rotation. That is:
        ax.set_theta_offset(pi / 2)
        ax.set_theta_direction(-1)

        # Set position of y-labels
        ax.set_rlabel_position(0)

        # Set color and line style of grid
        ax.xaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.5)
        ax.yaxis.grid(True, color="#888888", linestyle='solid', linewidth=0.5)

        # Set number of radial axes and remove labels
        plt.xticks(x_as[:-1], [])

        # Set yticks
        # 10 ticks of set (max_val-min_val). Since range cannot accept non integer step, multiple by ten in range()
        # and then divide by 10 (x/10)
        plt.yticks([x/10 for x in range(min_val*10, max_val*10, max_val-min_val)], [str(x/10) for x in range(min_val*10, max_val*10, max_val-min_val)])

        # Plot data
        ax.plot(x_as, values, linewidth=3, linestyle='solid', zorder=3)

        # Fill area
        ax.fill(x_as, values, 'b', alpha=0.3)

        # Set axes limits
        plt.ylim(min_val, max_val)

        # Draw ytick labels to make sure they fit properly
        for i in range(n):
            angle_rad = i / float(n) * 2 * pi

            if angle_rad == 0:
                ha, distance_ax = "center", 10
            elif 0 < angle_rad < pi:
                ha, distance_ax = "left", 1
            elif angle_rad == pi:
                ha, distance_ax = "center", 1
            else:
                ha, distance_ax = "right", 1

            ax.text(angle_rad, max_val + distance_ax, cat[i], size=10, horizontalalignment=ha, verticalalignment="center")

    @staticmethod
    def show():
        # Show polar plot
        plt.show()


class RadarChart_interType(RadarChart):
    def __init__(self, year, nb_years, subplot_pos=221):
        cat = self.get_cat_inter_type()
        values = self.get_data_inter_type(year=year, nb_years=nb_years)

        super(RadarChart_interType, self).__init__(cat=cat, values=values, subplot_pos=subplot_pos)

    @staticmethod
    def get_cat_inter_type():
        return pglx_qt.TYPE_INTER

    @staticmethod
    def get_cat_fma_type():
        return pglx_qt.TYPE_FMA

    @staticmethod
    def get_data_inter_type(year, nb_years, types_i=None):
        if types_i is None:
            types_i = [0, 1, 2, 3, 4, 5]

        types_i = sorted(types_i)

        data = [0] * len(pglx_qt.TYPE_INTER)
        total = 0
        for t in types_i:
            data[t] = models.Interventions.filter(models.Interventions.typeInter == t,
                                                  models.Interventions.date_appel.year >= year,
                                                  models.Interventions.date_appel.year < year + nb_years).count() * 10
            total += data[t]

        data = list(map(lambda x: x / total * 100, data))

        return data


class RadarChart_interVehicle(RadarChart):
    def __init__(self, year, nb_years, vehicles_type=None, subplot_pos=222):

        if vehicles_type is None:
            vehicles_type = ["vl", "vtu", "fpt"]

        self.cat = self.get_cat_inter_vehicles(vehicles_type)
        self.values = self.get_data_inter_type(year=year, nb_years=nb_years, vehicles_type=vehicles_type)

        super(RadarChart_interVehicle, self).__init__(cat=self.cat, values=self.values, max_val=max(self.values), subplot_pos=subplot_pos)

    @staticmethod
    def get_cat_inter_vehicles(vehicles_type=None):
        """

        :param vehicles_type:
        :return: cat
        """
        cat = []

        if "vl" in vehicles_type:
            for i in models.VehicleVL.select().order_by(models.VehicleVL.casID):
                cat.append(i.toString())
        if "vtu" in vehicles_type:
            for i in models.VehicleVTU.select().order_by(models.VehicleVTU.casID):
                cat.append(i.toString())
        if "fpt" in vehicles_type:
            for i in models.VehicleFPT.select().order_by(models.VehicleFPT.casID):
                cat.append(i.toString())

        return cat

    def get_data_inter_type(self, year, nb_years, vehicles_type):
        data = [0] * len(self.cat)
        cpt = 0
        if "vl" in vehicles_type:
            for i in models.VehicleVL.select().order_by(models.VehicleVL.casID):
                data[cpt] = models.VL.filter(models.VL.vehicle == i).join(models.Interventions).where(
                                                  models.Interventions.date_appel.year >= year,
                                                  models.Interventions.date_appel.year < year + nb_years).count()
                cpt += 1
        if "vtu" in vehicles_type:
            for i in models.VehicleVTU.select().order_by(models.VehicleVTU.casID):
                data[cpt] = models.VTU.filter(models.VTU.vehicle == i).join(models.Interventions).where(
                    models.Interventions.date_appel.year >= year,
                    models.Interventions.date_appel.year < year + nb_years).count()
                cpt += 1
        if "fpt" in vehicles_type:
            for i in models.VehicleFPT.select().order_by(models.VehicleFPT.casID):
                data[cpt] = models.FPTL.filter(models.FPTL.vehicle == i).join(models.Interventions).where(
                    models.Interventions.date_appel.year >= year,
                    models.Interventions.date_appel.year < year + nb_years).count()
                cpt += 1

        return data
