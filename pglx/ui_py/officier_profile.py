# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/officier_profile.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ProfileDialog(object):
    def setupUi(self, ProfileDialog):
        ProfileDialog.setObjectName("ProfileDialog")
        ProfileDialog.resize(334, 516)
        self.gridLayout_2 = QtWidgets.QGridLayout(ProfileDialog)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.buttonBox = QtWidgets.QDialogButtonBox(ProfileDialog)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Apply|QtWidgets.QDialogButtonBox.Close)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_2.addWidget(self.buttonBox, 3, 0, 1, 1)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label_first_name = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_first_name.sizePolicy().hasHeightForWidth())
        self.label_first_name.setSizePolicy(sizePolicy)
        self.label_first_name.setObjectName("label_first_name")
        self.gridLayout.addWidget(self.label_first_name, 1, 2, 1, 1)
        self.label_matricule = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_matricule.sizePolicy().hasHeightForWidth())
        self.label_matricule.setSizePolicy(sizePolicy)
        self.label_matricule.setObjectName("label_matricule")
        self.gridLayout.addWidget(self.label_matricule, 2, 2, 1, 1)
        self.label_matricule_cs = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_matricule_cs.sizePolicy().hasHeightForWidth())
        self.label_matricule_cs.setSizePolicy(sizePolicy)
        self.label_matricule_cs.setObjectName("label_matricule_cs")
        self.gridLayout.addWidget(self.label_matricule_cs, 3, 2, 1, 1)
        self.label_last_name = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_last_name.sizePolicy().hasHeightForWidth())
        self.label_last_name.setSizePolicy(sizePolicy)
        self.label_last_name.setObjectName("label_last_name")
        self.gridLayout.addWidget(self.label_last_name, 0, 2, 1, 1)
        self.widget_icon = QtWidgets.QWidget(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_icon.sizePolicy().hasHeightForWidth())
        self.widget_icon.setSizePolicy(sizePolicy)
        self.widget_icon.setMinimumSize(QtCore.QSize(120, 0))
        self.widget_icon.setBaseSize(QtCore.QSize(90, 116))
        self.widget_icon.setStyleSheet("image: url(:/avatar_icon/Firefox_Nightly.png);")
        self.widget_icon.setObjectName("widget_icon")
        self.gridLayout.addWidget(self.widget_icon, 0, 1, 4, 1)
        self.spinBox_matricule_cs = QtWidgets.QSpinBox(ProfileDialog)
        self.spinBox_matricule_cs.setMaximum(999999)
        self.spinBox_matricule_cs.setObjectName("spinBox_matricule_cs")
        self.gridLayout.addWidget(self.spinBox_matricule_cs, 3, 3, 1, 1)
        self.spinBox_matricule = QtWidgets.QSpinBox(ProfileDialog)
        self.spinBox_matricule.setObjectName("spinBox_matricule")
        self.gridLayout.addWidget(self.spinBox_matricule, 2, 3, 1, 1)
        self.lineEdit_first_name = QtWidgets.QLineEdit(ProfileDialog)
        self.lineEdit_first_name.setObjectName("lineEdit_first_name")
        self.gridLayout.addWidget(self.lineEdit_first_name, 1, 3, 1, 1)
        self.lineEdit_last_name = QtWidgets.QLineEdit(ProfileDialog)
        self.lineEdit_last_name.setObjectName("lineEdit_last_name")
        self.gridLayout.addWidget(self.lineEdit_last_name, 0, 3, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.comboBox_grade = QtWidgets.QComboBox(ProfileDialog)
        self.comboBox_grade.setObjectName("comboBox_grade")
        self.gridLayout_3.addWidget(self.comboBox_grade, 0, 1, 1, 1)
        self.checkBox_cod2 = QtWidgets.QCheckBox(ProfileDialog)
        self.checkBox_cod2.setObjectName("checkBox_cod2")
        self.gridLayout_3.addWidget(self.checkBox_cod2, 4, 1, 1, 1)
        self.label_heavyVehicleLicenceDate = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_heavyVehicleLicenceDate.sizePolicy().hasHeightForWidth())
        self.label_heavyVehicleLicenceDate.setSizePolicy(sizePolicy)
        self.label_heavyVehicleLicenceDate.setObjectName("label_heavyVehicleLicenceDate")
        self.gridLayout_3.addWidget(self.label_heavyVehicleLicenceDate, 6, 0, 1, 1)
        self.comboBox_rank = QtWidgets.QComboBox(ProfileDialog)
        self.comboBox_rank.setObjectName("comboBox_rank")
        self.gridLayout_3.addWidget(self.comboBox_rank, 7, 1, 1, 1)
        self.comboBox_type = QtWidgets.QComboBox(ProfileDialog)
        self.comboBox_type.setObjectName("comboBox_type")
        self.gridLayout_3.addWidget(self.comboBox_type, 1, 1, 1, 1)
        self.label_type = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_type.sizePolicy().hasHeightForWidth())
        self.label_type.setSizePolicy(sizePolicy)
        self.label_type.setObjectName("label_type")
        self.gridLayout_3.addWidget(self.label_type, 1, 0, 1, 1)
        self.label_rank = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_rank.sizePolicy().hasHeightForWidth())
        self.label_rank.setSizePolicy(sizePolicy)
        self.label_rank.setObjectName("label_rank")
        self.gridLayout_3.addWidget(self.label_rank, 7, 0, 1, 1)
        self.label_involvementDate = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_involvementDate.sizePolicy().hasHeightForWidth())
        self.label_involvementDate.setSizePolicy(sizePolicy)
        self.label_involvementDate.setObjectName("label_involvementDate")
        self.gridLayout_3.addWidget(self.label_involvementDate, 5, 0, 1, 1)
        self.dateEdit_involvementDate = QtWidgets.QDateEdit(ProfileDialog)
        self.dateEdit_involvementDate.setObjectName("dateEdit_involvementDate")
        self.gridLayout_3.addWidget(self.dateEdit_involvementDate, 5, 1, 1, 1)
        self.label_driver_licence = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_driver_licence.sizePolicy().hasHeightForWidth())
        self.label_driver_licence.setSizePolicy(sizePolicy)
        self.label_driver_licence.setObjectName("label_driver_licence")
        self.gridLayout_3.addWidget(self.label_driver_licence, 2, 0, 1, 1)
        self.label_grade = QtWidgets.QLabel(ProfileDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_grade.sizePolicy().hasHeightForWidth())
        self.label_grade.setSizePolicy(sizePolicy)
        self.label_grade.setObjectName("label_grade")
        self.gridLayout_3.addWidget(self.label_grade, 0, 0, 1, 1)
        self.checkBox_driver_licence = QtWidgets.QCheckBox(ProfileDialog)
        self.checkBox_driver_licence.setObjectName("checkBox_driver_licence")
        self.gridLayout_3.addWidget(self.checkBox_driver_licence, 2, 1, 1, 1)
        self.checkBox_cod1 = QtWidgets.QCheckBox(ProfileDialog)
        self.checkBox_cod1.setObjectName("checkBox_cod1")
        self.gridLayout_3.addWidget(self.checkBox_cod1, 3, 1, 1, 1)
        self.dateEdit_heavyVehicleLicenceDate = QtWidgets.QDateEdit(ProfileDialog)
        self.dateEdit_heavyVehicleLicenceDate.setObjectName("dateEdit_heavyVehicleLicenceDate")
        self.gridLayout_3.addWidget(self.dateEdit_heavyVehicleLicenceDate, 6, 1, 1, 1)
        self.label_num_bip = QtWidgets.QLabel(ProfileDialog)
        self.label_num_bip.setObjectName("label_num_bip")
        self.gridLayout_3.addWidget(self.label_num_bip, 8, 0, 1, 1)
        self.lineEdit_num_bip = QtWidgets.QLineEdit(ProfileDialog)
        self.lineEdit_num_bip.setObjectName("lineEdit_num_bip")
        self.gridLayout_3.addWidget(self.lineEdit_num_bip, 8, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout_3, 1, 0, 1, 1)
        self.pushButton_change_password = QtWidgets.QPushButton(ProfileDialog)
        self.pushButton_change_password.setObjectName("pushButton_change_password")
        self.gridLayout_2.addWidget(self.pushButton_change_password, 2, 0, 1, 1)

        self.retranslateUi(ProfileDialog)
        QtCore.QMetaObject.connectSlotsByName(ProfileDialog)

    def retranslateUi(self, ProfileDialog):
        _translate = QtCore.QCoreApplication.translate
        ProfileDialog.setWindowTitle(_translate("ProfileDialog", "Profil agent"))
        self.label_first_name.setText(_translate("ProfileDialog", "Prénom"))
        self.label_matricule.setText(_translate("ProfileDialog", "Matricule"))
        self.label_matricule_cs.setText(_translate("ProfileDialog", "Matricule CS"))
        self.label_last_name.setText(_translate("ProfileDialog", "Nom"))
        self.checkBox_cod2.setText(_translate("ProfileDialog", "COD 2"))
        self.label_heavyVehicleLicenceDate.setText(_translate("ProfileDialog", "Date permis PL"))
        self.label_type.setText(_translate("ProfileDialog", "Type d\'agent"))
        self.label_rank.setText(_translate("ProfileDialog", "Permissions"))
        self.label_involvementDate.setText(_translate("ProfileDialog", "Date d\'engagement"))
        self.label_driver_licence.setText(_translate("ProfileDialog", "Permis"))
        self.label_grade.setText(_translate("ProfileDialog", "Grade"))
        self.checkBox_driver_licence.setText(_translate("ProfileDialog", "Permis B"))
        self.checkBox_cod1.setText(_translate("ProfileDialog", "COD 1"))
        self.label_num_bip.setText(_translate("ProfileDialog", "N° Bipper"))
        self.pushButton_change_password.setText(_translate("ProfileDialog", "Changer le mot de passe"))

