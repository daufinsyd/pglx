# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/pglx_changePasswd.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ChangePasswordDialog(object):
    def setupUi(self, ChangePasswordDialog):
        ChangePasswordDialog.setObjectName("ChangePasswordDialog")
        ChangePasswordDialog.resize(289, 267)
        self.gridLayout = QtWidgets.QGridLayout(ChangePasswordDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.frame = QtWidgets.QFrame(ChangePasswordDialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_oldPasswd = QtWidgets.QLabel(self.frame)
        self.label_oldPasswd.setObjectName("label_oldPasswd")
        self.gridLayout_2.addWidget(self.label_oldPasswd, 0, 0, 1, 1)
        self.lineEdit_oldPasswd = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_oldPasswd.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.lineEdit_oldPasswd.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_oldPasswd.setObjectName("lineEdit_oldPasswd")
        self.gridLayout_2.addWidget(self.lineEdit_oldPasswd, 1, 0, 1, 1)
        self.label_newPasswd = QtWidgets.QLabel(self.frame)
        self.label_newPasswd.setObjectName("label_newPasswd")
        self.gridLayout_2.addWidget(self.label_newPasswd, 2, 0, 1, 1)
        self.lineEdit_newPasswd = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_newPasswd.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_newPasswd.setObjectName("lineEdit_newPasswd")
        self.gridLayout_2.addWidget(self.lineEdit_newPasswd, 3, 0, 1, 1)
        self.label_newPasswd2 = QtWidgets.QLabel(self.frame)
        self.label_newPasswd2.setObjectName("label_newPasswd2")
        self.gridLayout_2.addWidget(self.label_newPasswd2, 4, 0, 1, 1)
        self.lineEdit_newPasswd2 = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_newPasswd2.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText|QtCore.Qt.ImhSensitiveData)
        self.lineEdit_newPasswd2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_newPasswd2.setObjectName("lineEdit_newPasswd2")
        self.gridLayout_2.addWidget(self.lineEdit_newPasswd2, 5, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.frame)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_2.addWidget(self.buttonBox, 6, 0, 1, 1)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)

        self.retranslateUi(ChangePasswordDialog)
        QtCore.QMetaObject.connectSlotsByName(ChangePasswordDialog)

    def retranslateUi(self, ChangePasswordDialog):
        _translate = QtCore.QCoreApplication.translate
        ChangePasswordDialog.setWindowTitle(_translate("ChangePasswordDialog", "Changer de mot de passe"))
        self.label_oldPasswd.setText(_translate("ChangePasswordDialog", "Saisissez l\'ancien mot de passe :"))
        self.label_newPasswd.setText(_translate("ChangePasswordDialog", "Saisissez le nouveau mot de passe :"))
        self.label_newPasswd2.setText(_translate("ChangePasswordDialog", "Ressaisissez le nouveau mot de passe :"))

