# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/pglx_managementVehicle.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_VehicleManagement(object):
    def setupUi(self, VehicleManagement):
        VehicleManagement.setObjectName("VehicleManagement")
        VehicleManagement.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(VehicleManagement)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.frame_principale = QtWidgets.QFrame(self.centralwidget)
        self.frame_principale.setToolTip("")
        self.frame_principale.setToolTipDuration(-1)
        self.frame_principale.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_principale.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_principale.setObjectName("frame_principale")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_principale)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.pushButton_addVehicle = QtWidgets.QPushButton(self.frame_principale)
        self.pushButton_addVehicle.setObjectName("pushButton_addVehicle")
        self.gridLayout_2.addWidget(self.pushButton_addVehicle, 2, 0, 1, 1)
        self.label_sortByType = QtWidgets.QLabel(self.frame_principale)
        self.label_sortByType.setObjectName("label_sortByType")
        self.gridLayout_2.addWidget(self.label_sortByType, 1, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.frame_principale)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_2.addWidget(self.buttonBox, 2, 3, 1, 1)
        self.pushButton_deactivateVehicle = QtWidgets.QPushButton(self.frame_principale)
        self.pushButton_deactivateVehicle.setObjectName("pushButton_deactivateVehicle")
        self.gridLayout_2.addWidget(self.pushButton_deactivateVehicle, 2, 2, 1, 1)
        self.pushButton_activateVehicle = QtWidgets.QPushButton(self.frame_principale)
        self.pushButton_activateVehicle.setObjectName("pushButton_activateVehicle")
        self.gridLayout_2.addWidget(self.pushButton_activateVehicle, 2, 1, 1, 1)
        self.comboBox_sortByType = QtWidgets.QComboBox(self.frame_principale)
        self.comboBox_sortByType.setObjectName("comboBox_sortByType")
        self.comboBox_sortByType.addItem("")
        self.comboBox_sortByType.addItem("")
        self.comboBox_sortByType.addItem("")
        self.comboBox_sortByType.addItem("")
        self.gridLayout_2.addWidget(self.comboBox_sortByType, 1, 1, 1, 1)
        self.tableWidget_vehicle = QtWidgets.QTableWidget(self.frame_principale)
        self.tableWidget_vehicle.setToolTip("")
        self.tableWidget_vehicle.setObjectName("tableWidget_vehicle")
        self.tableWidget_vehicle.setColumnCount(7)
        self.tableWidget_vehicle.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setHorizontalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setHorizontalHeaderItem(6, item)
        self.gridLayout_2.addWidget(self.tableWidget_vehicle, 0, 0, 1, 4)
        self.checkBox_showDeactivatedVehicles = QtWidgets.QCheckBox(self.frame_principale)
        self.checkBox_showDeactivatedVehicles.setObjectName("checkBox_showDeactivatedVehicles")
        self.gridLayout_2.addWidget(self.checkBox_showDeactivatedVehicles, 1, 2, 1, 1)
        self.gridLayout.addWidget(self.frame_principale, 0, 0, 1, 1)
        VehicleManagement.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(VehicleManagement)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 30))
        self.menubar.setObjectName("menubar")
        VehicleManagement.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(VehicleManagement)
        self.statusbar.setObjectName("statusbar")
        VehicleManagement.setStatusBar(self.statusbar)

        self.retranslateUi(VehicleManagement)
        QtCore.QMetaObject.connectSlotsByName(VehicleManagement)

    def retranslateUi(self, VehicleManagement):
        _translate = QtCore.QCoreApplication.translate
        VehicleManagement.setWindowTitle(_translate("VehicleManagement", "Gestion des véhicules"))
        self.pushButton_addVehicle.setText(_translate("VehicleManagement", "Ajouter un véhicule"))
        self.label_sortByType.setText(_translate("VehicleManagement", "Trier par type"))
        self.pushButton_deactivateVehicle.setToolTip(_translate("VehicleManagement", "Désactive le véhicule sélectionné (inutilisable)"))
        self.pushButton_deactivateVehicle.setText(_translate("VehicleManagement", "Désactiver un véhicule"))
        self.pushButton_activateVehicle.setToolTip(_translate("VehicleManagement", "Rend le véhicule sélectionné, actif (utilisable)"))
        self.pushButton_activateVehicle.setText(_translate("VehicleManagement", "Réactiver un véhicule"))
        self.comboBox_sortByType.setItemText(0, _translate("VehicleManagement", "Tous"))
        self.comboBox_sortByType.setItemText(1, _translate("VehicleManagement", "FPT"))
        self.comboBox_sortByType.setItemText(2, _translate("VehicleManagement", "VL"))
        self.comboBox_sortByType.setItemText(3, _translate("VehicleManagement", "VTU"))
        item = self.tableWidget_vehicle.horizontalHeaderItem(0)
        item.setText(_translate("VehicleManagement", "Id"))
        item = self.tableWidget_vehicle.horizontalHeaderItem(1)
        item.setText(_translate("VehicleManagement", "Id CS"))
        item = self.tableWidget_vehicle.horizontalHeaderItem(2)
        item.setText(_translate("VehicleManagement", "Nom"))
        item = self.tableWidget_vehicle.horizontalHeaderItem(3)
        item.setText(_translate("VehicleManagement", "Surnom"))
        item = self.tableWidget_vehicle.horizontalHeaderItem(4)
        item.setText(_translate("VehicleManagement", "Statut"))
        item.setToolTip(_translate("VehicleManagement", "<html><head/><body><p>0: JPS</p><p>1: apprenant</p><p>2: stagiaire</p><p>3: sapeur</p><p>4: caporal</p><p>5: caporal-chef</p><p>6: sergent</p><p>...</p></body></html>"))
        item = self.tableWidget_vehicle.horizontalHeaderItem(5)
        item.setText(_translate("VehicleManagement", "Kilométrage"))
        item = self.tableWidget_vehicle.horizontalHeaderItem(6)
        item.setText(_translate("VehicleManagement", "Prochain contrôle"))
        self.checkBox_showDeactivatedVehicles.setToolTip(_translate("VehicleManagement", "<html><head/><body><p>Si non coché, seuls les véhicules utilisables (actifs) sont affichés</p></body></html>"))
        self.checkBox_showDeactivatedVehicles.setText(_translate("VehicleManagement", "Afficher tous les véhicules"))

