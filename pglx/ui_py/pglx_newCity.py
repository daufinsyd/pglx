# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pglx/ui/pglx_newCity.ui'
#
# Created by: PyQt5 UI code generator 5.8
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogNewCity(object):
    def setupUi(self, DialogNewCity):
        DialogNewCity.setObjectName("DialogNewCity")
        DialogNewCity.resize(265, 186)
        self.verticalLayout = QtWidgets.QVBoxLayout(DialogNewCity)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(DialogNewCity)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_name = QtWidgets.QLabel(self.frame)
        self.label_name.setObjectName("label_name")
        self.verticalLayout_2.addWidget(self.label_name)
        self.lineEdit_name = QtWidgets.QLineEdit(self.frame)
        self.lineEdit_name.setObjectName("lineEdit_name")
        self.verticalLayout_2.addWidget(self.lineEdit_name)
        self.label_code = QtWidgets.QLabel(self.frame)
        self.label_code.setObjectName("label_code")
        self.verticalLayout_2.addWidget(self.label_code)
        self.spinBox_code = QtWidgets.QSpinBox(self.frame)
        self.spinBox_code.setMinimum(1)
        self.spinBox_code.setMaximum(99999)
        self.spinBox_code.setObjectName("spinBox_code")
        self.verticalLayout_2.addWidget(self.spinBox_code)
        self.verticalLayout.addWidget(self.frame)
        self.buttonBox = QtWidgets.QDialogButtonBox(DialogNewCity)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(DialogNewCity)
        self.buttonBox.accepted.connect(DialogNewCity.accept)
        self.buttonBox.rejected.connect(DialogNewCity.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogNewCity)

    def retranslateUi(self, DialogNewCity):
        _translate = QtCore.QCoreApplication.translate
        DialogNewCity.setWindowTitle(_translate("DialogNewCity", "Nouvelle commune"))
        self.label_name.setText(_translate("DialogNewCity", "Nom de la commune"))
        self.label_code.setText(_translate("DialogNewCity", "Code commune"))

