# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/pglx_inter.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow_ReportInter(object):
    def setupUi(self, MainWindow_ReportInter):
        MainWindow_ReportInter.setObjectName("MainWindow_ReportInter")
        MainWindow_ReportInter.resize(794, 725)
        self.centralwidget = QtWidgets.QWidget(MainWindow_ReportInter)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.pushButton_cancel = QtWidgets.QPushButton(self.frame)
        icon = QtGui.QIcon.fromTheme("application-exit")
        self.pushButton_cancel.setIcon(icon)
        self.pushButton_cancel.setObjectName("pushButton_cancel")
        self.gridLayout_2.addWidget(self.pushButton_cancel, 1, 1, 1, 1)
        self.pushButton_ok = QtWidgets.QPushButton(self.frame)
        icon = QtGui.QIcon.fromTheme("document-new")
        self.pushButton_ok.setIcon(icon)
        self.pushButton_ok.setObjectName("pushButton_ok")
        self.gridLayout_2.addWidget(self.pushButton_ok, 1, 2, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_2.addItem(spacerItem, 1, 0, 1, 1)
        self.scrollArea = QtWidgets.QScrollArea(self.frame)
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 746, 665))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_dateDepart = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_dateDepart.setObjectName("label_dateDepart")
        self.gridLayout_3.addWidget(self.label_dateDepart, 1, 0, 1, 1)
        self.lineEdit_natureInter = QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
        self.lineEdit_natureInter.setObjectName("lineEdit_natureInter")
        self.gridLayout_3.addWidget(self.lineEdit_natureInter, 2, 6, 1, 1)
        self.textEdit = QtWidgets.QTextEdit(self.scrollAreaWidgetContents)
        self.textEdit.setObjectName("textEdit")
        self.gridLayout_3.addWidget(self.textEdit, 22, 0, 1, 9)
        self.lineEdit_demandeur = QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
        self.lineEdit_demandeur.setObjectName("lineEdit_demandeur")
        self.gridLayout_3.addWidget(self.lineEdit_demandeur, 4, 2, 1, 1)
        self.pushButton_editVehicle = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        icon = QtGui.QIcon.fromTheme("document-edit")
        self.pushButton_editVehicle.setIcon(icon)
        self.pushButton_editVehicle.setObjectName("pushButton_editVehicle")
        self.gridLayout_3.addWidget(self.pushButton_editVehicle, 8, 6, 1, 1)
        self.comboBox_typeInter = QtWidgets.QComboBox(self.scrollAreaWidgetContents)
        self.comboBox_typeInter.setMinimumSize(QtCore.QSize(0, 30))
        self.comboBox_typeInter.setObjectName("comboBox_typeInter")
        self.gridLayout_3.addWidget(self.comboBox_typeInter, 1, 6, 1, 1)
        self.label_dateRetour = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_dateRetour.setObjectName("label_dateRetour")
        self.gridLayout_3.addWidget(self.label_dateRetour, 2, 0, 1, 1)
        self.checkBox_brigadeVerte = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBox_brigadeVerte.setObjectName("checkBox_brigadeVerte")
        self.gridLayout_3.addWidget(self.checkBox_brigadeVerte, 21, 4, 1, 1)
        self.dateTimeEdit_depart = QtWidgets.QDateTimeEdit(self.scrollAreaWidgetContents)
        self.dateTimeEdit_depart.setObjectName("dateTimeEdit_depart")
        self.gridLayout_3.addWidget(self.dateTimeEdit_depart, 1, 2, 1, 1)
        self.label_listVehicleActions = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_listVehicleActions.setObjectName("label_listVehicleActions")
        self.gridLayout_3.addWidget(self.label_listVehicleActions, 6, 6, 1, 1)
        self.toolBox2 = QtWidgets.QToolBox(self.scrollAreaWidgetContents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toolBox2.sizePolicy().hasHeightForWidth())
        self.toolBox2.setSizePolicy(sizePolicy)
        self.toolBox2.setToolTip("")
        self.toolBox2.setObjectName("toolBox2")
        self.premDep = QtWidgets.QWidget()
        self.premDep.setGeometry(QtCore.QRect(0, 0, 722, 82))
        self.premDep.setObjectName("premDep")
        self.gridLayout_premDep = QtWidgets.QGridLayout(self.premDep)
        self.gridLayout_premDep.setObjectName("gridLayout_premDep")
        self.spinBox_premDep_vl = QtWidgets.QSpinBox(self.premDep)
        self.spinBox_premDep_vl.setObjectName("spinBox_premDep_vl")
        self.gridLayout_premDep.addWidget(self.spinBox_premDep_vl, 0, 3, 1, 1)
        self.label_premDep_epsa = QtWidgets.QLabel(self.premDep)
        self.label_premDep_epsa.setObjectName("label_premDep_epsa")
        self.gridLayout_premDep.addWidget(self.label_premDep_epsa, 1, 0, 1, 1)
        self.label_premDep_vl = QtWidgets.QLabel(self.premDep)
        self.label_premDep_vl.setObjectName("label_premDep_vl")
        self.gridLayout_premDep.addWidget(self.label_premDep_vl, 0, 2, 1, 1)
        self.spinBox_premDep_vsav = QtWidgets.QSpinBox(self.premDep)
        self.spinBox_premDep_vsav.setObjectName("spinBox_premDep_vsav")
        self.gridLayout_premDep.addWidget(self.spinBox_premDep_vsav, 0, 1, 1, 1)
        self.label_premDep_fpt = QtWidgets.QLabel(self.premDep)
        self.label_premDep_fpt.setObjectName("label_premDep_fpt")
        self.gridLayout_premDep.addWidget(self.label_premDep_fpt, 0, 4, 1, 1)
        self.spinBox_premDep_epsa = QtWidgets.QSpinBox(self.premDep)
        self.spinBox_premDep_epsa.setObjectName("spinBox_premDep_epsa")
        self.gridLayout_premDep.addWidget(self.spinBox_premDep_epsa, 1, 1, 1, 1)
        self.label_premDep_vsav = QtWidgets.QLabel(self.premDep)
        self.label_premDep_vsav.setObjectName("label_premDep_vsav")
        self.gridLayout_premDep.addWidget(self.label_premDep_vsav, 0, 0, 1, 1)
        self.label_premDep_heliSmur = QtWidgets.QLabel(self.premDep)
        self.label_premDep_heliSmur.setObjectName("label_premDep_heliSmur")
        self.gridLayout_premDep.addWidget(self.label_premDep_heliSmur, 1, 4, 1, 1)
        self.label_premDep_smur = QtWidgets.QLabel(self.premDep)
        self.label_premDep_smur.setObjectName("label_premDep_smur")
        self.gridLayout_premDep.addWidget(self.label_premDep_smur, 1, 2, 1, 1)
        self.spinBox_premDep_smur = QtWidgets.QSpinBox(self.premDep)
        self.spinBox_premDep_smur.setObjectName("spinBox_premDep_smur")
        self.gridLayout_premDep.addWidget(self.spinBox_premDep_smur, 1, 3, 1, 1)
        self.spinBox_premDep_fpt = QtWidgets.QSpinBox(self.premDep)
        self.spinBox_premDep_fpt.setObjectName("spinBox_premDep_fpt")
        self.gridLayout_premDep.addWidget(self.spinBox_premDep_fpt, 0, 5, 1, 1)
        self.spinBox_premDep_heliSmur = QtWidgets.QSpinBox(self.premDep)
        self.spinBox_premDep_heliSmur.setObjectName("spinBox_premDep_heliSmur")
        self.gridLayout_premDep.addWidget(self.spinBox_premDep_heliSmur, 1, 5, 1, 1)
        self.spinBox_premDep_ccf = QtWidgets.QSpinBox(self.premDep)
        self.spinBox_premDep_ccf.setObjectName("spinBox_premDep_ccf")
        self.gridLayout_premDep.addWidget(self.spinBox_premDep_ccf, 0, 7, 1, 1)
        self.label_premDep_ccf = QtWidgets.QLabel(self.premDep)
        self.label_premDep_ccf.setObjectName("label_premDep_ccf")
        self.gridLayout_premDep.addWidget(self.label_premDep_ccf, 0, 6, 1, 1)
        self.label_premDep_vpb = QtWidgets.QLabel(self.premDep)
        self.label_premDep_vpb.setObjectName("label_premDep_vpb")
        self.gridLayout_premDep.addWidget(self.label_premDep_vpb, 1, 6, 1, 1)
        self.spinBox_premDep_vpb = QtWidgets.QSpinBox(self.premDep)
        self.spinBox_premDep_vpb.setObjectName("spinBox_premDep_vpb")
        self.gridLayout_premDep.addWidget(self.spinBox_premDep_vpb, 1, 7, 1, 1)
        self.toolBox2.addItem(self.premDep, "")
        self.renforts = QtWidgets.QWidget()
        self.renforts.setGeometry(QtCore.QRect(0, 0, 722, 82))
        self.renforts.setObjectName("renforts")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.renforts)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.label__renforts_vpb = QtWidgets.QLabel(self.renforts)
        self.label__renforts_vpb.setObjectName("label__renforts_vpb")
        self.gridLayout_4.addWidget(self.label__renforts_vpb, 1, 6, 1, 1)
        self.label_renforts_vsav = QtWidgets.QLabel(self.renforts)
        self.label_renforts_vsav.setObjectName("label_renforts_vsav")
        self.gridLayout_4.addWidget(self.label_renforts_vsav, 0, 0, 1, 1)
        self.spinBox_renforts_vsav = QtWidgets.QSpinBox(self.renforts)
        self.spinBox_renforts_vsav.setObjectName("spinBox_renforts_vsav")
        self.gridLayout_4.addWidget(self.spinBox_renforts_vsav, 0, 1, 1, 1)
        self.label_renforts_vl = QtWidgets.QLabel(self.renforts)
        self.label_renforts_vl.setObjectName("label_renforts_vl")
        self.gridLayout_4.addWidget(self.label_renforts_vl, 0, 2, 1, 1)
        self.spinBox_renforts_vl = QtWidgets.QSpinBox(self.renforts)
        self.spinBox_renforts_vl.setObjectName("spinBox_renforts_vl")
        self.gridLayout_4.addWidget(self.spinBox_renforts_vl, 0, 3, 1, 1)
        self.label_renforts_fpt = QtWidgets.QLabel(self.renforts)
        self.label_renforts_fpt.setObjectName("label_renforts_fpt")
        self.gridLayout_4.addWidget(self.label_renforts_fpt, 0, 4, 1, 1)
        self.spinBox_renforts_fpt = QtWidgets.QSpinBox(self.renforts)
        self.spinBox_renforts_fpt.setObjectName("spinBox_renforts_fpt")
        self.gridLayout_4.addWidget(self.spinBox_renforts_fpt, 0, 5, 1, 1)
        self.label_renforts_epsa = QtWidgets.QLabel(self.renforts)
        self.label_renforts_epsa.setObjectName("label_renforts_epsa")
        self.gridLayout_4.addWidget(self.label_renforts_epsa, 1, 0, 1, 1)
        self.spinBox_renforts_epsa = QtWidgets.QSpinBox(self.renforts)
        self.spinBox_renforts_epsa.setObjectName("spinBox_renforts_epsa")
        self.gridLayout_4.addWidget(self.spinBox_renforts_epsa, 1, 1, 1, 1)
        self.label_renforts_smur = QtWidgets.QLabel(self.renforts)
        self.label_renforts_smur.setObjectName("label_renforts_smur")
        self.gridLayout_4.addWidget(self.label_renforts_smur, 1, 2, 1, 1)
        self.spinBox_renforts_smur = QtWidgets.QSpinBox(self.renforts)
        self.spinBox_renforts_smur.setObjectName("spinBox_renforts_smur")
        self.gridLayout_4.addWidget(self.spinBox_renforts_smur, 1, 3, 1, 1)
        self.label_renforts_heliSmur = QtWidgets.QLabel(self.renforts)
        self.label_renforts_heliSmur.setObjectName("label_renforts_heliSmur")
        self.gridLayout_4.addWidget(self.label_renforts_heliSmur, 1, 4, 1, 1)
        self.spinBox_renforts_heliSmur = QtWidgets.QSpinBox(self.renforts)
        self.spinBox_renforts_heliSmur.setObjectName("spinBox_renforts_heliSmur")
        self.gridLayout_4.addWidget(self.spinBox_renforts_heliSmur, 1, 5, 1, 1)
        self.label_renforts_ccf = QtWidgets.QLabel(self.renforts)
        self.label_renforts_ccf.setObjectName("label_renforts_ccf")
        self.gridLayout_4.addWidget(self.label_renforts_ccf, 0, 6, 1, 1)
        self.spinBox_renforts_ccf = QtWidgets.QSpinBox(self.renforts)
        self.spinBox_renforts_ccf.setObjectName("spinBox_renforts_ccf")
        self.gridLayout_4.addWidget(self.spinBox_renforts_ccf, 0, 7, 1, 1)
        self.spinBox_renforts_vpb = QtWidgets.QSpinBox(self.renforts)
        self.spinBox_renforts_vpb.setObjectName("spinBox_renforts_vpb")
        self.gridLayout_4.addWidget(self.spinBox_renforts_vpb, 1, 7, 1, 1)
        self.toolBox2.addItem(self.renforts, "")
        self.gridLayout_3.addWidget(self.toolBox2, 17, 0, 1, 7)
        self.label_nInter = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_nInter.setObjectName("label_nInter")
        self.gridLayout_3.addWidget(self.label_nInter, 0, 4, 1, 1)
        self.line = QtWidgets.QFrame(self.scrollAreaWidgetContents)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout_3.addWidget(self.line, 20, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem1, 5, 2, 1, 1)
        self.checkBox_gendarmerie = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBox_gendarmerie.setObjectName("checkBox_gendarmerie")
        self.gridLayout_3.addWidget(self.checkBox_gendarmerie, 21, 0, 1, 1)
        self.checkBox_erdf = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBox_erdf.setObjectName("checkBox_erdf")
        self.gridLayout_3.addWidget(self.checkBox_erdf, 21, 1, 1, 1)
        self.pushButton_addVehicle = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        icon = QtGui.QIcon.fromTheme("document-new")
        self.pushButton_addVehicle.setIcon(icon)
        self.pushButton_addVehicle.setObjectName("pushButton_addVehicle")
        self.gridLayout_3.addWidget(self.pushButton_addVehicle, 7, 6, 1, 1)
        self.spinBox_nInter = QtWidgets.QSpinBox(self.scrollAreaWidgetContents)
        self.spinBox_nInter.setObjectName("spinBox_nInter")
        self.gridLayout_3.addWidget(self.spinBox_nInter, 0, 6, 1, 1)
        self.label_commune = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_commune.setObjectName("label_commune")
        self.gridLayout_3.addWidget(self.label_commune, 4, 4, 1, 1)
        self.label_dateAppel = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_dateAppel.setObjectName("label_dateAppel")
        self.gridLayout_3.addWidget(self.label_dateAppel, 0, 0, 1, 1)
        self.dateTimeEdit_retour = QtWidgets.QDateTimeEdit(self.scrollAreaWidgetContents)
        self.dateTimeEdit_retour.setObjectName("dateTimeEdit_retour")
        self.gridLayout_3.addWidget(self.dateTimeEdit_retour, 2, 2, 1, 1)
        self.comboBox_commune = QtWidgets.QComboBox(self.scrollAreaWidgetContents)
        self.comboBox_commune.setObjectName("comboBox_commune")
        self.gridLayout_3.addWidget(self.comboBox_commune, 4, 6, 1, 1)
        self.lineEdit_lieu = QtWidgets.QLineEdit(self.scrollAreaWidgetContents)
        self.lineEdit_lieu.setObjectName("lineEdit_lieu")
        self.gridLayout_3.addWidget(self.lineEdit_lieu, 3, 6, 1, 1)
        self.label_17 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_17.setObjectName("label_17")
        self.gridLayout_3.addWidget(self.label_17, 3, 4, 1, 1)
        self.pushButton_removeVehicle = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        icon = QtGui.QIcon.fromTheme("document-close")
        self.pushButton_removeVehicle.setIcon(icon)
        self.pushButton_removeVehicle.setObjectName("pushButton_removeVehicle")
        self.gridLayout_3.addWidget(self.pushButton_removeVehicle, 10, 6, 1, 1)
        self.tableWidget_vehicleList = QtWidgets.QTableWidget(self.scrollAreaWidgetContents)
        self.tableWidget_vehicleList.setObjectName("tableWidget_vehicleList")
        self.tableWidget_vehicleList.setColumnCount(5)
        self.tableWidget_vehicleList.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicleList.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicleList.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicleList.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicleList.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicleList.setHorizontalHeaderItem(4, item)
        self.tableWidget_vehicleList.horizontalHeader().setDefaultSectionSize(100)
        self.gridLayout_3.addWidget(self.tableWidget_vehicleList, 6, 0, 5, 5)
        self.label_18 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_18.setObjectName("label_18")
        self.gridLayout_3.addWidget(self.label_18, 4, 0, 1, 1)
        self.dateTimeEdit_appel = QtWidgets.QDateTimeEdit(self.scrollAreaWidgetContents)
        self.dateTimeEdit_appel.setObjectName("dateTimeEdit_appel")
        self.gridLayout_3.addWidget(self.dateTimeEdit_appel, 0, 2, 1, 1)
        self.label_typeInter = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_typeInter.setObjectName("label_typeInter")
        self.gridLayout_3.addWidget(self.label_typeInter, 1, 4, 1, 1)
        self.label_16 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_16.setObjectName("label_16")
        self.gridLayout_3.addWidget(self.label_16, 2, 4, 1, 1)
        self.checkBox_maire = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBox_maire.setObjectName("checkBox_maire")
        self.gridLayout_3.addWidget(self.checkBox_maire, 21, 3, 1, 1)
        self.checkBox_grdf = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBox_grdf.setObjectName("checkBox_grdf")
        self.gridLayout_3.addWidget(self.checkBox_grdf, 21, 2, 1, 1)
        self.label_ncodis = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_ncodis.setObjectName("label_ncodis")
        self.gridLayout_3.addWidget(self.label_ncodis, 3, 0, 1, 1)
        self.spinBox = QtWidgets.QSpinBox(self.scrollAreaWidgetContents)
        self.spinBox.setObjectName("spinBox")
        self.gridLayout_3.addWidget(self.spinBox, 3, 2, 1, 1)
        self.checkBox_servicesEaux = QtWidgets.QCheckBox(self.scrollAreaWidgetContents)
        self.checkBox_servicesEaux.setObjectName("checkBox_servicesEaux")
        self.gridLayout_3.addWidget(self.checkBox_servicesEaux, 21, 6, 1, 1)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout_2.addWidget(self.scrollArea, 0, 0, 1, 3)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)
        MainWindow_ReportInter.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow_ReportInter)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 794, 30))
        self.menubar.setObjectName("menubar")
        MainWindow_ReportInter.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow_ReportInter)
        self.toolBox2.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow_ReportInter)

    def retranslateUi(self, MainWindow_ReportInter):
        _translate = QtCore.QCoreApplication.translate
        MainWindow_ReportInter.setWindowTitle(_translate("MainWindow_ReportInter", "PGLX - Rapport d\'intervention"))
        self.pushButton_cancel.setText(_translate("MainWindow_ReportInter", "Annuler"))
        self.pushButton_ok.setText(_translate("MainWindow_ReportInter", "Valider"))
        self.label_dateDepart.setText(_translate("MainWindow_ReportInter", "Heur de départ"))
        self.lineEdit_natureInter.setPlaceholderText(_translate("MainWindow_ReportInter", "Description du type d\'intervention"))
        self.textEdit.setHtml(_translate("MainWindow_ReportInter", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Noto Sans\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.textEdit.setPlaceholderText(_translate("MainWindow_ReportInter", "Rédaction du rapport"))
        self.lineEdit_demandeur.setPlaceholderText(_translate("MainWindow_ReportInter", "CODIS / SAMU / ..."))
        self.pushButton_editVehicle.setToolTip(_translate("MainWindow_ReportInter", "<html><head/><body><p>Édite l\'équipage du véhicule sélectionné (dans le tableau de gauche, ci-contre)</p></body></html>"))
        self.pushButton_editVehicle.setText(_translate("MainWindow_ReportInter", "Éditer"))
        self.label_dateRetour.setText(_translate("MainWindow_ReportInter", "Heure de retour"))
        self.checkBox_brigadeVerte.setText(_translate("MainWindow_ReportInter", "Brigade Verte"))
        self.label_listVehicleActions.setText(_translate("MainWindow_ReportInter", "Actions sur les véhicules"))
        self.premDep.setToolTip(_translate("MainWindow_ReportInter", "<html><head/><body><p>Moyens engagés des autres casernes au début de l\'alerte</p></body></html>"))
        self.label_premDep_epsa.setText(_translate("MainWindow_ReportInter", "EPSA"))
        self.label_premDep_vl.setText(_translate("MainWindow_ReportInter", "VL"))
        self.label_premDep_fpt.setText(_translate("MainWindow_ReportInter", "FPT(SR)"))
        self.label_premDep_vsav.setText(_translate("MainWindow_ReportInter", "VSAV"))
        self.label_premDep_heliSmur.setText(_translate("MainWindow_ReportInter", "HéliSMUR"))
        self.label_premDep_smur.setText(_translate("MainWindow_ReportInter", "SMUR"))
        self.label_premDep_ccf.setText(_translate("MainWindow_ReportInter", "CCF"))
        self.label_premDep_vpb.setToolTip(_translate("MainWindow_ReportInter", "Véhicule Porteur-Berce (Cellule)"))
        self.label_premDep_vpb.setText(_translate("MainWindow_ReportInter", "VPB"))
        self.toolBox2.setItemText(self.toolBox2.indexOf(self.premDep), _translate("MainWindow_ReportInter", "Premier Départ"))
        self.renforts.setToolTip(_translate("MainWindow_ReportInter", "<html><head/><body><p>Renforts demandés au cours de l\'intervention</p></body></html>"))
        self.label__renforts_vpb.setToolTip(_translate("MainWindow_ReportInter", "Véhicule Porteur-Berce (Cellule)"))
        self.label__renforts_vpb.setText(_translate("MainWindow_ReportInter", "VPB"))
        self.label_renforts_vsav.setText(_translate("MainWindow_ReportInter", "VSAV"))
        self.label_renforts_vl.setText(_translate("MainWindow_ReportInter", "VL"))
        self.label_renforts_fpt.setText(_translate("MainWindow_ReportInter", "FPT(SR)"))
        self.label_renforts_epsa.setText(_translate("MainWindow_ReportInter", "EPSA"))
        self.label_renforts_smur.setText(_translate("MainWindow_ReportInter", "SMUR"))
        self.label_renforts_heliSmur.setText(_translate("MainWindow_ReportInter", "HéliSMUR"))
        self.label_renforts_ccf.setText(_translate("MainWindow_ReportInter", "CCF"))
        self.toolBox2.setItemText(self.toolBox2.indexOf(self.renforts), _translate("MainWindow_ReportInter", "Renforts"))
        self.label_nInter.setText(_translate("MainWindow_ReportInter", "Intervention n°"))
        self.checkBox_gendarmerie.setText(_translate("MainWindow_ReportInter", "Gendarmerie"))
        self.checkBox_erdf.setText(_translate("MainWindow_ReportInter", "ERDF"))
        self.pushButton_addVehicle.setText(_translate("MainWindow_ReportInter", "Ajouter"))
        self.label_commune.setText(_translate("MainWindow_ReportInter", "Commune"))
        self.label_dateAppel.setText(_translate("MainWindow_ReportInter", "Heure d\'appel"))
        self.lineEdit_lieu.setPlaceholderText(_translate("MainWindow_ReportInter", "Lieu(x) de l\'intervention"))
        self.label_17.setText(_translate("MainWindow_ReportInter", "Localisation"))
        self.pushButton_removeVehicle.setToolTip(_translate("MainWindow_ReportInter", "<html><head/><body><p>Supprime le véhicule sélectionné (dans le tableau de gauche, ci-contre)</p></body></html>"))
        self.pushButton_removeVehicle.setText(_translate("MainWindow_ReportInter", "Supprimer"))
        item = self.tableWidget_vehicleList.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow_ReportInter", "ID"))
        item = self.tableWidget_vehicleList.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow_ReportInter", "Type"))
        item = self.tableWidget_vehicleList.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow_ReportInter", "Véhicule"))
        item = self.tableWidget_vehicleList.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow_ReportInter", "CA"))
        item = self.tableWidget_vehicleList.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow_ReportInter", "Conducteur"))
        self.label_18.setText(_translate("MainWindow_ReportInter", "Demandeur"))
        self.label_typeInter.setText(_translate("MainWindow_ReportInter", "Type d\'intervention"))
        self.label_16.setText(_translate("MainWindow_ReportInter", "Nature de l\'intervention:"))
        self.checkBox_maire.setText(_translate("MainWindow_ReportInter", "Maire"))
        self.checkBox_grdf.setText(_translate("MainWindow_ReportInter", "GRDF"))
        self.label_ncodis.setText(_translate("MainWindow_ReportInter", "N° CODIS"))
        self.checkBox_servicesEaux.setText(_translate("MainWindow_ReportInter", "Service des Eaux"))

