# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/db_import_export.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogDB(object):
    def setupUi(self, DialogDB):
        DialogDB.setObjectName("DialogDB")
        DialogDB.resize(431, 166)
        self.gridLayout = QtWidgets.QGridLayout(DialogDB)
        self.gridLayout.setObjectName("gridLayout")
        self.label_select_db = QtWidgets.QLabel(DialogDB)
        self.label_select_db.setObjectName("label_select_db")
        self.gridLayout.addWidget(self.label_select_db, 3, 0, 1, 1)
        self.label_version = QtWidgets.QLabel(DialogDB)
        self.label_version.setObjectName("label_version")
        self.gridLayout.addWidget(self.label_version, 2, 0, 1, 1)
        self.label_file_path = QtWidgets.QLabel(DialogDB)
        self.label_file_path.setObjectName("label_file_path")
        self.gridLayout.addWidget(self.label_file_path, 0, 0, 1, 1)
        self.label_file_path_value = QtWidgets.QLabel(DialogDB)
        self.label_file_path_value.setObjectName("label_file_path_value")
        self.gridLayout.addWidget(self.label_file_path_value, 0, 2, 1, 1)
        self.toolButton_file_path = QtWidgets.QToolButton(DialogDB)
        self.toolButton_file_path.setObjectName("toolButton_file_path")
        self.gridLayout.addWidget(self.toolButton_file_path, 0, 3, 1, 1)
        self.label_version_value = QtWidgets.QLabel(DialogDB)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_version_value.sizePolicy().hasHeightForWidth())
        self.label_version_value.setSizePolicy(sizePolicy)
        self.label_version_value.setObjectName("label_version_value")
        self.gridLayout.addWidget(self.label_version_value, 2, 2, 1, 2)
        self.checkBox_user_db = QtWidgets.QCheckBox(DialogDB)
        self.checkBox_user_db.setChecked(True)
        self.checkBox_user_db.setObjectName("checkBox_user_db")
        self.gridLayout.addWidget(self.checkBox_user_db, 3, 2, 1, 2)
        self.checkBox_db = QtWidgets.QCheckBox(DialogDB)
        self.checkBox_db.setChecked(True)
        self.checkBox_db.setObjectName("checkBox_db")
        self.gridLayout.addWidget(self.checkBox_db, 4, 2, 1, 2)
        self.buttonBox = QtWidgets.QDialogButtonBox(DialogDB)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 5, 2, 1, 2)

        self.retranslateUi(DialogDB)
        self.buttonBox.rejected.connect(DialogDB.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogDB)

    def retranslateUi(self, DialogDB):
        _translate = QtCore.QCoreApplication.translate
        DialogDB.setWindowTitle(_translate("DialogDB", "Dialog"))
        self.label_select_db.setText(_translate("DialogDB", "Sélections des bases de donneés"))
        self.label_version.setText(_translate("DialogDB", "Version :"))
        self.label_file_path.setText(_translate("DialogDB", "Chemin de l\'archive"))
        self.label_file_path_value.setText(_translate("DialogDB", "TextLabel"))
        self.toolButton_file_path.setText(_translate("DialogDB", "..."))
        self.label_version_value.setText(_translate("DialogDB", "TextLabel"))
        self.checkBox_user_db.setText(_translate("DialogDB", "Base de données utilisateurs"))
        self.checkBox_db.setText(_translate("DialogDB", "Base de données des rapports"))

