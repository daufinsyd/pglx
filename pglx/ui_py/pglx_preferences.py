# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pglx/ui/pglx_preferences.ui'
#
# Created by: PyQt5 UI code generator 5.8
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindowPreferences(object):
    def setupUi(self, MainWindowPreferences):
        MainWindowPreferences.setObjectName("MainWindowPreferences")
        MainWindowPreferences.resize(379, 293)
        self.centralwidget = QtWidgets.QWidget(MainWindowPreferences)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_title = QtWidgets.QLabel(self.centralwidget)
        self.label_title.setObjectName("label_title")
        self.verticalLayout.addWidget(self.label_title)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_admin = QtWidgets.QWidget()
        self.tab_admin.setObjectName("tab_admin")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab_admin)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.checkBox_vehicleManagementShowAll = QtWidgets.QCheckBox(self.tab_admin)
        self.checkBox_vehicleManagementShowAll.setObjectName("checkBox_vehicleManagementShowAll")
        self.verticalLayout_2.addWidget(self.checkBox_vehicleManagementShowAll)
        self.tabWidget.addTab(self.tab_admin, "")
        self.verticalLayout.addWidget(self.tabWidget)
        self.buttonBox = QtWidgets.QDialogButtonBox(self.centralwidget)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        MainWindowPreferences.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindowPreferences)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 379, 30))
        self.menubar.setObjectName("menubar")
        MainWindowPreferences.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindowPreferences)
        self.statusbar.setObjectName("statusbar")
        MainWindowPreferences.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindowPreferences)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindowPreferences)

    def retranslateUi(self, MainWindowPreferences):
        _translate = QtCore.QCoreApplication.translate
        MainWindowPreferences.setWindowTitle(_translate("MainWindowPreferences", "Préférences de PGLX"))
        self.label_title.setText(_translate("MainWindowPreferences", "Mes préférences"))
        self.checkBox_vehicleManagementShowAll.setText(_translate("MainWindowPreferences", "Montrer tous les véhicules"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_admin), _translate("MainWindowPreferences", "Administration"))

