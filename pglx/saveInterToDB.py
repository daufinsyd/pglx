from pglx.models import *
from PyQt5 import QtCore, QtWidgets
from datetime import datetime


def interSaveToDb(self, edit):

    print('[II] Enregistrement des données dans la base de donnée en cours ...')
    nbInter = self.spinBox_nInter.value()
    ncodis = self.spinBox.value()
    date_appel = QtCore.QDateTime(self.dateTimeEdit_appel.dateTime()).toString(1)
    date_appel = date_appel.replace("T", " ")  # remplace le caractère T par un espace pour avoir %Y-%m-%d %H:%M:%S
    date_depart = QtCore.QDateTime(self.dateTimeEdit_depart.dateTime()).toString(1).replace("T", " ")
    #date_depart = datetime.datetime.strptime(date_depart, '%Y-%m-%d %H:%M:%S')
    date_fin = QtCore.QDateTime(self.dateTimeEdit_retour.dateTime()).toString(1).replace("T", " ")
    typeInter = self.comboBox_typeInter.currentIndex()
    natureInter = self.lineEdit_natureInter.text()
    localisation = self.lineEdit_lieu.text()
    demandeur = self.lineEdit_demandeur.text()
    commune = self.comboBox_commune.currentIndex()  # prend la position de la commune
    commune = Commune.get(id=commune+1)  # prend la commune +1 car id commence à 1

    gendarmerie = self.checkBox_gendarmerie.isChecked()
    erdf = self.checkBox_erdf.isChecked()
    brigadeVerte = self.checkBox_brigadeVerte.isChecked()
    grdf = self.checkBox_grdf.isChecked()
    servicesEaux = self.checkBox_servicesEaux.isChecked()
    maire = self.checkBox_maire.isChecked()

    rapport = self.textEdit.toPlainText()

    redacteur = self.loginUser  # le rédacteur est l'utilisateur actuellement enregistré

    if edit:
        # If in edition mode
        inter = Interventions.get(id=self.modeAccesRapport[1])
        print('[II] Modification de l\'intervention:', inter)

        # traitement des 1erDep:
        inter.nInter = self.spinBox_nInter.value()
        inter.renforts.vl = self.spinBox_renforts_vl.value()
        inter.renforts.epsa = self.spinBox_renforts_epsa.value()
        inter.renforts.fptsr = self.spinBox_renforts_fpt.value()
        inter.renforts.vsav = self.spinBox_renforts_vsav.value()
        inter.renforts.smur = self.spinBox_renforts_smur.value()
        inter.renforts.heliSmur = self.spinBox_renforts_heliSmur.value()
        inter.renforts.ccf = self.spinBox_renforts_ccf.value()
        inter.renforts.vpb = self.spinBox_renforts_vpb.value()
        inter.renforts.save()

        inter.premierDepart.vl = self.spinBox_premDep_vl.value()
        inter.premierDepart.vsav = self.spinBox_premDep_vsav.value()
        inter.premierDepart.epsa = self.spinBox_premDep_epsa.value()
        inter.premierDepart.fptsr = self.spinBox_premDep_fpt.value()
        inter.premierDepart.smur = self.spinBox_premDep_smur.value()
        inter.premierDepart.heliSmur = self.spinBox_premDep_heliSmur.value()
        inter.premierDepart.ccf = self.spinBox_premDep_ccf.value()
        inter.premierDepart.vpb = self.spinBox_premDep_vpb.value()
        inter.premierDepart.save()

        # inter.nInter=nbInter  #ne pas redéfinir car calculé à parti du nombre d'inter listée
        inter.ncodis=ncodis
        inter.date_appel=date_appel
        inter.date_depart=date_depart
        inter.date_fin=date_fin
        inter.typeInter=typeInter
        inter.natureInter=natureInter
        inter.commune = commune
        inter.localisation=localisation
        inter.demandeur=demandeur
        inter.gendarmerie=gendarmerie
        inter.erdf=erdf
        inter.brigadeVerte=brigadeVerte
        inter.grdf=grdf
        inter.servicesEaux=servicesEaux
        inter.maire=maire
        inter.rapport=rapport
        inter.redacteur = redacteur  # Todo: inutile puisque seul le rédacteur peut modifier le rapport
        inter.dateRedaction=datetime.now()

        inter.save()
    else:
        # traitement des 1erDep:
        renforts = Renforts.create(vl=self.spinBox_renforts_vl.value(), epsa=self.spinBox_renforts_epsa.value(),
                        fptsr=self.spinBox_renforts_fpt.value(), vsav=self.spinBox_renforts_vsav.value(),
                        smur=self.spinBox_renforts_smur.value(), heliSmur=self.spinBox_renforts_heliSmur.value(),
                        ccf=self.spinBox_renforts_ccf.value(), vpb=self.spinBox_renforts_vpb.value()
        )
        # traitement renforts:
        premierDepart = PremierDepart.create(vl=self.spinBox_premDep_vl.value(), epsa=self.spinBox_premDep_epsa.value(),
                            fptsr=self.spinBox_premDep_fpt.value(), vsav=self.spinBox_premDep_vsav.value(),
                            smur=self.spinBox_premDep_smur.value(), heliSmur=self.spinBox_premDep_heliSmur.value(),
                            ccf=self.spinBox_premDep_ccf.value(), vpb=self.spinBox_premDep_vpb.value()
        )

        inter = Interventions.create(nInter=nbInter, ncodis=ncodis, date_appel=date_appel, date_depart=date_depart,
                                     date_fin=date_fin, typeInter=typeInter, natureInter=natureInter,
                                     localisation=localisation, demandeur=demandeur,
                                     gendarmerie=gendarmerie, erdf=erdf, brigadeVerte=brigadeVerte, grdf=grdf,
                                     servicesEaux=servicesEaux, maire=maire, rapport=rapport,
                                     premierDepart=premierDepart, renforts=renforts,
                                     dateRedaction=datetime.now(), commune=commune, redacteur=redacteur)

    # If edit: save all FPT, VL, VTU and check at the end if they have been deleted by the user.
    # If yes, we delete them
    if self.modeAccesRapport[0] == 1:
        previousFptList = inter.FPTL_inter
        previousVlList = inter.VL_inter
        previousVtuList = inter.VTU_inter

    # New lists are use for the comparaison on editing inter
    newFptList = []
    newVlList = []
    newVtuList = []
    # Create or Update vehicles
    for win in self.windowsList:
        type = win.comboBox_type.currentText().split('-')[0]

        # Mendatory and mutual items
        vehiId = win.comboBox_type.currentData()
        ca = win.comboBox_ca.currentData()
        con = win.comboBox_con.currentData()
        # mutual item
        stag = win.comboBox_stag.currentData()

        if type == "FPT":
            vehi = VehicleFPT.get(id=vehiId)  # Get "Physical" vehicle
            ceBat = win.comboBox_ceBat.currentData()
            ceBal = win.comboBox_ceBal.currentData()
            equBat = win.comboBox_equBat.currentData()
            equBal = win.comboBox_equBal.currentData()
            try:
                vehiInter = FPTL.get(inter=inter, vehicle=vehi)  # Get or create relation between vehicle and inter
                vehiInter.vehicle = vehi
                vehiInter.con = con
                vehiInter.ca = ca
                vehiInter.ce = ceBat
                vehiInter.ce2 = ceBal
                vehiInter.equ = equBat
                vehiInter.equ2 = equBal
                vehiInter.stag = stag
                vehiInter.save()
            except FPTL.DoesNotExist:
                print("Création ManyToMany FPT")
                try:
                    vehiInter = FPTL.create(con=con, ca=ca, ce=ceBat, ce2=ceBal, equ=equBat, equ2=equBal, stag=stag,
                                            inter=inter, vehicle=vehi)
                except Exception as e:
                    print("[EE] Impossible de créer un FPT inter", e)
            finally:
                newFptList.append(vehiInter.id)

        elif type == "VL":
            vehi = VehicleVL.get(id=vehiId)  # Get "Physical" vehicle
            equ = win.comboBox_equ.currentData()
            try:
                vehiInter = VL.get(inter=inter, vehicle=vehi)  # Get or create relation between vehicle and inter
                vehiInter.vehicle = vehi
                vehiInter.con = con
                vehiInter.ca = ca
                vehiInter.equ = equ
                vehiInter.save()
            except VL.DoesNotExist:
                print("Création ManyToMany VL")
                try:
                    vehiInter = VL.create(con=con, ca=ca, equ=equ, stag=stag, inter=inter, vehicle=vehi)
                except Exception as e:
                    print("[EE] Impossible de créer une VL inter", e)
            finally:
                newVlList.append(vehiInter.id)

        elif type == "VTU":
            vehi = VehicleVTU.get(id=vehiId)  # Get "Physical" vehicle
            equ = win.comboBox_equ.currentData()
            try:
                vehiInter = VTU.get(inter=inter, vehicle=vehi)  # Get or create relation between vehicle and inter
                vehiInter.vehicle = vehi
                vehiInter.con = con
                vehiInter.ca = ca
                vehiInter.equ = equ
                vehiInter.save()
            except VTU.DoesNotExist:
                print("Création ManyToMany VTU")
                try:
                    vehiInter = VTU.create(con=con, ca=ca, equ=equ, stag=stag, inter=inter, vehicle=vehi)
                except Exception as e:
                    print("[EE] Impossible de créer une VTU inter", e)
            finally:
                newVtuList.append(vehiInter.id)

    # Delete removed vehicles by the user
    # (aka vehicle which were in previousList but were added in newList)
    if self.modeAccesRapport[0] == 1:
        for vehicle in previousFptList:
            if vehicle.id not in newFptList:
                vehicle.delete_instance()
        for vehicle in previousVlList:
            if vehicle.id not in newVlList:
                vehicle.delete_instance()
        for vehicle in previousVtuList:
            if vehicle.id not in newVtuList:
                vehicle.delete_instance()


    print("Rapport enregistré")
    return inter
