import os
import tarfile
import datetime
import json

import shutil

from PyQt5.QtWidgets import QMessageBox

from pglx.migrations import *
from pglx.migrations import migration_dummy_000, migration_dummy_001, migration_005_bip_personnel
from pglx.models import BDMeta, BDUserMeta

from pglx import logger

DESC_FILE_NAME = "database.pglxdesc"


def create_desc_file(current_db_version, export_db, export_user_db):
    with open(DESC_FILE_NAME, mode="w") as file:
        data = {
            'CURRENT_DB_VERSION': current_db_version,
            'export_datetime': str(datetime.datetime.now()),
            'exported_db': {
                'db': export_db,
                'user_db': export_user_db,
            },
            'BD': {
                'version': BDMeta.get(id=1).version,
            },
            "BDUser": {
                'version': BDUserMeta.get(id=1).version,
            },
        }
        json.dump(data, file, ensure_ascii=False)


def read_desc_file(file_path_to_archive):
    tar = tarfile.open(file_path_to_archive, mode="r")
    desc = tar.getmember("database.pglxdesc")
    ext_desc = tar.extractfile(desc)
    json_ = json.load(ext_desc)
    return json_


def export_database(file_path, current_db_version, export_user_db=True, export_db=True, path_to_pglx="./"):
    create_desc_file(current_db_version, export_db, export_user_db)
    export_db = tarfile.open("database.pglxdb", mode="w")
    export_db.add(DESC_FILE_NAME)
    if export_user_db:
        export_db.add(path_to_pglx + "/pglx/user_db.sqlite", arcname="user_db.sqlite")
    if export_db:
        export_db.add(path_to_pglx + "/pglx/db.sqlite", arcname="db.sqlite")
    export_db.close()
    try:
        # shutil work better than os when src and dst are not on the same file system
        shutil.move("database.pglxdb", file_path)
        # clean up
        os.remove(DESC_FILE_NAME)

    except OSError as e:
        QMessageBox.critical(None, "Erreur", "Une erreur système a été rencontrée : " + str(e))
        logger.exception(e)

        return 2

    except Exception as e:
        QMessageBox.critical(None, "Erreur", "Une erreur a été rencontrée : " + str(e))
        logger.exception(e)

        return 1

    return 0


def save_current_dbs(current_db_version):
    export_database("database_save.pglxdb", current_db_version, True, True)


def import_database(file_path, current_db_version, import_db=True, import_user_db=True, path_to_pglx="./"):
    save_current_dbs(current_db_version)
    tar = tarfile.open(file_path, mode="r")
    if import_db:
        db = tar.getmember("db.sqlite")
        tar.extractall(path_to_pglx + "/pglx/", members=[db])
    if import_user_db:
        user_db = tar.getmember("user_db.sqlite")
        tar.extractall(path_to_pglx + "/pglx/", members=[user_db])
    return 0


def check_versions(current_db_version):
    up_to_date = True
    try:
        bdV = BDMeta.get(id=1).version
        bdUV = BDUserMeta.get(id=1).version

        if bdV < current_db_version or bdUV < current_db_version:
            """QMessageBox.warning(None, "Version dépréciée", "La version " + str(bdV) + " de la base de données n'est pas"
                                                                                      " à jour. Veuillez appliquer les migrations requises.\nVersion actuelle : " + str(
                current_db_version)
                                + "\nPour migrer la base de données : cliquez sur \"mettre à jour la base de données\""
                                  " dans le panneau \"Administration\"")
            """
            up_to_date = False

    except Exception as e:
        print(str(e))
        QMessageBox.critical(None, "Impossible de continuer", "Cette intervention utilise un version antérieure"
                             + "de PGLX. Veuillez re-saisir à la main des véhicules utilisés et les rôles des SP.\nErreur :\n" + str(e))
        up_to_date = False

    return up_to_date


def migrate_db(current_version):
    bdV = 0
    bdUV = 0
    try:
        bdV = BDMeta.get(id=1).version
        bdUV = BDUserMeta.get(id=1).version
    except Exception as e:
        print(str(e))
        pass

    if bdV == 0:
        migration_dummy_000.migrate_000()
    elif bdV == 1:
        migration_dummy_001.migrate_001()
    elif bdV == 2:
        migration_005_bip_personnel.migrate_005()

