import unittest
from pglx.models import Personnel, Caserne
import pglx.pglx_qt as pglx_qt


class TestPermsMethods(unittest.TestCase):
    # TODO setup & tear down
    def test_has_perms(self):
        nobody = Personnel.get(id=1)
        admin = Personnel.get(id=2)
        caserne = Caserne.get(id=1)
        self.assertFalse(pglx_qt.fonctions.userHasPerm(3, user=nobody, caserne=caserne))
        self.assertFalse(pglx_qt.fonctions.userHasPerm(2, user=nobody, caserne=caserne))
        self.assertFalse(pglx_qt.fonctions.userHasPerm(1, user=nobody, caserne=caserne))
        self.assertTrue(pglx_qt.fonctions.userHasPerm(0, user=nobody, caserne=caserne))

        self.assertTrue(pglx_qt.fonctions.userHasPerm(3, user=admin, caserne=caserne))
        self.assertTrue(pglx_qt.fonctions.userHasPerm(2, user=admin, caserne=caserne))
        self.assertTrue(pglx_qt.fonctions.userHasPerm(1, user=admin, caserne=caserne))
        self.assertTrue(pglx_qt.fonctions.userHasPerm(0, user=admin, caserne=caserne))


if __name__ == "__main__":
    unittest.main()
