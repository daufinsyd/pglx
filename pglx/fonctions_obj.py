from pglx.fonctions import *
import os
from pglx.models import Personnel

def addDriverCOD2_comboBox(self, combobox):
    for pers in Personnel.filter(Personnel.COD2==True):
        item = str(pers.lastName) + ' ' + str(pers.firstName)
        combobox.addItem(item)

def addDriverCOD1_comboBox(self, combobox):
    for pers in Personnel.filter(Personnel.COD1==True):
        item = str(pers.lastName) + ' ' + str(pers.firstName)
        combobox.addItem(item)

def addDriver_comboBox(self, combobox):
    for pers in Personnel.filter(Personnel.VL==True):
        item = str(pers.lastName) + ' ' + str(pers.firstName)
        combobox.addItem(item)

def addTrainee_comboBox(self, combobox): #Ajoute à combobox les items présents dans le fichier file_items (lui même présent dans path_to_pglx)
    for pers in Personnel.filter(Personnel.grade<=1):
        item = str(pers.lastName) + ' ' + str(pers.firstName)
        combobox.addItem(item)

def addFireman_comboBox(self, combobox, grade): #Ajoute à combobox les items présents dans le fichier file_items (lui même présent dans path_to_pglx)
    for pers in Personnel.filter((Personnel.grade>=grade) | (Personnel.grade==-1)):
        item = str(pers.lastName) + ' ' + str(pers.firstName)
        combobox.addItem(item)

"""
renvoit la position des pompiers ayant pour id id dans la combobox via un dictionnaire:
{'idPompier':'Position'}
S'oppose à addFireman_comboBox
Différent de getFireman...
"""
def getFiremen_comboBoxById(self, grade):
    dico = {}
    position = 0
    for pers in Personnel.filter((Personnel.grade>=grade) | (Personnel.grade==-1)):
        id = pers.id
        dico.update({id:position})
        position += 1
    return dico

def addFiremanInRank_comboBox(self, combobox, gradeInf, gradeSup):
    for pers in Personnel.filter((Personnel.grade>=grade) and (Personnel.grade<=grade)| (Personnel.grade==-1)):
        item = str(pers.lastName) + ' ' + str(pers.firstName)
        combobox.addItem(item)

def addItems_comboBox(self, combobox, *items): #Ajoute à combobox les items présents dans le fichier file_items (lui même présent dans path_to_pglx)
    for item in items:
        combobox.addItem(item)

def addItemList_comboBox(self, combobox, list): #Ajoute à combobox les items présents dans le fichier file_items (lui même présent dans path_to_pglx)
    for item in list:
        combobox.addItem(item)


"""
    Pompier-GLX
    Copyright (C) 2013-2015  Sydney Rodolphe Torcuato Gems

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
