import textwrap


def display_dict(dict_, txt="", indent=0):
    """
    Takes a dict {k, v} where v is a dict {'value': ...,} or an end value.
    Warning : do not get mixed up with v and 'value' !
    :param indent:
    :param dict_: {'value': ..., 'name': "Human name of the value", 'subvalue': "bool if current_value['value']
        should be passed (incrementally) to display_dict.
    :param txt: current txt to which append values
    :return: txt with appended values
    """
    indent_char = "\t"

    if 'name' in dict_:
        txt += textwrap.indent(dict_['name'] + " ", indent_char * indent)

    for v in dict_.values():
        if 'name' in v:
            txt += textwrap.indent(str(v['name']) + " : ", indent_char * indent)

        if type(v) is dict and 'subvalue' in v and v['subvalue'] is True:
            """
            since display_dict append values to txt, here we have to put values inside txt. Otherwise, we add
            all the value (current + next one) to the current and we have 2times current values + 1 time next ones.
            """
            txt += '\n'
            txt = display_dict(v['value'], txt, indent=indent + 1)
        elif type(v) is dict:
            if 'value' in v:
                txt += textwrap.indent(str(v['value']), indent_char * indent)
            txt += "\n"
        else:
            txt += textwrap.indent(str(v), indent_char * indent)

    return txt
