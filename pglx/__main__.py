import sys
import pglx.pglx_qt as pglx_qt


def main(args=None):
    """The main routine."""
    if args is None:
        args = sys.argv[1:]

    print("PGLX.")

    pglx_qt.start()


if __name__ == "__main__":
    main()

