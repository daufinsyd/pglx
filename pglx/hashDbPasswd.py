#!/bin/python3
# Hash les mots de passes de la base de données

import bcrypt
from peewee import *

user_db = SqliteDatabase('user_db.sqlite')

# Définition de la classe
# ATTENTION A METTRE A JOUR SI models.py L'EST!
class Personnel(Model):
    grade = IntegerField()
    matricule = IntegerField()
    type = IntegerField()  # administratif / actif / vétérant
    firstName = CharField(max_length=30)
    lastName = CharField(max_length=30)
    COD1 = BooleanField(default=False)
    COD2 = BooleanField(default=False)
    VL = BooleanField(default=False)
    involvementDate = DateTimeField(null=True)
    birthDate = DateTimeField(null=True)

    rank = IntegerField(default=1)
    """
    -1 ~ ne peut rien faire
     0 peut voir rapports,
     1 peut écrire rapports,
     2 peut éditer diverses choses (lesquelles ?) (modérateur),
     3 administrateur
    """

    # UTILSIATEUR (devrait être dans une classe à part)
    password = CharField()


    class Meta:
        database = user_db  # This model uses the "people.db" database.


print("Vérification du hashage des mots de passes utilisateurs")
for user in Personnel.select():
    print("Traitement de l'utilisateur :", user.id)
    try:
        hash = b"a".encode()
        bcrypt.hashpw(hash, user.password.encode()) == user.password.encode()
        print("Ok")
    except:
        print("Hashage du mot de passe")
        pwd = user.password.encode()
        hashedPwd = bcrypt.hashpw(pwd, bcrypt.gensalt())
        user.password = hashedPwd
        user.save()
        print("Hashé")
