import os, subprocess


def delete_all_file(path, *args):
    try:
        os.chdir(path)
        for i in args:
            subprocess.call(["rm", i])
    except:
        print('[WW] Impossible de supprimer les fichiers dans le dossier:', path)

"""
    Pompier-GLX
    Copyright (C) 2013-2018 Sydney Rodolphe Torcuato Gems

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
