import os
import subprocess
import datetime
import tempfile
import threading

from PyQt5.QtWidgets import QMessageBox

from pglx import pglx_qt

# cf pglx_qt.py
TYPE_INTER = ('Accident de la circulation', 'Destruction d\'hymenopteres', 'Divers', 'Incendie', 'Secours a personne',
              'Piquet',
              )


def insert_newlines(string, every=110):  # max ~128
    """
    Insert a new line every 64 characters
    :param string:
    :param every:
    :return:
    """
    return '\n\t'.join(string[i:i + every] for i in range(0, len(string), every))


def check_and_create_folder(path, folder_name=""):
    if not os.path.exists(path + "/" + folder_name):
        os.makedirs(path + "/" + folder_name)


def rediger(inter, path_to_rinter, modeAccessRapport):
    """
    Juste write recently created (or edited) intervention's report
    :param modeAccessRapport:
    :param inter:
    :param path_to_rinter:
    :return:
    """

    print("Rédaction en cours ...")
    print("Récupération des variables dans les fichiers")

    nombre = inter.nInter
    print('Intervention N°:', nombre)

    heure_appel = inter.date_appel
    heure_depart = inter.date_depart
    heure_fin = inter.date_fin
    ncodis = inter.ncodis
    type_inter = inter.typeInter
    type_inter = TYPE_INTER[type_inter]

    nature_inter = inter.natureInter
    loc_inter = inter.localisation
    demandeur_inter = inter.demandeur
    try:
        commune = inter.commune
        commune = commune.name
    except Exception as e:
        commune = ''
        pglx_qt.logger.critical("in rediger: " + str(e))

    whitespace = "\t"  # marge du document

    premier_depart = inter.premierDepart

    vsav_1erDepart = premier_depart.vsav
    fptsr_1erDepart = premier_depart.fptsr
    epsa_1erDepart = premier_depart.epsa
    vl_1erDepart = premier_depart.vl
    smur_1erDepart = premier_depart.smur
    helismur_1erDepart = premier_depart.heliSmur
    ccf_1erDepart = premier_depart.ccf
    vpb_1erDepart = premier_depart.vpb

    renforts = inter.renforts

    vsav_renfort = renforts.vsav
    fptsr_renfort = renforts.fptsr
    epsa_renfort = renforts.epsa
    vl_renfort = renforts.vl
    smur_renfort = renforts.smur
    helismur_renfort = renforts.heliSmur
    ccf_renfort = renforts.ccf
    vpb_renfort = renforts.vpb

    gendarmerie = inter.gendarmerie
    edf = inter.erdf
    gdf = inter.grdf
    brigade_verte = inter.brigadeVerte
    service_eaux = inter.servicesEaux
    maire = inter.maire

    rapport = inter.rapport
    rapport = rapport.replace("\n", "\n" + whitespace)

    print("Ecriture du rapport en cours ...")

    type_rapport = "Intervention"

    ligne1 = type_rapport + ': ' + 'N° ' + str(nombre) + '  |  N° CODIS: ' + str(ncodis) + "\n"
    ligne2 = 'Date d\'appel: ' + str(heure_appel) + '\n' + '\tDate de départ: ' + str(
        heure_depart) + '\n' + '\tDate de retour: ' + str(
        heure_fin) + '\n' + '\n'  # espaces mis ici pour la marge car on écrit la ligne qui  comporte des retour à la ligne
    ligne3_bis = "Véhicules: "
    ligne3 = ""  # ligne3 sert à écrire les véhicules dans .dataInters.pglxdi et ne veut donc pas 'Véhicules'

    ligne3 += "\n"
    ligne4 = 'Nature: ' + str(nature_inter).rstrip('\n') + "\n"
    ligne5 = 'Localisation: ' + str(loc_inter).rstrip('\n') + "\n"
    ligne6 = 'Demandeur: ' + str(demandeur_inter).rstrip('\n') + "\n"
    ligne7 = '>>Sapeur-Pompiers & Fonctions'

    # whitespace est défini plus haut car utilisé dans spv_cas et spv_sll

    fichier = tempfile.NamedTemporaryFile('w',
                                          delete=False)  # delete=False prevent file from being deleted at f.close()
    # This is mandatory since the file has to be closed before it's copied

    fichier.write(whitespace)
    fichier.write(ligne1)
    fichier.write(whitespace)
    fichier.write(ligne2)
    fichier.write(whitespace)
    fichier.write(ligne3_bis)
    fichier.write(whitespace)
    fichier.write(ligne3)
    fichier.write("\n")
    fichier.write(whitespace)
    fichier.write(ligne4)
    fichier.write(whitespace)
    fichier.write(type_inter)
    fichier.write('\n')
    fichier.write(whitespace)
    fichier.write(commune)
    fichier.write('      ')
    fichier.write(ligne5)
    fichier.write(whitespace)
    fichier.write(ligne6)
    fichier.write("\n" + "\n")
    fichier.write(whitespace)
    fichier.write(ligne7)

    fichier.write('\n')
    list_vehi_inter = inter.FPTL_inter
    for v in list_vehi_inter:
        fichier.write(whitespace + v.vehicle.toString() + '\n')
        fichier.write(whitespace + "\tCA: " + v.ca.toString() + '\n')
        fichier.write(whitespace + "\tConducteur: " + v.con.toString() + '\n')
        fichier.write(whitespace + "\tCE BAT: " + v.ce.toString() + '\n')
        fichier.write(whitespace + "\tCE BAL: " + v.ce2.toString() + '\n')
        fichier.write(whitespace + "\tEQU BAT: " + v.equ.toString() + '\n')
        fichier.write(whitespace + "\tEQU BAL: " + v.equ2.toString() + '\n')
        fichier.write(whitespace + "\tStagiaire: " + v.stag.toString() + '\n')
        fichier.write('\n')

    list_vehi_inter = inter.VL_inter
    for v in list_vehi_inter:
        fichier.write(whitespace + v.vehicle.toString() + '\n')
        fichier.write(whitespace + "\tCA: " + v.ca.toString() + '\n')
        fichier.write(whitespace + "\tConducteur: " + v.con.toString() + '\n')
        fichier.write(whitespace + "\tEQU: " + v.equ.toString() + '\n')
        fichier.write(whitespace + "\tStagiaire: " + v.stag.toString() + '\n')
        fichier.write('\n')

    list_vehi_inter = inter.VTU_inter
    for v in list_vehi_inter:
        fichier.write(whitespace + v.vehicle.toString() + '\n')
        fichier.write(whitespace + "\tCA: " + v.ca.toString() + '\n')
        fichier.write(whitespace + "\tConducteur: " + v.con.toString() + '\n')
        fichier.write(whitespace + "\tEQU: " + v.equ.toString() + '\n')
        fichier.write(whitespace + "\tStagiaire: " + v.stag.toString() + '\n')
        fichier.write('\n')

    fichier.write('\n')
    fichier.write(whitespace)
    fichier.write(">>Véhicules au 1e départ\n")

    if vsav_1erDepart != 0:
        fichier.write(whitespace)
        fichier.write("VSAV:" + str(vsav_1erDepart) + '\n')
    if fptsr_1erDepart != 0:
        fichier.write(whitespace)
        fichier.write("FPTSR:" + str(fptsr_1erDepart) + '\n')
    if epsa_1erDepart != 0:
        fichier.write(whitespace)
        fichier.write("EPSA:" + str(epsa_1erDepart) + '\n')
    if vl_1erDepart != 0:
        fichier.write(whitespace)
        fichier.write("VL:" + str(vl_1erDepart) + '\n')
    if smur_1erDepart != 0:
        fichier.write(whitespace)
        fichier.write("SMUR:" + str(smur_1erDepart) + '\n')
    if helismur_1erDepart != 0:
        fichier.write(whitespace)
        fichier.write("HELI SMUR:" + str(helismur_1erDepart) + '\n')
    if ccf_1erDepart != 0:
        fichier.write(whitespace)
        fichier.write("CCF:" + str(ccf_1erDepart) + '\n')
    if vpb_1erDepart != 0:
        fichier.write(whitespace)
        fichier.write("VPB:" + str(vpb_1erDepart) + '\n')

    fichier.write('\n')
    fichier.write(whitespace)
    fichier.write(">>Véhicules en renfort\n")

    if vsav_renfort != 0:
        fichier.write(whitespace)
        fichier.write("VSAV:" + str(vsav_renfort) + '\n')
    if fptsr_renfort != 0:
        fichier.write(whitespace)
        fichier.write("FPTSR:" + str(fptsr_renfort) + '\n')
    if epsa_renfort != 0:
        fichier.write(whitespace)
        fichier.write("EPSA:" + str(epsa_renfort) + '\n')
    if vl_renfort != 0:
        fichier.write(whitespace)
        fichier.write("VL:" + str(vl_renfort) + '\n')
    if smur_renfort != 0:
        fichier.write(whitespace)
        fichier.write("SMUR:" + str(smur_renfort) + '\n')
    if helismur_renfort != 0:
        fichier.write(whitespace)
        fichier.write("HELI SMUR:" + str(helismur_renfort) + '\n')
    if ccf_renfort != 0:
        fichier.write(whitespace)
        fichier.write("CCF:" + str(ccf_renfort) + '\n')
    if vpb_renfort != 0:
        fichier.write(whitespace)
        fichier.write("VPB:" + str(vpb_renfort) + '\n')

    fichier.write("\n \n")

    if gendarmerie:
        fichier.write(whitespace)
        fichier.write("Gendarmerie SLL | ")
    if gdf:
        fichier.write(whitespace)
        fichier.write("GRDF SLL | ")
    if service_eaux:
        fichier.write(whitespace)
        fichier.write("Service des eaux SLL | ")
    if edf:
        fichier.write(whitespace)
        fichier.write("ERDF SLL | ")
    if brigade_verte:
        fichier.write(whitespace)
        fichier.write("Brigade Verte SLL | ")
    if maire:
        fichier.write(whitespace)
        fichier.write("Autorité municipale SLL")

    fichier.write('\n' + '\n')
    fichier.write(whitespace)
    fichier.write('>>Rapport de l\'intervention:' + '\n')
    fichier.write(whitespace)
    rapport = insert_newlines(rapport)
    fichier.write(rapport)
    fichier.close()  # The file isn't deleted
    print("Rapport rédigé avec succès !")

    if modeAccessRapport[0] == 1:
        name = inter.name
    else:
        name = "Rapport-" + str(datetime.datetime.now())
        inter.name = name

    inter.save()

    new_name = path_to_rinter + '/' + name
    check_and_create_folder(path_to_rinter)
    subprocess.call(["mv", fichier.name, new_name])

    try:
        os.chdir(path_to_rinter)
        subprocess.call(["xdg-open", name])
    except Exception as e:
        print("[WW] Cannot open redacted file :", e)
        raise e  # re raise error for GUI Handling


def export_inter_csv(file, inters):
    with open(file, 'w') as output_stream:
        output_stream.write("mois;interventions\n")
        for month, number in enumerate(inters):
            output_stream.write(str("{:02d}".format(month + 1)) + ";" + str(number) + "\n")


def check_and_create_report_dir(path, is_by_year):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except PermissionError as e:
            QMessageBox.critical(None, "Erreur", "Vous n'avez pas les permissions d'écriture sur le dossier " + str(
                path) + " .\nLe chemin reste inchangé.")
            pglx_qt.logger.critical("Chemin non accessible : " + str(e))

            # RETURN
            return
        except Exception as e:
            QMessageBox.critical(None, "Erreur", "Impossible de créer le chemin souhaité : " + str(e))
            pglx_qt.logger.critical("Failed to create path : " + str(e))

            # RETURN
            return

    if is_by_year:
        path_with_year = os.path.join(path, str(datetime.date.today().year))
        if not os.path.exists(path_with_year):
            try:
                os.makedirs(path_with_year)
            except PermissionError as e:
                QMessageBox.critical(None, "Erreur", "Vous n'avez pas les permissions d'écriture sur le dossier " + str(
                    path) + " .\nLe chemin reste inchangé.")
                pglx_qt.logger.critical("Chemin non accessible : " + str(e))
            except Exception as e:
                QMessageBox.critical(None, "Erreur", "Impossible de créer le chemin souhaité : " + str(e))
                pglx_qt.logger.critical("Failed to create path : " + str(e))


def open_file(path):
    """
    Open with system's editor, a file (in a new thread)
    :param self:
    :param path:
    :return:
    """

    thr = threading.Thread(target=open_file_(path))
    thr.start()


def open_file_(path):
    try:
        subprocess.call(["xdg-open", path])
    except Exception as e:
        pglx_qt.logger.exception(
            "[WW] Impossible d'ouvrir le fichier demandé, " + path + " : ")
        QMessageBox.warning(self, "Ouverture impossible", "Impossible d'ouvrir le fichier demandé:\n"
                            + path + " [E]:" + str(e))


"""
    Pompier-GLX 4.8
    Copyright (C) 2017 - 2018  Sydney Rodolphe Torcuato Gems

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
