#!/usr/bin/python3
# coding=utf-8

from PyQt5.uic.uiparser import QtWidgets
from PyQt5.QtCore import Qt

import operator  # allow to sort dic
import webbrowser
from functools import partial
import threading

from PyQt5.QtWidgets import QDialog
from PyQt5 import QtPrintSupport
from matplotlib.axis import *
from matplotlib.ticker import MaxNLocator

import pglx.saveInterToDB as saveInterToDB
from pglx.functionsFiles import *
import pglx.database as database

from pglx.models import *

from pglx.ui_py.pglx_managementVehicle import *
from pglx.ui_py.pglx_newUser import *
from pglx.ui_py.pglx import *
from pglx.ui_py.pglx_admin import *
from pglx.ui_py.pglx_fma import *
from pglx.ui_py.pglx_inter import *
from pglx.ui_py.pglx_inter_listFPT import *
from pglx.ui_py.pglx_inter_listVL import *
from pglx.ui_py.pglx_inter_listVTU import *
from pglx.ui_py.pglx_listFma import *
from pglx.ui_py.pglx_listInters import *
from pglx.ui_py.pglx_newCity import *
from pglx.ui_py.pglx_preferences import *
from pglx.ui_py.pglx_changePasswd import *
from pglx.ui_py.manageStaff import *
from pglx.ui_py.officier_profile import *
from pglx.ui_py.db_import_export import *

from pglx import LOGGING_LEVEL, DB_VERSION

from pglx.widgets.my_credits import Credits
from pglx.widgets.my_main_window import Ui_MyMainWindow
from pglx.widgets.my_licence import License
from pglx.widgets.my_statistics import Ui_MyStatistics

from pglx import *

import matplotlib

matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

#######################

create_tables()

db_is_up_to_date = database.check_versions(DB_VERSION)
loginUser = None
currentCaserne = None

if not os.path.exists(path_to_pglx):
    logger.warning(path_to_pglx + " doesn't exist. Attemp to create it.")
    subprocess.call(["mkdir", "-p", path_to_pglx])
    if not os.path.exists(path_to_pglx):
        logger.warning("Impossible de créer un chemin")
        logger.critical("Path to pglx : " + path_to_pglx + " is not valid. Using " + os.getcwd() + " instead")
    path_to_pglx = os.getcwd()

NCODISMAX = 5000000  # nombre maximum pour le numero CODIS
MAX_MATRICULE = 1000000

# Since Python 3.6 dict ordered is preserved
TYPE_INTER = ('Accident de la circulation', 'Destruction d\'hymenopteres', 'Divers', 'Incendie', 'Secours a personne',
              'Piquet',
              )
TYPE_FMA = ('Accident de la circulation', 'Destruction d\'hymenopteres', 'Divers', 'Incendie', 'Secours a personne',
            'Piquet', 'Autre'
            )

TYPE_CENTRE = ('CPI', 'CS', 'CSP')
VEHICULES = ('FPTL', 'VL', 'VTU')

GRADES = {'système': -1, 'JSP': 0, 'Apprenant': 1, 'Stagiaire': 2, 'Sapeur': 3, 'Caporal': 4, 'Caporal-chef': 5,
          'Sergent': 6, 'Sergent-Chef': 7, 'Adjudant': 8, 'Adjudant-chef': 9, 'Lieutenant': 10, 'Capitaine': 11,
          'Commandant': 12, 'Lieutenant-Colonnel': 13, 'Colonnel': 14}

TYPES = {'Non applicable': -1, 'Actif': 0, 'Vétérant': 1, 'Administratif': 2, 'Muté': 3, 'Démissionnaire': 4}

RANK = {'Sans droits': -1, 'Observateur': 0, 'Utilisateur': 1, 'Modérateur': 2, 'Administrateur': 3}
# rank == 4 : used with fonction.hasPerms() to only allow ChefDeCorps and/or Adjoint

# variales globales:
modeAccesRapport = [2, 0]  # défini [x,] le mode visionnage (0), modification (1) du rapport [,y]
modeAccesFma = [2, 0]


class Ui_MyDialog(QDialog):
    keyPressed = QtCore.pyqtSignal(QtCore.QEvent)  # cf connexion in __init__

    def __init__(self, parent):
        super(Ui_MyDialog, self).__init__(parent)
        out = subprocess.check_output(['xset q | grep "Caps"'], shell=True)
        out = out.decode("utf-8")  # because get binary
        if out[23] == "f":
            self.capsLock = False
        else:
            self.capsLock = True
        if out[47] == "f":
            self.NumLock = False
        else:
            self.NumLock = True

        self.keyPressed.connect(self.on_key)

    def keyPressEvent(self, event):
        logger.debug("[II] Event emit", event)
        super(Ui_MyDialog, self).keyPressEvent(event)
        self.keyPressed.emit(event)

    def on_key(self, event):
        if event.key() == QtCore.Qt.Key_CapsLock:
            self.capsLock = not self.capsLock
            self.checkLocks()
        elif event.key() == QtCore.Qt.Key_NumLock:
            self.NumLock = not self.NumLock
            self.checkLocks()


class Ui_MyPompierGLX(Ui_MyMainWindow, Ui_PompierGLX):
    def __init__(self, parent=None):
        super(Ui_MyPompierGLX, self).__init__(parent)
        global self_Ui_PompierGLX, db_is_up_to_date
        self_Ui_PompierGLX = self
        fonctions.update_vars()

        db_is_up_to_date = database.check_versions(DB_VERSION)

        self.win_list = []  # list of living windows (prevent they destruction by the garbage collector)
        self.setupUi(self)
        self.modifications()
        # Si l'utilisateur n'est pas connecté on demande à le connecter
        # permet surtout de demander à se connecter lors que le programme est lancé.
        if loginUser.id == 1:
            fonctions_self.login(self)

        self.center()

        # There isn't any window for now
        self.win_credits = None
        self.win_license = None
        self.win_fma = None
        self.win_list_inter = None
        self.win_list_inter_perso = None
        self.win_list_fma = None
        self.win_list_fma_perso = None
        self.win_vehicle = None
        self.win_manage_staff = None
        self.win_preferences = None
        self.win_administration = None
        self.win_new_rapport_inter = None
        self.win_stats = None

        # Slots
        self.slotReEnable = lambda: self.centralwidget.setEnabled(True)

    # CONNEXIONS

    def on_vehicleClicked(self):
        """
        Show (if allowed) the vehicle management window
        :return:
        """
        if loginUser.rank == 3 or loginUser == currentCaserne.chef or loginUser == currentCaserne.adjoint:
            self.setEnabled(False)
            win = Ui_MyVehicleManagement()
            win.show()
            self.win_list.append(win)
        else:
            QMessageBox.warning(self, "Accès interdit", "Vous n'avez pas la permission d'ouvrir ce menu")

    def on_pushButton_preferences_pressed(self):
        try:
            self.win_preferences = Ui_MyPreferences()
            # is delatedLater so no need to check is None
        except PermissionError:
            QMessageBox.warning(self, "Accès refusé", "Vous ne pouvez pas éditer le compte général.")
            # RETURN
            return
        except Exception as e:
            QMessageBox.critical(self, "Erreur", "Une erreur est survenue :\n" + str(e))
            logger.exception("Failed to open userPerf")
            # RETURN
            return
        self.win_preferences.show()
        self.win_preferences.closed.connect(self.slotReEnable)
        self.centralwidget.setEnabled(False)

    def on_pushButton_7_pressed(self):
        # Credits
        if self.win_credits is None:
            self.win_credits = Credits()
        self.win_credits.show()

    def on_pushButton_8_pressed(self):
        # License
        if self.win_license is None:
            self.win_license = License()
        self.win_license.show()

    def on_pushButton_listeInters_pressed(self):
        if self.win_list_inter is None:
            try:
                self.win_list_inter = Ui_MyListInters(self)
                self.win_list_inter.closed.connect(self.slotReEnable)
            except PermissionError:
                QMessageBox.warning(self, "Accès refusé", "Vous n'avez pas les droits requis.")
                # RETURN
                return
            except Exception as e:
                QMessageBox.critical(self, "Erreur", str(e))
                logger.exception("Failed to open inter list: ")
                # RETURN
                return

        else:
            self.win_list_inter.fillTable()  # If new inter in the meaning time
        self.win_list_inter.show()
        self.centralwidget.setEnabled(False)

    def on_pushButton_personnalInter_pressed(self):
        if self.win_list_inter_perso is None:
            try:
                self.win_list_inter_perso = Ui_MyListIntersPersonnal(self)
                self.win_list_inter_perso.closed.connect(self.slotReEnable)
            except PermissionError:
                QMessageBox.warning(self, "Accès refusé", "Vous n'avez pas les droits requis.")
                # RETURN
                return
            except Exception as e:
                QMessageBox.critical(self, "Erreur", str(e))
                logger.exception("Failed to open personnal inter list:")
                # RETURN
                return
        else:
            self.win_list_inter_perso.fillTable()  # if user changed, need to redraw for example
        self.win_list_inter_perso.show()
        self.centralwidget.setEnabled(False)

    def on_pushButton_listeFma_pressed(self):
        if self.win_list_fma is None:
            try:
                self.win_list_fma = Ui_MyListFma(self)
                self.win_list_fma.closed.connect(self.slotReEnable)  # closed is emit on closing
            except PermissionError:
                QMessageBox.warning(self, "Accès refusé", "Vous n'avez pas les droits requis.")
                # RETURN
                return
            except Exception as e:
                QMessageBox.critical(self, "Erreur", str(e))
                logger.exception("Failed to open FMA list:")
                # RETURN
                return

        else:
            self.win_list_fma.fillTableWidget()
        self.win_list_fma.show()

        self.centralwidget.setEnabled(False)

    def on_pushButton_listOwnFma_pressed(self):
        if self.win_list_fma_perso is None:
            try:
                self.win_list_fma_perso = Ui_MyListFmaPersonnal(self)
                self.win_list_fma_perso.closed.connect(self.slotReEnable)  # closed is emit on closing
            except PermissionError:
                QMessageBox.warning(self, "Accès refusé", "Vous n'avez pas les droits requis.")
                # RETURN
                return
            except Exception as e:
                QMessageBox.critical(self, "Erreur", str(e))
                logger.exception("Failed to open own FMA list:")
                # RETURN
                return

        else:
            self.win_list_fma_perso.fillTableWidget()
        self.win_list_fma_perso.show()

        self.centralwidget.setEnabled(False)

    def on_pushButton_vehicle_pressed(self):
        try:
            self.win_vehicle = Ui_MyVehicleManagement()
        except PermissionError:
            QMessageBox.warning(self, "Accès refusé", "Vous n'êtes pas administrateur.")
            return  # doesn't continue
        except Exception as e:
            QMessageBox.critical(self, "Errur", "Une erreur est survenue :\n" + str(e))
            logger.exception("Failed to open vehicle view")
        self.win_vehicle.closed.connect(self.slotReEnable)
        self.win_vehicle.show()
        self.centralwidget.setEnabled(False)

    def on_pushButton_10_pressed(self):
        fonctions_self.login(self)

    def on_pushButton_9_pressed(self):
        # No need of "if self.win_administration is None" 'cause admin delateLater() on closeEvent
        try:
            self.win_administration = Ui_MyAdministration(self)
        except PermissionError:
            QMessageBox.warning(self, "Accès refusé", "Vous n'êtes pas administrateur.")
            logger.exception("Failed to open Administration view")
            # RETURN
            return  # doesn't continue

        self.win_administration.closed.connect(self.slotReEnable)
        self.win_administration.show()
        self.centralwidget.setEnabled(False)

    def on_new_rapportInter_pressed(self):
        try:
            self.win_new_rapport_inter = Ui_MyReportInter(self)
        except PermissionError:
            QMessageBox.warning(self, "Accès interdit", "Vous n'avez pas les permissions suffisantes pour rédiger un "
                                                        "rapport.\nVeuillez vérifier que vous êtes connecté sur votre compte.")
            logger.warning("Permission Error: cannot create new inter report")
            # RETURN
            return
        except Exception as e:
            QMessageBox.critical(self, "Erreur", "Une erreur est survenue :\n" + str(e))
            logger.exception("Failed to create new inter report")
            # RETURN
            return
        except BaseException:
            # BaseException == user do not want to write report with sys account
            return

        self.win_new_rapport_inter.closed.connect(self.slotReEnable)
        self.win_new_rapport_inter.show()
        self.centralwidget.setEnabled(False)

    def on_pushButton_2_pressed(self):
        fonctions_self.fma(self)

    def on_pushButton_managment_pressed(self):
        try:
            self.win_manage_staff = Ui_MyManageStaff(self)
        except PermissionError:
            QMessageBox.warning(self, "Accès refusé", "Vous n'êtes pas administrateur.")
            logger.warning("Permission Error: manage staff")
            # RETURN
            return  # doesn't continue
        self.win_manage_staff.closed.connect(self.slotReEnable)
        self.win_manage_staff.show()
        self.centralwidget.setEnabled(False)

    def on_pushButton_editPasswd_pressed(self):
        fonctions_self.DialogChangeUserPasswd(self)

    def on_pushButton_stats_pressed(self):
        if self.win_stats is None:
            try:
                self.win_stats = Ui_MyStatistics(self, on_closed_=self.slotReEnable)
            except PermissionError:
                QMessageBox.warning(self, "Accès refusé",
                                    "Vous n'avez pas les permissions de visionner ou générer des statistiques.")
                # RETURN
                return
            except Exception as e:
                QMessageBox.critical(self, "Erreur", str(e))
                logger.exception("Failed to open stats: ")
                # RETURN
                return

        self.win_stats.show()
        self.centralwidget.setEnabled(False)

    def on_pushButton_edit_profile_pressed(self):
        self.centralwidget.setEnabled(False)
        Ui_MyProfileDialog(parent=self, user_id=loginUser.id).exec_()  # editon rights checked after
        self.centralwidget.setEnabled(True)

    def on_button_close_pressed(self):
        self.close()

    # MODIFICATIONS

    def modifications(self):
        cas = currentCaserne
        name = cas.name
        _translate = QtCore.QCoreApplication.translate
        self.user.setText(_translate("PompierGLX", loginUser.firstName + " " + loginUser.lastName))
        self.name.setText(_translate("PompierGLX", name))
        if loginUser.id == 1:
            self.pushButton_10.setText("Connexion")
        else:
            self.pushButton_10.setText("Déconnexion")


# noinspection PyPep8Naming
class Ui_MyReportInter(Ui_MyMainWindow, Ui_MainWindow_ReportInter):
    closed = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(Ui_MyReportInter, self).__init__(parent)
        global modeAccesRapport

        if (loginUser.grade < 0 and modeAccesRapport[0] != 0):
            # is the user sure to edit/create a report with a system account ?
            reply = QMessageBox.question(None, "Compte système", "Vous êtes connectés avec un compte système ;"
                                                                 " il est fortement déconseillé de créer un rapport"
                                                                 " depuis ce compte.\nUtilisez votre compte personnel.",
                                         QMessageBox.Ignore | QMessageBox.Cancel,QMessageBox.Cancel)
            if reply == QMessageBox.Cancel:
                # BaseException == user do not want to write report with sys account
                raise BaseException

        if (modeAccesRapport[0] == 0 and loginUser.rank >= 0) or (modeAccesRapport[
                                                                      0] == 2 and loginUser.rank >= 1) or loginUser == currentCaserne.chef or loginUser == currentCaserne.adjoint or (
                modeAccesFma[0] == 1 and loginUser == Interventions.get(id=modeAccesRapport[1]).redacteur):
            self.setupUi(self)

            self.loginUser = loginUser  # for intersaveToDB
            self.modeAccesRapport = modeAccesRapport  # for intersaveToDB
            self.windowsList = []
            self.FptIdList = []  # list of fpt in the intervention
            self.VlIdList = []
            self.VtuIdList = []

            self.modifications()
            self.modifications_retranslateUi(self, modeAccesRapport[1])

            if modeAccesRapport[0] == 0:  # si juste visionnage
                logger.debug('[II] mode visionnage')
                self.setValues()
                self.disableModifications()
            elif modeAccesRapport[0] == 1:  # si juste modification
                logger.debug('[II] mode modification')
                self.setValues()
            self.connexions()

            self.center()
        else:
            raise PermissionError

        if not Commune.select():
            raise Exception("Aucune commune n'a été configurée dans la base de données. Veuillez en ajouter une "
                            "avant de poursuivre.")

    def closeEvent(self, event):
        global modeAccesRapport
        modeAccesRapport = [2, 0]  # stop editing/viewing inter
        self.closed.emit()
        self.deleteLater()

    def modifications(self):
        self.spinBox.setMaximum(NCODISMAX)
        actual_date = datetime.datetime.now()
        self.dateTimeEdit_appel.setDateTime(actual_date)
        self.dateTimeEdit_depart.setDateTime(actual_date)
        self.dateTimeEdit_retour.setDateTime(actual_date)
        self.spinBox_nInter.setValue(
            Interventions.filter(Interventions.date_appel.year == datetime.datetime.now().year).count() + 1)

        self.comboBox_typeInter.addItems(TYPE_INTER)
        try:
            for commune in Commune.select():
                self.comboBox_commune.addItem(commune.name)
        except Exception as e:
            QMessageBox.warning(self, "Impossible de récupérer la/les commune(s)",
                                "Toutes les communes n'ont pas pu être récupérées: \n" + str(e))
            logger.exception("Failed to get Commune")

    def modifications_retranslateUi(self, MainWindow, id_inter):
        _translate = QtCore.QCoreApplication.translate
        if id_inter:
            n_inter = Interventions.get(id=id_inter).nInter
        else:
            n_inter = Interventions.filter(Interventions.date_appel.year
                                           == datetime.datetime.now().year).count() + 1
        MainWindow.setWindowTitle(_translate("MainWindow", "PGLX - Rapport d\'intervention N°" + str(n_inter)))

    def edit_vehicle(self):
        """
        Show/Edit the current vehicle (show dialog if hided)
        :return:
        """
        row = self.tableWidget_vehicleList.currentRow()
        if row >= 0:
            # Remove corresponding window
            self.windowsList[row].showIt()
        else:
            QMessageBox.information(self, "Aucun véhicle sélectionné",
                                    "Vous n'avez sélectionné aucun véhicle dans la liste.")

    def setValues(self):
        inter = Interventions.get(id=modeAccesRapport[1])
        self.spinBox.setValue(inter.ncodis)
        self.dateTimeEdit_appel.setDateTime(inter.date_appel)
        self.dateTimeEdit_depart.setDateTime(inter.date_depart)
        self.dateTimeEdit_retour.setDateTime(inter.date_fin)
        self.comboBox_typeInter.setCurrentIndex(inter.typeInter)
        self.spinBox_nInter.setValue(inter.nInter)
        #  self.comboBox_commune.setCurrentIndex(inter.commune)
        self.textEdit.setText(inter.rapport)
        self.lineEdit_natureInter.setText(inter.natureInter)
        self.lineEdit_lieu.setText(inter.localisation)
        self.lineEdit_demandeur.setText(inter.demandeur)
        #  checkbox
        self.checkBox_maire.setChecked(inter.maire)
        self.checkBox_servicesEaux.setChecked(inter.servicesEaux)
        self.checkBox_grdf.setChecked(inter.grdf)
        self.checkBox_erdf.setChecked(inter.erdf)
        self.checkBox_gendarmerie.setChecked(inter.gendarmerie)
        self.checkBox_brigadeVerte.setChecked(inter.brigadeVerte)

        self.spinBox_premDep_vl.setValue(inter.premierDepart.vl)
        self.spinBox_premDep_vsav.setValue(inter.premierDepart.vsav)
        self.spinBox_premDep_epsa.setValue(inter.premierDepart.epsa)
        self.spinBox_premDep_fpt.setValue(inter.premierDepart.fptsr)  # TODO: replace with fpt
        self.spinBox_premDep_smur.setValue(inter.premierDepart.smur)
        self.spinBox_premDep_heliSmur.setValue(inter.premierDepart.heliSmur)
        self.spinBox_premDep_ccf.setValue(inter.premierDepart.ccf)
        self.spinBox_premDep_vpb.setValue(inter.premierDepart.vpb)

        self.spinBox_renforts_vl.setValue(inter.renforts.vl)
        self.spinBox_renforts_vsav.setValue(inter.renforts.vsav)
        self.spinBox_renforts_epsa.setValue(inter.renforts.epsa)
        self.spinBox_renforts_fpt.setValue(inter.renforts.fptsr)  # TODO: replace with fpt
        self.spinBox_renforts_smur.setValue(inter.renforts.smur)
        self.spinBox_renforts_heliSmur.setValue(inter.renforts.heliSmur)
        self.spinBox_renforts_ccf.setValue(inter.renforts.ccf)
        self.spinBox_renforts_vpb.setValue(inter.renforts.vpb)

        # Set vehicles values
        # TMP for backward compatibility
        try:
            bd_v = BDMeta.get(id=1).version
        except Exception as e:
            QMessageBox.critical(self, "Impossible de continuer", "Cette intervention utilise un version antérieure"
                                                                  "de PGLX. Veuillez re-saisir à la main des véhicules utilisés et les rôles des SP." +
                                 str(e))
            logger.exception("Failed to retreive DB version")
            return

        if bd_v < DB_VERSION:
            QMessageBox.warning(self, "Version dépréciée", "La version " + str(bd_v) + " de la base de données n'est pas"
                                      " à jour. Veuillez appliquer les migrations requises.\nVersion actuelle: " + str(DB_VERSION))
            #migrateInter(inter)
        else:
            self.set_fpt_values(inter)
            self.set_vl_values(inter)
            self.set_vtu_values(inter)

    def set_fpt_values(self, inter):
        row = self.tableWidget_vehicleList.rowCount()  # get first free row
        list_ = FPTL.filter(FPTL.inter == inter)  # get all FPTL for current inter
        for f in list_:
            # Update fpt casID list
            self.FptIdList.append(f.vehicle.id)

            self.tableWidget_vehicleList.insertRow(row)  # add new row
            #  Set data values
            for i in range(0, 5):
                # Add items for tableWidget
                item = QtWidgets.QTableWidgetItem()
                self.tableWidget_vehicleList.setItem(row, i, item)
            self.tableWidget_vehicleList.item(row, 0).setText(str(f.vehicle.id))  # id of the vehicle
            self.tableWidget_vehicleList.item(row, 1).setText("FPT")  # type
            self.tableWidget_vehicleList.item(row, 2).setText("FPT-" + str(f.vehicle.casID))  # name (toString)
            self.tableWidget_vehicleList.item(row, 3).setText(str(f.ca.toString()))  # ca toString
            self.tableWidget_vehicleList.item(row, 4).setText(str(f.con.toString()))  # con toString

            # Create window
            w = Ui_MyInterListFPT(self, self.FptIdList, f.vehicle.id)

            w.comboBox_type.setCurrentText("FPT-" + str(f.vehicle.casID))
            # Find correct index (based on spv id) for each member
            index = w.comboBox_con.findData(f.con.id)
            w.comboBox_con.setCurrentIndex(index)
            index = w.comboBox_ca.findData(f.ca.id)
            w.comboBox_ca.setCurrentIndex(index)
            index = w.comboBox_ceBat.findData(f.ce.id)
            w.comboBox_ceBat.setCurrentIndex(index)
            index = w.comboBox_ceBal.findData(f.ce2.id)
            w.comboBox_ceBal.setCurrentIndex(index)
            index = w.comboBox_equBat.findData(f.equ.id)
            w.comboBox_equBat.setCurrentIndex(index)
            index = w.comboBox_equBal.findData(f.equ2.id)
            w.comboBox_equBal.setCurrentIndex(index)
            index = w.comboBox_stag.findData(f.stag.id)
            w.comboBox_stag.setCurrentIndex(index)

            # Add newly created window to win_list
            self.windowsList.append(w)

            row += 1

    def set_vl_values(self, inter):
        row = self.tableWidget_vehicleList.rowCount()
        list_ = VL.filter(VL.inter == inter)
        for v in list_:
            self.VlIdList.append(v.vehicle.id)

            self.tableWidget_vehicleList.insertRow(row)
            #  Set data values
            for i in range(0, 5):
                # Add items for tableWidget
                item = QtWidgets.QTableWidgetItem()
                self.tableWidget_vehicleList.setItem(row, i, item)
            self.tableWidget_vehicleList.item(row, 0).setText(str(v.vehicle.id))
            self.tableWidget_vehicleList.item(row, 1).setText("VL")
            self.tableWidget_vehicleList.item(row, 2).setText("VL-" + str(v.vehicle.casID))
            self.tableWidget_vehicleList.item(row, 3).setText(str(v.ca.toString()))
            self.tableWidget_vehicleList.item(row, 4).setText(str(v.con.toString()))

            # Create window
            w = Ui_MyInterListVL(self, self.VlIdList, vl_id=v.vehicle.id)
            # w.comboBox_type.setCurrentIndex(v.id-1)

            w.comboBox_type.setCurrentText("VL-" + str(v.vehicle.casID))
            # Find correct index (based on spv id) for each member
            index = w.comboBox_con.findData(v.con.id)
            w.comboBox_con.setCurrentIndex(index)
            index = w.comboBox_ca.findData(v.ca.id)
            w.comboBox_ca.setCurrentIndex(index)
            index = w.comboBox_equ.findData(v.equ.id)
            w.comboBox_equ.setCurrentIndex(index)
            index = w.comboBox_stag.findData(v.stag.id)
            w.comboBox_stag.setCurrentIndex(index)

            self.windowsList.append(w)

            row += 1

    def set_vtu_values(self, inter):
        row = self.tableWidget_vehicleList.rowCount()
        list_ = VTU.filter(VTU.inter == inter)
        for v in list_:
            self.VtuIdList.append(v.vehicle.id)

            self.tableWidget_vehicleList.insertRow(row)
            #  Set data values
            for i in range(0, 5):
                # Add items for tableWidget
                item = QtWidgets.QTableWidgetItem()
                self.tableWidget_vehicleList.setItem(row, i, item)
            self.tableWidget_vehicleList.item(row, 0).setText(str(v.vehicle.id))
            self.tableWidget_vehicleList.item(row, 1).setText("VTU")
            self.tableWidget_vehicleList.item(row, 2).setText("VTU-" + str(v.vehicle.casID))
            self.tableWidget_vehicleList.item(row, 3).setText(str(v.ca.toString()))
            self.tableWidget_vehicleList.item(row, 4).setText(str(v.con.toString()))

            # Create window
            w = Ui_MyInterListVTU(self, self.VtuIdList, v.vehicle.id)
            # w.comboBox_type.setCurrentIndex(v.id-1)

            w.comboBox_type.setCurrentText("VTU-" + str(v.vehicle.casID))
            # Find correct index (based on spv id) for each member
            index = w.comboBox_con.findData(v.con.id)
            w.comboBox_con.setCurrentIndex(index)
            index = w.comboBox_ca.findData(v.ca.id)
            w.comboBox_ca.setCurrentIndex(index)
            index = w.comboBox_equ.findData(v.equ.id)
            w.comboBox_equ.setCurrentIndex(index)
            index = w.comboBox_stag.findData(v.stag.id)
            w.comboBox_stag.setCurrentIndex(index)

            self.windowsList.append(w)

            row += 1

    def disableModifications(self):  # empêche la modification des données
        self.spinBox_nInter.setReadOnly(True)
        self.dateTimeEdit_appel.setReadOnly(True)
        self.dateTimeEdit_depart.setReadOnly(True)
        self.dateTimeEdit_retour.setReadOnly(True)
        self.spinBox.setReadOnly(True)
        self.spinBox_premDep_vsav.setReadOnly(True)
        self.spinBox_premDep_vl.setReadOnly(True)
        self.spinBox_premDep_fpt.setReadOnly(True)
        self.spinBox_premDep_epsa.setReadOnly(True)
        self.spinBox_premDep_smur.setReadOnly(True)
        self.spinBox_premDep_heliSmur.setReadOnly(True)
        self.spinBox_premDep_ccf.setReadOnly(True)
        self.spinBox_premDep_vpb.setReadOnly(True)
        self.spinBox_renforts_vsav.setReadOnly(True)
        self.spinBox_renforts_vl.setReadOnly(True)
        self.spinBox_renforts_fpt.setReadOnly(True)
        self.spinBox_renforts_epsa.setReadOnly(True)
        self.spinBox_renforts_smur.setReadOnly(True)
        self.spinBox_renforts_heliSmur.setReadOnly(True)
        self.spinBox_renforts_ccf.setReadOnly(True)
        self.spinBox_renforts_vpb.setReadOnly(True)
        self.comboBox_typeInter.setEnabled(False)
        self.comboBox_commune.setEnabled(False)

        self.lineEdit_demandeur.setReadOnly(True)
        self.lineEdit_lieu.setReadOnly(True)
        self.lineEdit_natureInter.setReadOnly(True)

        self.checkBox_brigadeVerte.setEnabled(False)
        self.checkBox_erdf.setEnabled(False)
        self.checkBox_gendarmerie.setEnabled(False)
        self.checkBox_grdf.setEnabled(False)
        self.checkBox_maire.setEnabled(False)
        self.checkBox_servicesEaux.setEnabled(False)
        self.textEdit.setReadOnly(True)

        # Vehicles
        self.pushButton_addVehicle.setEnabled(False)
        self.pushButton_removeVehicle.setEnabled(False)
        self.pushButton_editVehicle.setText("Voir")

        for w in self.windowsList:
            w.disableModifications()

    def updateTableWidgetItem(self, selfSubWin, id, name, ca, con):
        """
        Update the edited row
        :param selfSubWin: self of the vehicleType window
        :param id: id of the vehicle
        :param name: name of the vehicle
        :param ca: ca of the vehicle
        :param con con of the vehicle
        :return:
        """
        # windowsList has the same order as tableWidgets' rows
        row = self.windowsList.index(selfSubWin)

        # Update row
        self.tableWidget_vehicleList.item(row, 0).setText(str(id))
        self.tableWidget_vehicleList.item(row, 2).setText(name)
        self.tableWidget_vehicleList.item(row, 3).setText(ca)
        self.tableWidget_vehicleList.item(row, 4).setText(con)

    # EVENTS
    def on_spinBox_nInter_valueChanged(self, value):
        # If edit or visio, current n_inter is ok
        if self.check_ninter(value):
            self.set_nInter_warning(True)
        else:
            self.set_nInter_warning(False)

    def on_spinBox_valueChanged(self, value):
        if modeAccesRapport[0] in (0, 1):
            n_inter = Interventions.get(id=modeAccesRapport[1]).nInter
        else:
            n_inter = -1

        if Interventions.filter(Interventions.date_appel.year == datetime.datetime.now().year,
                                Interventions.nInter != n_inter, Interventions.ncodis == value):
            self.set_ncodis_warning(True)
        else:
            self.set_ncodis_warning(False)

    def on_addVehicle(self):
        """
        Add a new row in vehicle list and ask user for the current vehicle features
        :return:
        """
        logger.debug('PyQt5 button click')
        w = Ui_Dialog_VehicleTypeChoice()  # Get vehicle choice (or null il user cancel)
        if w.exec_():
            vehicle_type = w.choice
            logger.debug(vehicle_type)
            all_vehicles_used = True  # We assume that there is no vehicle left
            if vehicle_type == "FPT" or vehicle_type == "FPTL":
                # FPT*
                if len(self.FptIdList) < VehicleFPT.filter(VehicleFPT.status == 1).count():
                    # Check Whether there is a vehicle left
                    w = Ui_MyInterListFPT(self, self.FptIdList)
                    all_vehicles_used = False
            elif vehicle_type == "VL":
                # VL*
                if len(self.VlIdList) < VehicleVL.filter(VehicleVL.status == 1).count():
                    # Check Whether there is a vehicle left
                    w = Ui_MyInterListVL(self, self.VlIdList)
                    all_vehicles_used = False
            elif vehicle_type == "VTU":
                # VTU*
                if len(self.VtuIdList) < VehicleVTU.filter(VehicleVTU.status == 1).count():
                    # Check Whether there is a vehicle left
                    w = Ui_MyInterListVTU(self, self.VtuIdList)
                    all_vehicles_used = False

            # If there is a vehicle left
            if not all_vehicles_used:
                row = self.tableWidget_vehicleList.rowCount()  # get row
                self.tableWidget_vehicleList.insertRow(row)  # add row to tableWidget
                for i in range(0, 5):
                    # Add items for tableWidget
                    item = QtWidgets.QTableWidgetItem()
                    self.tableWidget_vehicleList.setItem(row, i, item)
                self.tableWidget_vehicleList.item(row, 1).setText(vehicle_type)
                w.show()
                self.windowsList.append(
                    w)  # prevent win from being garbage collected and keep in memomy win to unhide it
            else:
                QMessageBox.warning(self, "Véhicule indisponible",
                                    "Il n'y a plus de véhicules disponibles pour le type " + vehicle_type + " .")

    def on_editVehicle(self):
        self.edit_vehicle()

    def on_tableWidget_vehicleList_doubleClicked(self):
        self.edit_vehicle()

    def on_removeVehicle(self):
        """
        Remove vehicle from vehicle list and destroy (via garbage collector) the window.
        :return:
        """
        row = self.tableWidget_vehicleList.currentRow()
        if row >= 0:
            # Set vehicle available back
            """
            Since vehicle are created with addVehicle(), the windowsList has the same order as items in TableWidgets
            Thus we use it to map the row number with the corresponding window
            """
            type_ = self.windowsList[row].comboBox_type.currentText().split('-')[0]
            id_ = int(self.tableWidget_vehicleList.item(row, 0).text())
            if type_ == "FPT":
                self.FptIdList.remove(id_)
            elif type_ == "VL":
                self.VlCasId.remove(id_)
            elif type_ == "VTU":
                self.VtuIdList.remove(id_)

            # Remove selected row
            self.tableWidget_vehicleList.removeRow(row)
            # Remove corresponding window
            self.windowsList.pop(row)
        else:
            QMessageBox.warning(self, "Aucun véhicle sélectionné", "Vous n'avez sélectionné aucun véhicule.\nVeuillez"
                                                                   " d'abord sélectionner un véhicule à supprimer.")

    def validate(self):
        # On OK clicked
        # If edit or visio, current n_inter is ok
        if modeAccesRapport[0] in (0, 1):
            n_inter = Interventions.get(id=modeAccesRapport[1]).nInter
        else:
            n_inter = -1

        err = False
        err_msg = "Veuillez d'abord corriger les problèmes suivants avant de continuer :\n"
        if Interventions.filter(Interventions.date_appel.year == datetime.datetime.now().year,
                                Interventions.nInter != n_inter, Interventions.nInter == self.spinBox_nInter.value()):
            err_msg += "\tLe numéro d'intervention est déjà pris.\n"
            err = True
        if self.dateTimeEdit_appel.dateTime() > datetime.datetime.now():
            err_msg += "\tLa date d'appel est dans le futur.\n"
            err = True
        if self.dateTimeEdit_depart.dateTime() < self.dateTimeEdit_appel.dateTime():
            err_msg += "\tLa date de départ ne peut précéder la date d'appel.\n"
            err = True
        if self.dateTimeEdit_retour.dateTime() < self.dateTimeEdit_depart.dateTime():
            err_msg += "\tLa date de retour ne peut précéder la date de départ.\n"
            err = True
        if Interventions.filter(Interventions.date_appel.year == datetime.datetime.now().year,
                                Interventions.nInter != n_inter, Interventions.ncodis == self.spinBox.value()):
            err_msg += "\tLe numéro CODIS est déjà utilisé par une autre interventions.\n"
            err = True
            self.set_ncodis_warning(True)
        if datetime.datetime.now() < self.dateTimeEdit_retour.dateTime():
            err_msg += "\tLa date de retour ne peut être dans le futur.\n"
            err = True
        if self.checkLineEdit(self.lineEdit_lieu, 1, "orange", "white", "Veuillez remplir ce champ"):
            err_msg += "\tLe lieu d'intervention doit être renseigné.\n"
            err = True
        if self.checkLineEdit(self.lineEdit_natureInter, 1, "orange", "white", "Veuillez remplir ce champ"):
            err_msg += "\tLa nature détaillée de l'intervention doit être renseignée.\n"
            err = True
        if len(self.textEdit.toPlainText()) < 1:
            err_msg += "\tLe rapport d'intervention ne peut pas être vide.\n"
            err = True
            self.set_redaction_warning(True)
        if self.check_ninter(self.spinBox_nInter.value()):
            err_msg += "\tLe numéro d'intervention est déjà utilisé.\n"
            err = True

        if not err:
            inter = saveInterToDB.interSaveToDb(self, modeAccesRapport[0] == 1)
            if inter:
                try:
                    rediger(inter, path_to_rinter, self.modeAccesRapport)
                except Exception as e:
                    QMessageBox.warning(self, "Action impossible",
                                        "Impossible de rédiger le rapport.\nIl a néanmoins été enregistré avec succès.\n\nDétails: " + str(
                                            e))
                    logger.exception("Failed to write inter")
            else:
                QMessageBox.critical(self, "Erreur", "Une erreur s'est produite lors de l'enregistrement du rapport")
            self.close()
        else:
            QMessageBox.warning(self, "Sauvegarde impossible", err_msg)

    def cancelEdit(self):
        global modeAccesRapport
        modeAccesRapport = [2, -1]  # réinitialise pour éviter les boucles
        self.close()

    # CONNEXIONS
    def connexions(self):
        global modeAccesRapport
        if modeAccesRapport[0] == 0:  # si mode visionnage
            self.pushButton_ok.clicked.connect(self.close)
        else:
            self.pushButton_ok.clicked.connect(self.validate)  # lambda: fonctions_self.interSaveToDb(self))
        if modeAccesRapport[0] == 1:  # si mode modification
            self.pushButton_cancel.clicked.connect(self.cancelEdit)
        else:
            self.pushButton_cancel.clicked.connect(self.close)

    def on_pushButton_addVehicle_pressed(self):
        self.on_addVehicle()

    def on_pushButton_editVehicle_pressed(self):
        self.on_editVehicle()

    def on_pushButton_removeVehicle_pressed(self):
        self.on_removeVehicle()

    def on_dateTimeEdit_appel_dateTimeChanged(self):
        # in the future ?
        if self.dateTimeEdit_appel.dateTime() > datetime.datetime.now():
            self.set_date_appel_warning(True)
        else:
            self.set_date_appel_warning(False)

        if not self.my_valide_DateTimeEdit_depart():
            self.set_date_depart_warning(True)
        else:
            self.set_date_depart_warning(False)

        if not self.my_valide_DateTimeEdit_retour():
            self.set_date_retour_warning(True)
        else:
            self.set_date_retour_warning(False)

    def on_dateTimeEdit_depart_dateTimeChanged(self):
        if not self.my_valide_DateTimeEdit_depart():
            self.dateTimeEdit_depart.setStyleSheet("QDateTimeEdit {background-color: orange;}")
        else:
            self.dateTimeEdit_depart.setStyleSheet("QDateTimeEdit {background-color: none;}")

        if not self.my_valide_DateTimeEdit_retour():
            self.dateTimeEdit_retour.setStyleSheet("QDateTimeEdit {background-color: orange;}")
        else:
            self.dateTimeEdit_retour.setStyleSheet("QDateTimeEdit {background-color: none;}")

    def on_dateTimeEdit_retour_dateTimeChanged(self):
        if not self.my_valide_DateTimeEdit_retour():
            self.dateTimeEdit_retour.setStyleSheet("QDateTimeEdit {background-color: orange;}")
        else:
            self.dateTimeEdit_retour.setStyleSheet("QDateTimeEdit {background-color: none;}")

    def my_valide_DateTimeEdit_depart(self):
        return datetime.datetime.now() >= self.dateTimeEdit_depart.dateTime() >= self.dateTimeEdit_appel.dateTime()

    def my_valide_DateTimeEdit_retour(self):
        return datetime.datetime.now() >= self.dateTimeEdit_retour.dateTime() >= self.dateTimeEdit_depart.dateTime() and self.dateTimeEdit_retour.dateTime() >= self.dateTimeEdit_appel.dateTime()

    @staticmethod
    def check_ninter(new_n_inter):
        # If edit or visio, current n_inter is ok
        if modeAccesRapport[0] in (0, 1):
            n_inter = Interventions.get(id=modeAccesRapport[1]).nInter
        else:
            n_inter = -1

        return Interventions.filter(Interventions.date_appel.year == datetime.datetime.now().year,
                                    Interventions.nInter != n_inter, Interventions.nInter == new_n_inter)

    @staticmethod
    def checkLineEdit(QLineEdit, strictMinNbOfCar, backgroundColorFailed, backgroundColorOk, toolTip):
        if len(QLineEdit.text()) <= strictMinNbOfCar:
            QLineEdit.setStyleSheet("QLineEdit {background-color: " + backgroundColorFailed + "};")
            QLineEdit.setToolTip(toolTip)
            code = 1
        else:
            QLineEdit.setStyleSheet("QLineEdit {background-color: " + backgroundColorOk + "};")
            QLineEdit.setToolTip("")
            code = 0
        return code

    def on_lineEdit_lieu_textChanged(self):
        self.checkLineEdit(self.lineEdit_lieu, 1, "orange", "white", "Veuillez remplir ce champ")

    def on_lineEdit_natureInter_textChanged(self):
        self.checkLineEdit(self.lineEdit_natureInter, 1, "orange", "white", "Veuillez remplir ce champ")

    def on_textEdit_textChanged(self):
        self.set_redaction_warning((len(self.textEdit.toPlainText()) < 1))

    @staticmethod
    def set_spinBox_warning(QSpinBox, tool_tip="", warning=True):
        if warning:
            QSpinBox.setStyleSheet("QSpinBox {background-color: orange;}")
        else:
            QSpinBox.setStyleSheet("QSpinBox {background-color: white;}")
        QSpinBox.setToolTip(tool_tip)

    def set_ncodis_warning(self, warning=True):
        self.set_spinBox_warning(self.spinBox, "Déjà utilisé par une autre intervention" if warning else "", warning)

    def set_nInter_warning(self, warning=True):
        self.set_spinBox_warning(self.spinBox_nInter, "Déjà utilisé par une autre intervention" if warning else "",
                                 warning)

    @staticmethod
    def set_dateTime_warning(QDateTime, tool_tip="", warning=True):
        if warning:
            QDateTime.setStyleSheet("QDateTimeEdit {background-color: orange;}")
        else:
            QDateTime.setStyleSheet("QDateTimeEdit {background-color: none;}")
        QDateTime.setToolTip(tool_tip)

    def set_date_appel_warning(self, warning=True):
        self.set_dateTime_warning(self.dateTimeEdit_appel, "", warning)

    def set_date_depart_warning(self, warning=True):
        self.set_dateTime_warning(self.dateTimeEdit_depart, "", warning)

    def set_date_retour_warning(self, warning=True):
        self.set_dateTime_warning(self.dateTimeEdit_retour, "", warning)

    def set_redaction_warning(self, warning=True):
        if warning:
            self.textEdit.setStyleSheet("QTextEdit {background-color: orange;}")
            self.textEdit.setToolTip("Le rapport d'invention ne peut être vide")
        else:
            self.textEdit.setStyleSheet("QTextEdit {background-color: none;}")
            self.textEdit.setToolTip("")


class Ui_MyAdministration(Ui_MyMainWindow, Ui_Administration):
    closed = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(Ui_MyAdministration, self).__init__(parent)
        if fonctions.userHasPerm(3, user=loginUser):
            self.setupUi(self)
            cas = currentCaserne
            self.modifications()
            self.connexions(cas)

            self.center()

            self.list_cities()

        else:
            raise PermissionError

    def closeEvent(self, event):
        self.closed.emit()
        self.deleteLater()

    def list_cities(self):
        """
        Add cities to the listWidget
        :return:
        """

        for c in Commune.select():
            item = QtWidgets.QListWidgetItem()
            item.setText(c.commonName())
            item.setData(32, c.id)  # 32 is the first role to app ownn puprose (see doc)
            self.listWidget_cities.addItem(item)

    def modifications(self):
        global db_is_up_to_date
        cas = currentCaserne
        self.checkBox_inter.setChecked(cas.pathInterByYear)
        self.checkBox_fma.setChecked(cas.pathFmaByYear)
        self.checkBox_casernement.setChecked(cas.pathCasernementByYear)

        try:
            self.lineEdit_name.setText(cas.name)
        except Exception as e:
            logger.exception('[WW] pas d\'entré pour la table Caserne :')

        self.comboBox_type.addItems(TYPE_CENTRE)
        self.comboBox_type.setCurrentIndex(cas.type)
        for spv in Personnel.select().where(Personnel.id > 1).order_by(Personnel.id):
            # Prevent default user to be chef ou adjoint
            self.comboBox_chef.addItem((spv.lastName + ' ' + spv.firstName))
            self.comboBox_adjoint.addItem((spv.lastName + ' ' + spv.firstName))
        # TODO: ATTENTION pour ces deux fonctions, si un utilisateur est supprimé, mauvais personnel: (1,2,4) << à corriger
        try:
            logger.debug('[II] chef:', cas.chef.lastName)
            self.comboBox_chef.setCurrentIndex(cas.chef.id - 2)
        except Exception as e:
            logger.exception("[WW] Impossible de définir le chef de centre :")
        try:
            logger.debug('[II] Adjoint:', cas.adjoint.lastName)
            self.comboBox_adjoint.setCurrentIndex(cas.adjoint.id - 2)
        except Exception as e:
            logger.exception("[WW] Impossible de définir l'adjoint au chef de centre")

        # DataBase
        self.label_current_db_version_value.setText(str(DB_VERSION))

        if db_is_up_to_date:
            self.pushButton_migrate_database.setEnabled(False)
            self.pushButton_migrate_database.setToolTip("Aucune mise à jour n'est nécessaire.")

    def connexions(self, cas):
        self.pushButton_passwd.clicked.connect(lambda a='': fonctions_self.DialogChangeUserPasswd(self))
        self.pushButton_pathInter.clicked.connect(lambda a='': fonctions_self.newInterPath(self, cas))
        self.pushButton_pathFma.clicked.connect(lambda a='': fonctions_self.newFmaPath(self, cas))
        self.pushButton_pathCasernement.clicked.connect(lambda a='': fonctions_self.newCasernementPath(self, cas))

    def on_pushButton_ok_pressed(self):
        cas = currentCaserne
        cas.name = self.lineEdit_name.text()
        cas.type = self.comboBox_type.currentIndex()
        cas.chef = self.comboBox_chef.currentIndex() + 2
        cas.adjoint = self.comboBox_adjoint.currentIndex() + 2
        cas.pathInterByYear = bool(self.checkBox_inter.isChecked())
        cas.pathFmaByYear = bool(self.checkBox_fma.isChecked())
        cas.pathCasernementByYear = bool(self.checkBox_casernement.isChecked())

        cas.save()
        self_Ui_PompierGLX.modifications()  # permet d'actualiser le nom de la caserne
        fonctions.update_vars()

        self.close()

    def addCity(self, city):
        item = QtWidgets.QListWidgetItem(city.commonName())
        item.setData(32, city.id)
        self.listWidget_cities.addItem(item)

    def on_pushButton_addCity_pressed(self):
        dialog = Ui_MyDialogNewCity()
        if dialog.exec_():
            name = dialog.getName()
            code = dialog.getCode()
            try:
                c = Commune.create(name=name, codeCommune=code)
                logger.info("[II] Commune créée")
                self.addCity(c)
            except Exception as e:
                QMessageBox.critical(self, "Action impossible", "Impossible de créer une nouvelle commune :\n" + str(e))
                logger.exception("Impossible de créer une commune : ")

    def on_pushButton_deleteCity_pressed(self):
        cities = self.listWidget_cities.selectedItems()
        if cities:
            for c in cities:
                try:
                    Commune.get(id=c.data(32)).delete_instance()
                    # Remove item from the widget
                    self.listWidget_cities.takeItem(self.listWidget_cities.row(c))
                except Exception as e:
                    QMessageBox.critical(self, "Opération impossible",
                                         "Impossible de supprimer la commune sélectionnée.\n" + str(e))
                    logger.exception("Impossible de supprimer une commune : ")
        else:
            QMessageBox.warning(self, "Sélection incomplète", "Veuillez d'abord sélectionner une commune à supprimer.")

    def on_pushButton_editCity_pressed(self):
        selected_city = self.listWidget_cities.selectedItems()
        if len(selected_city):
            city_id = selected_city[0].data(32)
            city = Commune.get(id=city_id)
            dialog = Ui_MyDialogNewCity(name=city.name, code=city.codeCommune, title="Édition de la commune")
            if dialog.exec_():
                new_name = dialog.getName()
                try:
                    Commune.update(name=new_name, codeCommune=dialog.getCode()).where(Commune.id == city_id).execute()
                    new_name = Commune.get(id=city_id).commonName()
                except Exception as e:
                    QMessageBox.critical(self, "Opération impossible",
                                         "La commune n'a pas pu être mise à jour.\Détails : " + str(e))
                    logger.exception("Failed to update Commune: ")
                selected_city[0].setText(new_name)

        else:
            QMessageBox.warning(self, "Aucune commune sélectionnée", "Veuillez sélectionner une commune à éditer")

    def on_pushButton_export_pressed(self):
        self.centralwidget.setEnabled(False)
        Ui_MyDialogDB(self, mode=1).exec_()
        self.centralwidget.setEnabled(True)

    def on_pushButton_import_pressed(self):
        self.centralwidget.setEnabled(False)
        Ui_MyDialogDB(self, mode=0).exec_()
        self.centralwidget.setEnabled(True)

    def on_pushButton_migrate_database_pressed(self):
        fonctions_self.migrate_databases(self)
        self.modifications()


class Ui_MyPreferences(Ui_MyMainWindow, Ui_MainWindowPreferences):
    closed = QtCore.pyqtSignal()

    def __init__(self):
        if loginUser.id != 1:
            super(Ui_MyPreferences, self).__init__()
            # If not generic user
            self.setupUi(self)

            self.userPref = loginUser.UserPreferences_User
            if self.userPref:
                self.userPref = self.userPref[0]
            else:
                QMessageBox.information(None, "Initialisation", "Vos préférences ont été initialisées.")
                UserPreferences.create(user=loginUser)
                self.userPref = loginUser.UserPreferences_User

            self.setValues()
            self.set_perms()
            self.center()
        else:
            raise PermissionError

    def set_perms(self):
        # Disable objects lambda users aren't allowed to edit
        if not fonctions.userHasPerm(3, allow_adjoint=True, allow_chef=True):
            self.tab_admin.setDisabled(True)

    def setValues(self):
        if fonctions.userHasPerm(3, allow_adjoint=True, allow_chef=True):
            show_all = self.userPref.vehicleManagementShowAll
            self.checkBox_vehicleManagementShowAll.setChecked(show_all)

    def closeEvent(self, event):
        self.closed.emit()
        self.deleteLater()

    def on_checkBox_vehicleManagementShowAll_stateChanged(self, state):
        self.userPref.vehicleManagementShowAll = state

    def on_buttonBox_accepted(self):
        self.userPref.save()
        self.close()

    def on_buttonBox_rejected(self):
        self.close()


class Ui_MyManageStaff(Ui_MyMainWindow, Ui_ManageStaff):
    closed = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(Ui_MyManageStaff, self).__init__(parent)
        if fonctions.userHasPerm(3, True, True, user=loginUser):
            self.setupUi(self)  # partie générée par pyuic
            self.adjust_layout()
            self.addModifications()  # ajoute les modifs
            self.addConnexions()  # ajoute les connexions des signaux

            self.center()
        else:
            raise PermissionError

    def closeEvent(self, event):
        self.closed.emit()
        event.accept()

    def on_buttonBox_accepted(self):
        self.close()

    def on_buttonBox_rejected(self):
        self.close()

    def on_pushButton_addStaff_pressed(self):
        fonctions_self.addStaffMember(self)

    def on_tableWidget_staff_cellChanged(self):
        fonctions_self.staffMemberEdited(self)

    def addConnexions(self):
        # Connexion pour le combobox des grades
        nb = self.tableWidget_staff.rowCount()

        for i in range(0, nb):  # up to 'nb', but not including 'nb'
            # for GRADE
            self.tableWidget_staff.cellWidget(i, 4).currentIndexChanged.connect(
                lambda: fonctions_self.staffMemberRankEdited(self, 4))
            # for TYPE
            self.tableWidget_staff.cellWidget(i, 11).currentIndexChanged.connect(
                lambda: fonctions_self.staffMemberTypeEdited(self, 11))

            self.tableWidget_staff.cellWidget(i, 5).dateChanged.connect(
                lambda: fonctions_self.staffMemberInvolvmentDateEdited(self, 5))
            self.tableWidget_staff.cellWidget(i, 10).dateChanged.connect(
                lambda: fonctions_self.staffMemberPLDateEdited(self, 10))
            # for CHECKBOX
            self.tableWidget_staff.cellWidget(i, 7).stateChanged.connect(self.on_checkBox_COD1_StateChanged)
            self.tableWidget_staff.cellWidget(i, 8).stateChanged.connect(self.on_checkBox_COD2_StateChanged)
            self.tableWidget_staff.cellWidget(i, 9).stateChanged.connect(self.on_checkBox_VL_StateChanged)

    def get_user_id(self, row):
        id_ = None
        if row > -1:
            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
        return id_

    def get_clicked_item(self):
        # On récupère le widget surlequel on a cliqué (et non l'item car on peut pas)
        clickme = QtWidgets.QApplication.focusWidget()
        return self.tableWidget_staff.itemAt(
            clickme.pos()) if clickme else None  # On récupère l'item ayant la même position

    def get_selected_user_id(self):
        row = self.tableWidget_staff.currentRow()
        return self.get_user_id(row)

    def add_spv(self, fireman):
        """
        add a new row and fill it with fireman data
        :param fireman:
        :return:
        """
        row = self.tableWidget_staff.rowCount()
        self.tableWidget_staff.setRowCount(row + 1)

        self.fill_spv_row(row, fireman)

    def update_row(self, row):
        user_id = self.get_user_id(row)
        spv = Personnel.get(id=user_id)
        self.tableWidget_staff.blockSignals(True)  # prevent reemitting change
        # todo : change, do nothing
        self.set_row_values(row, spv)
        self.tableWidget_staff.blockSignals(False)

    def fill_spv_row(self, row, fireman):
        """
        fill row with fireman data
        :param row:
        :param fireman:
        :return:
        """
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 0, item)
        self.tableWidget_staff.item(row, 0).setData(2, fireman.matricule)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 1, item)
        self.tableWidget_staff.item(row, 1).setData(2, fireman.matriculeCS)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 2, item)
        self.tableWidget_staff.item(row, 2).setText(str(fireman.lastName))
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 3, item)
        self.tableWidget_staff.item(row, 3).setText(str(fireman.firstName))

        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 4, item)
        rank_combo = QtWidgets.QComboBox()
        grades = sorted(GRADES.items(), key=operator.itemgetter(1))  # on trie par rapport à la valeur des elmts
        # ATTENTION GRADES commence à -1
        for name, i in grades:
            rank_combo.insertItem(i + 1, name)
        rank_combo.setCurrentIndex(int(fireman.grade) + 1)
        self.tableWidget_staff.setCellWidget(row, 4, rank_combo)  # ComboBox pour le choix du grade

        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 5, item)
        engagement_date = QtWidgets.QDateEdit()
        engagement_date.setMinimumDate(QtCore.QDate(1950, 1, 1))
        involvement_date = fireman.involvementDate
        if involvement_date is not None:
            engagement_date.setDate(QtCore.QDate(involvement_date.year, involvement_date.month, involvement_date.day))

        self.tableWidget_staff.setCellWidget(row, 5, engagement_date)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)

        self.tableWidget_staff.setItem(row, 7, item)
        cellWidget = QtWidgets.QCheckBox()
        cellWidget.setChecked(bool(fireman.COD1))
        self.tableWidget_staff.setCellWidget(row, 7, cellWidget)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 8, item)
        cellWidget = QtWidgets.QCheckBox()
        cellWidget.setChecked(bool(fireman.COD2))
        self.tableWidget_staff.setCellWidget(row, 8, cellWidget)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 9, item)
        cellWidget = QtWidgets.QCheckBox()
        cellWidget.setChecked(bool(fireman.VL))
        self.tableWidget_staff.setCellWidget(row, 9, cellWidget)

        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 10, item)
        PLDate = QtWidgets.QDateEdit()
        PLDate.setMinimumDate(QtCore.QDate(1950, 1, 1))
        heavyVehicleLicenceDate = fireman.heavyVehicleLicenceDate
        if heavyVehicleLicenceDate is not None:
            PLDate.setDate(
                QtCore.QDate(heavyVehicleLicenceDate.year, heavyVehicleLicenceDate.month, heavyVehicleLicenceDate.day))

        self.tableWidget_staff.setCellWidget(row, 10, PLDate)
        # TYPE
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 11, item)
        typeCombo = QtWidgets.QComboBox()
        types = sorted(TYPES.items(), key=operator.itemgetter(1))
        # ATTENTION TYPES commence à -1
        for name, i in types:
            typeCombo.insertItem(i + 1, name)
        typeCombo.setCurrentIndex(int(fireman.type) + 1)
        self.tableWidget_staff.setCellWidget(row, 11, typeCombo)

        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 12, item)
        self.tableWidget_staff.item(row, 12).setData(2, int(fireman.rank))
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable)
        self.tableWidget_staff.setItem(row, 13, item)
        self.tableWidget_staff.item(row, 13).setText(str(fireman.id))
        # ATTENTION utilisé dans fonctions_self.staffMemberEdited > changer column dans id

    def set_row_values(self, row, fireman):

        self.tableWidget_staff.item(row, 0).setData(2, fireman.matricule)
        self.tableWidget_staff.item(row, 1).setData(2, fireman.matriculeCS)
        self.tableWidget_staff.item(row, 2).setText(str(fireman.lastName))
        self.tableWidget_staff.item(row, 3).setText(str(fireman.firstName))

        gradeCombo = self.tableWidget_staff.cellWidget(row, 4)
        gradeCombo.setCurrentIndex(fireman.grade + 1)
        typeCombo = self.tableWidget_staff.cellWidget(row, 11)
        typeCombo.setCurrentIndex(fireman.type + 1)
        self.tableWidget_staff.item(row, 12).setData(2, fireman.rank)

        cod1 = self.tableWidget_staff.cellWidget(row, 7)
        cod1.setChecked(fireman.COD1)
        cod2 = self.tableWidget_staff.cellWidget(row, 8)
        cod2.setChecked(fireman.COD2)
        vl = self.tableWidget_staff.cellWidget(row, 9)
        vl.setChecked(fireman.VL)

        date_invol = self.tableWidget_staff.cellWidget(row, 5)
        date = fireman.get_involvement_date()
        if date:
            date_invol.setDate(date)
        date_heavy = self.tableWidget_staff.cellWidget(row, 10)
        date = fireman.get_heavy_vehicle_licence_date()
        if date:
            date_heavy.setDate(date)

    def on_checkBox_COD1_StateChanged(self):
        # todo user get_selected_user_id
        item = self.get_clicked_item()
        if item:
            row = item.row()
            column = item.column()
            COD1 = self.tableWidget_staff.cellWidget(row, column).isChecked()
            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
            if id_ != 1 and id_ != 2:  # empêche la modification du spv nobody et administrateur
                spv = Personnel.get(Personnel.id == id_)
                spv.COD1 = COD1
                spv.save()
            else:
                QMessageBox.warning(self, "Modification impossible", 'Vous ne pouvez pas modifier cet utilisateur.')

    def on_checkBox_COD2_StateChanged(self):
        # todo user get_selected_user_id
        item = self.get_clicked_item()
        if item:
            row = item.row()
            column = item.column()
            COD2 = self.tableWidget_staff.cellWidget(row, column).isChecked()
            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
            if id_ != 1 and id_ != 2:  # empêche la modification du spv nobody et administrateur
                spv = Personnel.get(Personnel.id == id_)
                spv.COD2 = COD2
                spv.save()
            else:
                QMessageBox.warning(self, "Modification impossible", 'Vous ne pouvez pas modifier cet utilisateur.')

    def on_checkBox_VL_StateChanged(self):
        # todo user get_selected_user_id
        item = self.get_clicked_item()
        if item:
            row = item.row()
            column = item.column()
            VL = self.tableWidget_staff.cellWidget(row, column).isChecked()
            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
            if id_ != 1 and id_ != 2:  # empêche la modification du spv nobody et administrateur
                spv = Personnel.get(Personnel.id == id_)
                spv.VL = VL
                spv.save()
            else:
                QMessageBox.warning(self, "Modification impossible", 'Vous ne pouvez pas modifier cet utilisateur.')

    def on_pushButton_edit_profile_pressed(self):
        user_id = self.get_selected_user_id()
        if user_id:
            self.centralwidget.setEnabled(False)
            profile_window = Ui_MyProfileDialog(editable=True, user_id=user_id)
            profile_window.exec_()
            self.centralwidget.setEnabled(True)
            self.update_row(self.tableWidget_staff.currentRow())
        else:
            QMessageBox.warning(self, "Veuillez sélectionner un personnel", "Veuillez sélectionner un personnel"
                                                                            " pour lequel vous souhaiter voir"
                                                                            " ou éditer sa fiche.")

    def on_pushButton_editPassword_pressed(self):
        pass_dialog = Ui_MyDialogChangeUserPasswd(user_to_change=Personnel.get(id=self.get_selected_user_id()))
        pass_dialog.exec_()

    def on_tableWidget_staff_doubleClicked(self):
        self.on_pushButton_edit_profile_pressed()

    def adjust_layout(self):
        self.tableWidget_staff.setColumnWidth(4, 160)
        self.tableWidget_staff.setColumnWidth(5, 110)
        self.tableWidget_staff.setColumnWidth(10, 110)
        self.tableWidget_staff.setColumnWidth(11, 130)
        self.tableWidget_staff.setColumnWidth(13, 50)

    # partie autogénérée:
    def addModifications(self):
        n_staff = Personnel.select().count()
        self.tableWidget_staff.setRowCount(n_staff)
        self.tableWidget_staff.setSortingEnabled(
            False)  # will allow you to use the same row argument for all items in the same row (i.e. setItem() will not move the row).

        USED_COLUMNS = (0, 1, 2, 3, 4, 7, 8, 9, 11, 12, 13)  # Because some columns aren't used
        COMBOBOX_COLUMNS = (4, 11)  # list of comboBox widgets
        DATE_COLUMNS = (5, 10)
        CHECKBOX_COLUMNS = (7, 8, 9)
        row = 0
        for fireman in Personnel.select():
            self.fill_spv_row(row, fireman)
            row += 1

        self.tableWidget_staff.setSortingEnabled(True)

        # Disable admin and caserne users
        for r in 0, 1:
            for c in USED_COLUMNS:  # for all columns
                self.tableWidget_staff.item(r, c).setFlags(QtCore.Qt.ItemIsSelectable)
            for i in COMBOBOX_COLUMNS:
                self.tableWidget_staff.cellWidget(r, i).setEnabled(0)
            for i in DATE_COLUMNS:
                self.tableWidget_staff.cellWidget(r, i).setEnabled(0)
            for i in CHECKBOX_COLUMNS:
                self.tableWidget_staff.cellWidget(r, i).setEnabled(0)


class Ui_MyFMA(Ui_MyMainWindow, Ui_FMA):
    def __init__(self, parent):
        super(Ui_MyFMA, self).__init__(parent)
        self.setupUi(self)
        self.modifications()
        global selfi, FMA
        selfi = self
        FMA = self

        if (loginUser.type < 0 and modeAccesFma[0] != 0):
            # is the user sure to edit/create a report with a system account ?
            reply = QMessageBox.question(None, "Compte système", "Vous êtes connectés avec un compte système ;"
                                                                 " il est fortement déconseillé de créer un rapport"
                                                                 " depuis ce compte.\nUtilisez votre compte personnel.",
                                         QMessageBox.Ignore | QMessageBox.Cancel,QMessageBox.Cancel)
            if reply == QMessageBox.Cancel:
                # BaseException == user do not want to write report with sys account
                raise BaseException

        if loginUser.rank > 0 or (modeAccesFma[
                                      0] == 0 and loginUser.rank >= 0) or loginUser == currentCaserne.chef or loginUser == currentCaserne.adjoint or (
                modeAccesFma[0] == 1 and loginUser == Interventions.get(id=modeAccesRapport[1]).redacteur):
            if modeAccesFma[0] == 0:  # mode visionnage
                logger.debug("[II] FMA en mode visionnage")
                self.setValues()
                self.disableModifications()
            elif modeAccesFma[0] == 1:  # mode édition
                logger.debug("[II] FMA en mode édition")
                self.setValues()

            self.center()
        else:
            raise PermissionError

    def closeEvent(self, event):
        global self_ui_inter
        self_ui_inter.centralwidget.setEnabled(True)
        event.accept()

    def on_buttonBox_accepted(self):
        if modeAccesFma[0] == 0:  # si mode visionnage
            fonctions_self.close_window(self, self_ui_inter)
        else:
            fonctions_self.fmaSaveToDb(self)

    def on_buttonBox_rejected(self):
        if modeAccesFma[0] == 1:  # si mode édition
            fonctions_self.cancel_RapportFmaEdit(self, self_ui_inter)
        else:
            fonctions_self.close_window(selfi, self_ui_inter)

    def modifications(self):
        self.dateTimeEdit_start.setDateTime(datetime.datetime.now())
        self.dateTimeEdit_end.setDateTime(datetime.datetime.now())

        self.comboBox_type.addItems(TYPE_FMA)

        # column width for names entry : +50%
        self.tableWidget_trainer.setColumnWidth(0, 200)
        self.tableWidget_trainee.setColumnWidth(0, 200)

    def disableModifications(self):  # empêche la modification des données
        self.dateTimeEdit_start.setReadOnly(True)
        self.dateTimeEdit_end.setReadOnly(True)
        self.comboBox_type.setEnabled(False)
        self.lineEdit_formation.setReadOnly(True)
        self.lineEdit_lieu.setReadOnly(True)
        self.lineEdit_name.setReadOnly(True)
        self.textEdit.setReadOnly(True)

        self.pushButton_addTrainer.setEnabled(False)
        self.pushButton_removeTrainer.setEnabled(False)
        self.pushButton_addTrainee.setEnabled(False)
        self.pushButton_removeTrainee.setEnabled(False)
        self.pushButton_addVehicle.setEnabled(False)
        self.pushButton_removeVehicle.setEnabled(False)

        self.tableWidget_trainer.setEnabled(False)
        self.tableWidget_trainee.setEnabled(False)
        self.tableWidget_vehicle.setEnabled(False)

    def setValues(self):
        fma = Fma.get(id=modeAccesFma[1])
        self.lineEdit_formation.setText(fma.theme)
        self.lineEdit_lieu.setText(fma.lieu)
        self.lineEdit_name.setText(fma.name)

        self.comboBox_type.setCurrentIndex(fma.type)

        self.dateTimeEdit_start.setDateTime(fma.dateDebut)
        self.dateTimeEdit_end.setDateTime(fma.dateFin)
        self.lineEdit_name.setText(fma.name)
        self.textEdit.setText(fma.rapport)

        try:  # Complétion de la liste des formateurs
            autresFormateurs = fma.autresFormateurs.replace('[', '').replace(']', '').split(',')  # en fait une liste
            i = 0
            for formateur in autresFormateurs:
                formateur = formateur.replace("'", '')
                self.addItem_tableWidget_trainer(formateur.split(';')[0], formateur.split(';')[1])
                i += 1
        except IndexError:
            logger.warning("IndexError in fma.autresFormateurs.replace(...) (expected)")
        except Exception as e:
            logger.exception('Une erreur est survenue dans l\'affectation des formateurs ')

        try:
            traineeList = fma.autresSpvFormes.replace('[', '').replace(']', '').split(',')  # en fait une liste
            i = 0
            for trainee in traineeList:
                trainee = trainee.replace("'", '')
                self.addItem_tableWidget_trainee(trainee.replace("'", '').replace('"', '').split(';')[0],
                                                 trainee.split(';')[1])
                i += 1
        except IndexError:
            logger.warning("IndexError in fma.autresSpvFormes(...) (expected)")
        except Exception as e:
            logger.exception('Une erreur est survenue dans l\'affectation des spv formés ')

        try:
            vehicleList = fma.vehicules.replace('[', '').replace(']', '').split(',')  # en fait une liste
            i = 0
            for vehicle in vehicleList:
                vehicle = vehicle.replace("'", '')
                self.addItem_tableWidget_vehicle(vehicle.replace("'", '').replace('"', '').split(';')[0],
                                                 vehicle.split(';')[1])
                i += 1
        except IndexError:
            logger.warning("IndexError in fma.vehicle(...) (expected)")
        except Exception as e:
            logger.exception('Une erreur est survenue dans l\'affectation des véhicules ')

    def on_dateTimeEdit_generic_dateTimeChanged(self):
        if self.dateTimeEdit_start.dateTime() > self.dateTimeEdit_end.dateTime():
            self.dateTimeEdit_end.setStyleSheet("QDateTimeEdit {background-color: orange}")
        else:
            self.dateTimeEdit_end.setStyleSheet("QDateTimeEdit {background-color: white}")

    def on_dateTimeEdit_start_dateTimeChanged(self):
        self.on_dateTimeEdit_generic_dateTimeChanged()

    def on_dateTimeEdit_end_dateTimeChanged(self):
        self.on_dateTimeEdit_generic_dateTimeChanged()

    def on_pushButton_removeTrainer_pressed(self):
        # Supprime la ligne sélectionnée dans RapportFMA

        currentRow = self.tableWidget_trainer.currentRow()
        self.tableWidget_trainer.removeRow(currentRow)

    def on_pushButton_removeTrainee_pressed(self):
        # Supprime la ligne sélectionnée dans RapportFMA

        currentRow = self.tableWidget_trainee.currentRow()
        self.tableWidget_trainee.removeRow(currentRow)

    def on_pushButton_removeVehicle_pressed(self):
        # Supprime la ligne sélectionnée dans RapportFMA

        currentRow = self.tableWidget_vehicle.currentRow()
        self.tableWidget_vehicle.removeRow(currentRow)

    def on_pushButton_addTrainer_pressed(self, first_field="Prénom Nom", second_field="Formation dispensée"):
        # Ajoute une ligne type pour un nouveau formateur dans RapportFMA
        self.addItem_tableWidget_trainer(first_field, second_field)

    def on_pushButton_addTrainee_pressed(self, first_field="Prénom Nom", second_field="Formation Suivie"):
        self.addItem_tableWidget_trainee(first_field, second_field)

    def on_pushButton_addVehicle_pressed(self, vehicle="Vehicule", role="Rôle"):
        self.addItem_tableWidget_vehicle(vehicle, role)

    def addItem_tableWidget_trainer(self, first_field, second_field):
        row = self.tableWidget_trainer.rowCount()  # row commence à 0

        # On insère une nouvelle ligne
        self.tableWidget_trainer.insertRow(row)

        # On ajoute à CHAQUE colonne un item
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_trainer.setItem(row, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_trainer.setItem(row, 1, item)

        # On définit la valeur par défaut de chaque item
        self.tableWidget_trainer.item(row, 0).setData(0, first_field)
        self.tableWidget_trainer.item(row, 1).setData(0, second_field)

    def addItem_tableWidget_trainee(self, first_field, second_field):
        # Ajoute une ligne type pour un nouveau sp dans RapportFMA
        row = self.tableWidget_trainee.rowCount()  # row commence à 0

        # On insère une nouvelle ligne
        self.tableWidget_trainee.insertRow(row)

        # On ajoute à CHAQUE colonne un item
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_trainee.setItem(row, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_trainee.setItem(row, 1, item)

        # On définit la valeur par défaut de l'item standard
        self.tableWidget_trainee.item(row, 1).setData(0, second_field)

        # AUTO COMPLETION
        perso = Personnel.select().where(Personnel.type == 0 or Personnel.type == 2)
        names = []
        for spv in perso:
            names.append(spv.firstName + ' ' + spv.lastName)

        completer = QtWidgets.QCompleter(names)
        self.lineEdit = QtWidgets.QLineEdit()
        self.lineEdit.setCompleter(completer)
        self.lineEdit.setPlaceholderText(first_field)
        self.tableWidget_trainee.setCellWidget(row, 0, self.lineEdit)

    def addItem_tableWidget_vehicle(self, vehicle, role):
        # Ajoute une ligne type pour un nouveau formateur dans RapportFMA
        row = self.tableWidget_vehicle.rowCount()  # row commence à 0

        # On insère une nouvelle ligne
        self.tableWidget_vehicle.insertRow(row)

        # On ajoute à CHAQUE colonne un item
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setItem(row, 0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_vehicle.setItem(row, 1, item)

        # On définit la valeur par défaut de chaque item
        self.tableWidget_vehicle.item(row, 0).setData(0, vehicle)
        self.tableWidget_vehicle.item(row, 1).setData(0, role)


class Ui_MyListInters(Ui_MyMainWindow, Ui_ListInters):
    closed = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(Ui_MyListInters, self).__init__(parent)
        if fonctions.userHasPerm(0, allow_chef=True, allow_adjoint=True, user=loginUser):
            self.setupUi(self)
            self.modifications()
            global selfi
            selfi = self

            self.center()
        else:
            raise PermissionError

        # self.slotReEnable = lambda: self.centralwidget.setEnabled(True)

    # Events

    def closeEvent(self, event):
        # Send closed signal to parent to re-enable centralWidget
        self.closed.emit()
        event.accept()

    def changeSelectedYear(self):
        # display inter of the selected year
        self.fillTable()

    def inter_edited(self):
        self.centralwidget.setEnabled(True)
        # TODO : check efficiency
        self.tableWidget_inters.clearContents()
        self.fillTable()

    # Methods DATA
    def getListInter(self):
        """

        :return: list of inters of the selected year
        """
        year = self.spinBox_year.value()
        return Interventions.filter(Interventions.date_appel.year == year)

    def get_selected_inter_id(self):
        id_ = None
        row = self.tableWidget_inters.currentRow()
        item = self.tableWidget_inters.item(row, 9)
        if item:
            id_ = int(item.text())
        else:
            QMessageBox.warning(self, "Aucune sélection", "Veuillez sélectionner une intervention à ouvrir.")

        return id_

    def show_edit_inter(self, edit=False):
        global modeAccesRapport

        interId = self.get_selected_inter_id()
        if interId:
            try:
                modeAccesRapport = [edit, interId]
                try:
                    self.winRapportInter = Ui_MyReportInter(self)
                except PermissionError:
                    QMessageBox.warning(self, "Accès interdit",
                                        "Vous n'avez pas les permissions suffisantes pour visionner et ou éditer un "
                                        "rapport.\nVeuillez vérifier que vous êtes connecté sur votre compte.")
                    logger.warning("Permission Error: show edit inter")
                    # RETURN
                    return
                except Exception as e:
                    QMessageBox.critical(self, "Erreur", "Une erreur est survenue :\n" + str(e))
                    logger.exception("Failed to show/edit inter")
                    # RETURN
                    return
                except BaseException:
                    # BaseException == user do not want to write report with sys account
                    return

                self.winRapportInter.show()
                self.winRapportInter.closed.connect(self.inter_edited)  # self.slotReEnable)
                self.centralwidget.setEnabled(False)
            except Exception as e:
                logger.exception("Show edit Inter")
                pass

    def on_pushButton_show_pressed(self):
        self.show_edit_inter(False)

    def on_tableWidget_inters_doubleClicked(self):
        self.show_edit_inter(False)

    def on_pushButton_edit_pressed(self):
        self.show_edit_inter(edit=True)
        """
        ATTENTION
        ici modeAccesRapport est réinitialisé dans interSaveToDb et la connexion.
        - modeAccessRapport est nécessaire dans Intersavetodb pour savoir si c'est une modif dans ce cas quelle est l'inter
        - si l'utilisateur décide de ne pas modifier, la valeur est réinitialisé par la connexion on_cancelClicked
        """

    def on_spinBox_year_valueChanged(self):
        # change selected year
        self.changeSelectedYear()

    def on_pushButton_pressed(self):
        interId = self.get_selected_inter_id()
        if interId:
            QMessageBox.information(self, "Avertissement", "Attention : si vous effectuez des modifications dans la"
                                                           " fenêtre qui va s'ouvrir, celles-ci ne seront pas"
                                                           " enregistrées dans la base de données.\nIl est préférable"
                                                           " d'utiliser les boutons visionner et éditer de PGLX.")
            inter = Interventions.get(id=interId)
            logger.debug("visionnage de l'intervention N° " + str(inter.nInter))
            path = path_to_rinter + "/" + inter.name
            open_file(path)

    def on_pushButton_print_pressed(self):
        id_ = self.get_selected_inter_id()
        if id_:
            lines = ""
            try:
                with open(os.path.join(path_to_rinter, Interventions.get(id=id_).name), 'r') as file:
                    for line in file:
                        lines += line
            except FileNotFoundError as e:
                logger.warning("Print" + str(e))
                QMessageBox.warning(self, "Rapport inexistant",
                                    "Veuillez d'abord enregistrer le rapport au format texte avant"
                                    " de l'imprimer.\n" + str(e) + "\n\nPour ce faire vous"
                                                                   " pouvez  \"éditer\" puis \"valider\" l'intervention.")
            except Exception as e:
                logger.exception("Failed to print")
                QMessageBox.warning(self, "Erreur", "L'erreur suivante est survenue :\n" + str(e))

            if lines:
                printer = QtPrintSupport.QPrinter()
                painter = QtGui.QPainter()
                dialog = QtPrintSupport.QPrintDialog(printer, self)
                if dialog.exec_():
                    painter.begin(printer)
                    painter.drawText(100, 100, 500, 500, Qt.AlignLeft | Qt.AlignTop, lines)
                    painter.end()
                    QMessageBox.information(self, "Impression réussie", "Le rapport a été imprimé avec succès.")

    def on_pushButton_delete_pressed(self):
        # héhé plus rien ne m'arrête pour cette version 4.2 !!!! :D C|

        inter_id = self.get_selected_inter_id()
        if inter_id:
            try:
                inter = Interventions.get(id=inter_id)
                cas = currentCaserne

                '''
                L'intervention ne peut être supprimée que par le chef de corps et son adjoint
                '''
                if loginUser == cas.chef or loginUser == cas.adjoint:
                    fonctions.deleteInter(inter)
                    self.tableWidget_inters.removeRow(self.tableWidget_inters.currentRow())
                else:
                    QMessageBox.warning(self, 'Suppression impossible',
                                        "Vous n'êtes pas habilité à supprimer un rapport.")
            except Exception as e:
                QMessageBox.critical(self, 'Suppression impossible',
                                     "Une erreur est survenue, merci de contacter l'administrateur." + str(e))
                logger.exception('[EE] impossible de supprimer l\'intervention ')

    def getDataInter(self):
        # TODO replace and implement in
        # TODO étant donné qu'on a "self" on peut récupérer minYear et maxYear sans les passer en argument !!!
        # enregistre dans mois le nombre d'interventions
        mois = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        inter_selected = Interventions.filter(Interventions.date_appel.year >= self.spinBox_minYear.value(),
                                              Interventions.date_appel.year <= self.spinBox_maxYear.value())
        # Si tri selon rédacteur
        # if redacteur:
        #    inter_selected = inter_selected.filter(Interventions.redacteur == redacteur)
        # les interventions sont cummulées sur toutes les années
        for inter in inter_selected:
            # Si sélection par véhicule, on regarde si l'intervention a ce véhicule.
            # Les if sont dans la boucle car il serait intéressant de pouvoir sélectioner plusieurs véhicules, dans ce
            #  cas les if sont bien placés.

            fptl_checked = self.radioButton_fptl.isChecked()
            vl_checked = self.radioButton_vl.isChecked()
            vtu_checked = self.radioButton_vtu.isChecked(),

            # ATTENTION: m = mois -1 car mois commence à 1 mais mois[m] < commence à 0
            if self.checkBox_sortByVehicle.isChecked():
                if fptl_checked and inter.FPTL_inter.exists():
                    m = inter.date_appel.month - 1
                    mois[m] += 1
                elif vtu_checked and inter.VTU_inter.exists():
                    m = inter.date_appel.month - 1
                    mois[m] += 1
                elif vl_checked and inter.VL_inter.exists():
                    m = inter.date_appel.month - 1
                    mois[m] += 1
            else:
                m = inter.date_appel.month - 1
                mois[m] += 1

        return mois

    def choose_file_name(self):
        file = None
        # TODO duplicate with export/import
        path = QtWidgets.QFileDialog(self)
        path.setNameFilter("*.csv")
        path.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        path.setDefaultSuffix("csv")

        if path.exec_():
            file = path.selectedFiles()[0]

        return file

    def on_buttonBox_accepted(self):
        self.close()

    def on_buttonBox_rejected(self):
        self.close()

    def on_pushButton_showGraphics_pressed(self):
        fonctions_self.showInterGraphics(self, self.getDataInter())

    def on_pushButton_export_pressed(self):
        file = self.choose_file_name()
        if file:
            export_inter_csv(file, self.getDataInter())

    def modifications(self):
        self.spinBox_minYear.setMaximum(datetime.datetime.today().year)
        self.spinBox_maxYear.setMaximum(datetime.datetime.today().year)
        self.spinBox_minYear.setValue(datetime.datetime.today().year)
        self.spinBox_maxYear.setValue(datetime.datetime.today().year)

        # Show inter of the selected year
        self.spinBox_year.setMaximum(datetime.datetime.today().year)
        self.spinBox_year.setValue(datetime.datetime.today().year)

        self.fillTable()

    def add_row(self, row, inter):
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.tableWidget_inters.setItem(row, 0, item)
        self.tableWidget_inters.item(row, 0).setData(Qt.DisplayRole, inter.nInter)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.tableWidget_inters.setItem(row, 1, item)
        self.tableWidget_inters.item(row, 1).setText(str(inter.date_appel))
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.tableWidget_inters.setItem(row, 2, item)
        self.tableWidget_inters.item(row, 2).setData(2, inter.ncodis)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.tableWidget_inters.setItem(row, 3, item)
        self.tableWidget_inters.item(row, 3).setText(str(inter.natureInter))
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.tableWidget_inters.setItem(row, 4, item)
        self.tableWidget_inters.item(row, 4).setText(str(inter.date_depart))
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.tableWidget_inters.setItem(row, 5, item)
        self.tableWidget_inters.item(row, 5).setText(str(inter.date_fin))
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        # Nom de la commune (utilise try si une commune n'est pas entrée ne bloque pas le programme
        self.tableWidget_inters.setItem(row, 6, item)
        try:
            commune = inter.commune.name
        except Exception as e:
            logger.critical("Ajout de la commune ; " + str(e))
            commune = "Non renseignée"
        self.tableWidget_inters.item(row, 6).setText(commune)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        # Nom du rédacteur (utilise try si le rédacteur n'est pas entrée ou n'existe plus ne bloque pas le programme
        self.tableWidget_inters.setItem(row, 7, item)
        try:
            if inter.redacteur.id == 1:
                redacteur = "Compte Caserne"
            else:
                redacteur = inter.redacteur.firstName + " " + inter.redacteur.lastName
        except Exception as e:
            logger.critical("Ajout du rédacteur " + str(e))
            redacteur = "Non renseigné"
        self.tableWidget_inters.item(row, 7).setText(redacteur)
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.tableWidget_inters.setItem(row, 8, item)
        if inter.premierDepart.alone() and inter.renforts.alone():
            self.tableWidget_inters.item(row, 8).setText('Oui')
        else:
            self.tableWidget_inters.item(row, 8).setText('Non')
        item = QtWidgets.QTableWidgetItem()
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        self.tableWidget_inters.setItem(row, 9, item)
        self.tableWidget_inters.item(row, 9).setData(2, int(inter.id))

    def fillTable(self):
        """
        Draw the interventions list
        """

        listInter = self.getListInter()
        self.tableWidget_inters.setRowCount(listInter.count())
        self.tableWidget_inters.setSortingEnabled(
            False)  # will allow you to use the same row argument for all items in the same row (i.e. setItem() will not move the row).

        for row, inter in enumerate(listInter):
            self.add_row(row, inter)
        self.tableWidget_inters.setSortingEnabled(True)


class Ui_MyListIntersPersonnal(Ui_MyListInters):
    """
    Cette classe affiche les rapports d'interventions rédigés par l'utilisateur
    Hérite de listInter
    """

    def __init__(self, parent):
        super(Ui_MyListIntersPersonnal, self).__init__(parent)

    # Methods DATA
    def getListInter(self):
        """
        @override
        :return: list of inters of the selected year
        """
        year = self.spinBox_year.value()
        return Interventions.filter(Interventions.date_appel.year == year, Interventions.redacteur == loginUser)


class Ui_MyListFma(Ui_MyMainWindow, Ui_ListFma):
    # Event for parent window : re-enable centralWidget on closed
    closed = QtCore.pyqtSignal()

    def __init__(self, parent):
        super(Ui_MyListFma, self).__init__(parent)

        if fonctions.userHasPerm(0, allow_chef=True, allow_adjoint=True, user=loginUser):
            self.setupUi(self)
            self.modifications()
            global selfi
            selfi = self

            self.center()

        else:
            raise PermissionError

    def getListFma(self):
        return Fma.filter(Fma.dateDebut.year == self.spinBoxYear.value())

    def closeEvent(self, event):
        # Send closed signal to parent to re-enable centralWidget
        self.closed.emit()
        event.accept()

    def on_pushButton_close_pressed(self):
        self.close()

    def modifications(self):
        self.spinBoxYear.setMaximum(datetime.datetime.now().year)
        self.spinBoxYear.setValue(datetime.datetime.now().year)

        self.fillTableWidget()

    def get_selected_fma_id(self):
        row = self.tableWidget_fma.currentRow()
        return int(self.tableWidget_fma.item(row, 6).text())

    def fillTableWidget(self):
        list_fma = self.getListFma()
        fma = list_fma.count()
        self.tableWidget_fma.setRowCount(fma)
        self.tableWidget_fma.setSortingEnabled(
            False)  # will allow you to use the same row argument for all items in the same row (i.e. setItem() will not move the row).

        row = 0
        for fma in list_fma:
            duree = fma.dateFin - fma.dateDebut
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget_fma.setItem(row, 0, item)
            self.tableWidget_fma.item(row, 0).setData(2, fma.nFma)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget_fma.setItem(row, 1, item)
            self.tableWidget_fma.item(row, 1).setText(str(fma.dateDebut))
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget_fma.setItem(row, 2, item)
            self.tableWidget_fma.item(row, 2).setData(2, fma.theme)
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget_fma.setItem(row, 3, item)
            self.tableWidget_fma.item(row, 3).setText(str(fma.name))
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget_fma.setItem(row, 4, item)
            self.tableWidget_fma.item(row, 4).setText(str(duree))
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget_fma.setItem(row, 5, item)
            try:  # TODO: remove : la base de donnée de test avait pas de rédacteurs pour les 1er fma >> redacteur.name impossible
                if fma.redacteur.id == 1:  # si l'utilisateur qui a rédigé la fma n'était pas connecté
                    self.tableWidget_fma.item(row, 5).setText('Compte générique')
                else:
                    self.tableWidget_fma.item(row, 5).setText(
                        fma.redacteur.firstName + ' ' + str(fma.redacteur.lastName))
            except Exception as e:
                logger.critical("FMA : échec ajout du rédacteur " + str(e))
                self.tableWidget_fma.item(row, 5).setText(str(fma.redacteur))
            item = QtWidgets.QTableWidgetItem()
            self.tableWidget_fma.setItem(row, 6, item)
            self.tableWidget_fma.item(row, 6).setData(2, int(fma.id))
            for i in range(0, 6):
                self.tableWidget_fma.item(row, i).setFlags(
                    QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)  # empêche la modification dans la clé primaire dans la base de donnée
            row += 1

        self.tableWidget_fma.setSortingEnabled(True)

    def on_pushButton_edit_pressed(self):
        self.show_fma(edit=True)

    def on_pushButton_show_pressed(self):
        self.show_fma()

    def on_spinBoxYear_valueChanged(self):
        self.fillTableWidget()

    def on_tableWidget_fma_doubleClicked(self):
        self.show_fma()

    def on_pushButton_graphics_pressed(self):
        fonctions_self.graphicDataFma(self)

    def on_pushButton_open_pressed(self):
        fmaId = self.get_selected_fma_id()

        if fmaId:
            QMessageBox.information(self, "Avertissement", "Attention : si vous effectuez des modifications dans la"
                                                           " fenêtre qui va s'ouvrir, celles-ci ne seront pas"
                                                           " enregistrées dans la base de données.\nIl est préférable"
                                                           " d'utiliser les boutons visionner et éditer de PGLX.")
            fma = Fma.get(id=fmaId)
            logger.debug("visionnage de la fma N°" + str(fma.nFma))
            path = path_to_rfma + "/" + fma.name
            open_file(path)

    def on_pushButton_delete_pressed(self):
        global listInterNum

        try:
            row = self.tableWidget_fma.currentRow()
            fma_id = int(self.tableWidget_fma.item(row, 6).text())
            fma = Fma.get(id=fma_id)
            cas = currentCaserne

            '''
            La Fma ne peut être supprimée que par le chef de corps et son adjoint
            '''
            if loginUser == cas.chef or loginUser == cas.adjoint:
                fonctions.deleteFma(fma)
                self.tableWidget_fma.removeRow(row)

            else:
                QMessageBox.warning(self, 'Suppression impossible', "Vous n'êtes pas habilité à supprimer un rapport.")

        except Exception as e:
            QMessageBox.error(self, 'Suppression impossible',
                              "Une erreur est survenue, merci de contacter l'administrateur.\n" + str(e))
            logger.exception('Impossible de supprimer la fma :')

    def show_fma(self, edit=False):
        global modeAccesFma
        """ met dans modeAccesRapport l'id de la fma souhaitée"""
        try:
            fma_id = self.get_selected_fma_id()
            modeAccesFma = [edit, fma_id]
            fonctions_self.fma(self)

        except PermissionError:
            QMessageBox.warning(self, "Accès refusé",
                                "Vous n'avez pas les permissions de visionner ou éditer un rapport FMA.")

            self.centralwidget.setEnabled(True)  # If there is a problem fma() cannot reenable it, so we do it here
            modeAccesFma = [2, -1]  # if failed: réinitialise pour éviter les boucles infinies

        except Exception as e:
            QMessageBox.warning(self, 'Visionnage FMA impossible',
                                "Une erreur est survenue lors du visionnage.\n Veuillez contacter l'administrateur.\n" + str(
                                    e))
            logger.exception('[EE] Impossible de visionner le rapport de FMA :')

            self.centralwidget.setEnabled(True)  # If there is a problem fma() cannot reenable it, so we do it here
            modeAccesFma = [2, -1]  # if failed: réinitialise pour éviter les boucles infinies

        except BaseException:
            # BaseException == user do not want to write report with sys account
            return

        """
        ATTENTION
        ici modeAccesFma est réinitialisé dans fmaSaveToDb et la connexion.
        - modeAccessRapport est nécessaire dans Intersavetodb pour savoir si c'est une modif dans ce cas quelle est l'inter
        - si l'utilisateur décide de ne pas modifier, la valeur est réinitialisé par la connexion on_cancelClicked
        
        - s'il devait y avoir une exception on ne sait pas si modeAccessRapport a pu être réinitialisé dans Intersavetodb : le fait donc dans le except.
        """


class Ui_MyListFmaPersonnal(Ui_MyListFma):
    """
    Cette fonction affiche les rapports de fma rédigés par l'utilisateur
    """

    def __init__(self, parent):
        super(Ui_MyListFmaPersonnal, self).__init__(parent)

    def getListFma(self):
        return Fma.filter(Fma.redacteur == loginUser, Fma.dateDebut.year == self.spinBoxYear.value())


class Ui_MyDialogDB(QDialog, Ui_DialogDB):
    def __init__(self, parent=None, mode=0):
        """

        :param parent:
        :param mode: 0 > import ; 1 > export
        """
        QDialog.__init__(self, parent=parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.mode = mode

        self.setupUi(self)
        if mode:
            self.set_export()
        else:
            self.set_import()

    def set_import(self):
        self.setWindowTitle("Importer une base de données")
        self.label_select_db.setText("Base de données à importer")
        self.label_file_path_value.setText("")
        self.label_version_value.setText("")
        self.checkBox_db.setChecked(False)
        self.checkBox_user_db.setChecked(False)
        self.checkBox_db.setEnabled(False)
        self.checkBox_user_db.setEnabled(False)

    def set_export(self):
        self.setWindowTitle("Exporter une base de données")
        self.label_file_path_value.setText("")
        self.label_version_value.setText(str(DB_VERSION))

    def on_buttonBox_accepted(self):
        if self.validate():
            if self.mode:
                err = database.export_database(self.label_file_path_value.text(), DB_VERSION,
                                     export_user_db=self.checkBox_user_db.isChecked(),
                                     export_db=self.checkBox_db.isChecked())
                if not err:
                    QMessageBox.information(self, "Export réussit !", "L'export de la base de données s'est déroulé"
                                                                      " avec succès.")

            elif not self.mode:
                err = database.import_database(self.label_file_path_value.text(), DB_VERSION,
                                        self.checkBox_db.isChecked(), self.checkBox_user_db.isChecked(),
                                        path_to_pglx=path_to_pglx)
                if not err:
                    QMessageBox.information(self, "Import réussit !", "L'importation de la base de données s'est "
                                                                      "correctement effectuée.\n"
                                                                      "PGLX va s'éteindre. Vous pouvez relancer le "
                                                                      "programme pour avoir accès à la nouvelle base "
                                                                      "de données.")
                    exit(0)

            self.accept()

    def on_toolButton_file_path_pressed(self):
        path = QtWidgets.QFileDialog(self)
        path.setNameFilter("*.pglxdb")
        if self.mode:
            # export
            path.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
            path.setDefaultSuffix("pglxdb")

        if path.exec_():
            self.label_file_path_value.setText(path.selectedFiles()[0])
            if self.validate_file():
                if not self.mode:
                    self.read_db()

    def read_db(self):
        try:
            json_ = database.read_desc_file(self.label_file_path_value.text())
            db_exported = json_["exported_db"]["db"]
            user_db_exported = json_["exported_db"]["user_db"]

            import_db_version = json_['DB_VERSION']
            if import_db_version > DB_VERSION:
                QMessageBox.critical(self, "Import impossible", "La version de la base de données (" + str(
                    import_db_version) + ") est supérieure à celle de PGLX (" + str(DB_VERSION)
                    + "). Veuillez d'abord mettre à jour votre version de PGLX.")
                self.reject()

            self.label_version_value.setText(str(import_db_version))
            self.checkBox_db.setChecked(db_exported)
            self.checkBox_user_db.setChecked(user_db_exported)

            if DB_VERSION != import_db_version:
                self.set_warning(self.label_version_value, toolTip="La version actuelle du programme et la version du" \
                    " programme utilisé lors de la création du fichier de sauvegarde sont différentes.\n" \
                    "Une mise à jour sera nécessaire après l'import. (Version actuelle : " + str(DB_VERSION))
            if user_db_exported:
                self.checkBox_user_db.setEnabled(True)
            if db_exported:
                self.checkBox_db.setEnabled(True)

        except Exception as e:
            QMessageBox.critical(self, "Lecture impossible", "La lecture de l'archive de sauvegarde"
                                                             " a échoué.\nCeci signifie que le fichier"
                                                             " est corrompu ou n'est pas un fichier"
                                                             " de sauvegarde.\nMessage d'erreur :\n" + str(e))
            logger.exception("Failed to load the archive")

    def validate_file(self):
        err = False
        err_msg = ""
        if not self.mode and not os.path.exists(self.label_file_path_value.text()):
            err = True
            err_msg += "Le fichier d'archive " + self.label_file_path_value.text() + " n'existe pas."
        self.set_warning(self.label_file_path_value, warning=err, toolTip=err_msg)

        return not err

    def validate(self):
        err = False

        if not err and not len(self.label_file_path_value.text()):
            err = True
            err_msg = "Veuillez sélectionner une archive valide."
            self.set_warning(self.label_file_path_value, err_msg)
        if not err and not self.validate_file():
            err = True
        if not err and not self.checkBox_db.isChecked() and not self.checkBox_user_db.isChecked():
            err = True
            err_msg = "Veuillez sélectionner au moins une base de données à exporter"
            self.set_warning(self.checkBox_db, err_msg)
            self.set_warning(self.checkBox_user_db, err_msg)

        return not err

    @staticmethod
    def set_warning(qobject, toolTip="", warning=True):
        if warning:
            color = 'orange'
        else:
            color = 'white'
        qobject.setStyleSheet(qobject.__class__.__name__ + "{background-color: " + color + ";}")
        qobject.setToolTip(toolTip)


class Ui_MyDialogNewCity(QDialog, Ui_DialogNewCity):
    def __init__(self, parent=None, name=None, code=0, title=None):
        super(Ui_MyDialogNewCity, self).__init__(parent)
        self.setupUi(self)

        if title:
            self.setWindowTitle(title)
        self.lineEdit_name.setText(name)
        self.spinBox_code.setValue(code)

    def getName(self):
        return self.lineEdit_name.text()

    def getCode(self):
        return self.spinBox_code.value()

    def on_lineEdit_name_textChanged(self):
        if len(self.lineEdit_name.text()) < 1:
            self.lineEdit_name.setStyleSheet("QLineEdit {background-color: orange}")
        else:
            self.lineEdit_name.setStyleSheet("QLineEdit {background-color: white}")

    def accept(self):
        if len(self.lineEdit_name.text()) < 4:
            QMessageBox.warning(self, "Impossible de créer une nouvelle commune",
                                "Veuillez entrer un nom de commune (4+ lettres)")
        else:
            super().accept()


class Ui_MyDialogNewUser(Ui_MyDialog, Ui_DialogNewUser):
    matriculeList = []  # list of currently used matricules
    matriculeCsList = []

    def __init__(self):
        super(Ui_MyDialogNewUser, self).__init__(None)
        self.setupUi(self)
        self.modifications()

    def checkLocks(self):
        if not self.NumLock or self.capsLock:
            self.lineEdit_passwd.setStyleSheet("QLineEdit {background-color: orange}")
            self.lineEdit_passwd.setToolTip("Attention : Verrouillage majuscule actif ou numérique non actif")
        else:
            self.lineEdit_passwd.setStyleSheet("QLineEdit {background-color: white}")
            self.lineEdit_passwd.setToolTip("Entrez votre mot de passe")

    def modifications(self):
        self.spinBox_matricule.setMaximum(NCODISMAX)
        self.dateEdit_involvementDate.setDate(
            QtCore.QDate(datetime.datetime.now())
        )
        for g in GRADES:
            self.comboBox_grade.addItem(g)

        rankCombo = self.comboBox_rank
        rank = sorted(RANK.items(), key=operator.itemgetter(1))  # on trie par rapport à la valeur des elmts
        # ATTENTION GRADES commence à -1
        for name, i in rank:
            rankCombo.insertItem(i + 1, name)
        rankCombo.setCurrentIndex(0)

    # override
    def accept(self):
        """
        Check wether minimum data were provided and wether an already used matricule(CS) is entered
        """
        error_msg = "Les erreurs suivantes empêchent de créer un nouvel utilisateur :\n"
        err = 0
        if not self.lineEdit_firstName.text():
            error_msg += "\tLe prénom est manquant\n"
            self.lineEdit_firstName.setStyleSheet("QLineEdit {background-color: orange; }")
            err = 1
        if not self.lineEdit_lastName.text():
            self.lineEdit_lastName.setStyleSheet("QLineEdit {background-color: orange; }")
            error_msg += "\tLe nom est manquant\n"
            err = 1
        if not self.lineEdit_passwd.text():
            self.lineEdit_passwd.setStyleSheet("QLineEdit {background-color: orange; }")
            error_msg += "\tLe mot de passe est manquant\n"
            err = 1
        if self.getMatricule() in self.matriculeList:
            error_msg += "\tLe matricule est déjà utilisé\n"
            err = 1
        if self.getMatriculeCs() in self.matriculeCsList:
            error_msg += "\tLe matricule CS est déjà utilisé\n"
            err = 1

        if err:
            logger.debug("[II] Incomplete infos")
            QMessageBox.warning(self, "Création impossible", error_msg)
        else:
            super().accept()

    # override
    def on_spinBox_matricule_valueChanged(self):
        # Done with autoconnect from Qt4
        if self.spinBox_matricule.value() in self.matriculeList:
            self.spinBox_matricule.setStyleSheet("QSpinBox { background-color: orange; }")
            self.spinBox_matricule.setToolTip("Cette valeur est déjà utilisée")
        else:
            self.spinBox_matricule.setStyleSheet("QSpinBox { background-color: none; }")
            self.spinBox_matricule.setToolTip("")

    # override
    def on_spinBox_matriculeCs_valueChanged(self):
        # Done with autoconnect from Qt4
        if self.spinBox_matriculeCs.value() in self.matriculeCsList:
            self.spinBox_matriculeCs.setStyleSheet("QSpinBox { background-color: orange; }")
            self.spinBox_matriculeCs.setToolTip("Cette valeur est déjà utilisée")
        else:
            self.spinBox_matriculeCs.setStyleSheet("QSpinBox { background-color: none; }")
            self.spinBox_matriculeCs.setToolTip("")

    # Getters
    def getGrade(self):
        return self.comboBox_grade.currentIndex() - 1

    def getFirstName(self):
        return self.lineEdit_firstName.text()

    def getLastName(self):
        return self.lineEdit_lastName.text()

    def getRank(self):
        return self.comboBox_rank.currentIndex() - 1

    def getVl(self):
        return self.checkBox_VL.isChecked()

    def getCod1(self):
        return self.checkBox_COD1.isChecked()

    def getCod2(self):
        return self.checkBox_COD2.isChecked()

    def getMatricule(self):
        return self.spinBox_matricule.value()

    def getMatriculeCs(self):
        return self.spinBox_matriculeCs.value()

    def getNumBip(self):
        return self.lineEdit_num_bip.text()

    def get_involvement_date(self):
        return self.dateEdit_involvementDate.date()


class Ui_MyProfileDialog(QDialog, Ui_ProfileDialog):
    def __init__(self, parent=None, editable=True, user_id=1):
        # super(Ui_ProfileDialog, self).__init__(parent)
        QDialog.__init__(self)

        self.editable = editable

        if user_id == 1 or user_id == 2:
            if editable:
                self.editable = False
                QMessageBox.information(self, "Édition refusée", "Vous ne pouvez pas modifier les utilisateurs"
                                                                 " Caserne et Admin. Passage en mode visualision uniquement.")

        self.spv = Personnel.get(id=user_id)

        self.setupUi(self)
        self.modifications()
        self.set_values(user_id)
        self.set_edit_permissions()

    def on_pushButton_close_pressed(self):
        self.accept()

    def on_buttonBox_clicked(self, btn):
        if self.buttonBox.buttonRole(btn) == QtWidgets.QDialogButtonBox.RejectRole:
            self.reject()
        elif self.buttonBox.buttonRole(btn) == QtWidgets.QDialogButtonBox.ApplyRole and self.validate():
            # todo : to optimize; change values directly ?
            self.spv.firstName = self.lineEdit_first_name.text()
            self.spv.lastName = self.lineEdit_last_name.text()
            self.spv.matricule = self.spinBox_matricule.value()
            self.spv.matriculeCS = self.spinBox_matricule_cs.value()
            self.spv.grade = self.comboBox_grade.currentIndex() - 1
            self.spv.rank = self.comboBox_rank.currentIndex() - 1
            self.spv.type = self.comboBox_type.currentIndex() - 1
            self.spv.VL = self.checkBox_driver_licence.isChecked()
            self.spv.COD1 = self.checkBox_cod1.isChecked()
            self.spv.COD2 = self.checkBox_cod2.isChecked()
            self.spv.set_involvement_date(self.dateEdit_involvementDate.date(), save=False)
            self.spv.set_heavy_vehicle_licence_date(self.dateEdit_heavyVehicleLicenceDate.date(), save=False)
            self.spv.num_bip = self.lineEdit_num_bip.text()
            self.spv.save()

            self.accept()

    def on_pushButton_change_password_pressed(self):
        dialog = Ui_MyDialogChangeUserPasswd(user_to_change=self.spv)
        dialog.exec_()

    @staticmethod
    def set_warning(qobject, tool_tip="", warning=True):
        if warning:
            color = 'orange'
        else:
            color = 'white'
        qobject.setStyleSheet(qobject.__class__.__name__ + "{background-color: " + color + ";}")
        qobject.setToolTip(tool_tip)

    def validate(self):
        # todo check date
        noerr = True
        if Personnel.filter(Personnel.id != self.spv.id, Personnel.matricule == self.spinBox_matricule.value()):
            noerr = False
            self.set_warning(self.spinBox_matricule, tool_tip="Valeur déjà prise par un autre sapeur.")
        if Personnel.filter(Personnel.id != self.spv.id, Personnel.matriculeCS == self.spinBox_matricule_cs.value()):
            noerr = False
            self.set_warning(self.spinBox_matricule_cs, tool_tip="Valeur déjà prise par un autre sapeur.")

        return noerr

    def modifications(self):
        self.spinBox_matricule.setMaximum(MAX_MATRICULE)

    def set_values(self, user_id):
        # TODO check errors

        self.spinBox_matricule.setMaximum(MAX_MATRICULE)
        self.spinBox_matricule_cs.setMaximum(MAX_MATRICULE)

        self.lineEdit_last_name.setText(self.spv.lastName)
        self.lineEdit_first_name.setText(self.spv.firstName)
        self.spinBox_matricule.setValue(self.spv.matricule)
        self.spinBox_matricule_cs.setValue(self.spv.matriculeCS if self.spv.matriculeCS else 0)

        self.checkBox_driver_licence.setChecked(self.spv.VL)
        self.checkBox_cod1.setChecked(self.spv.COD1)
        self.checkBox_cod2.setChecked(self.spv.COD2)

        self.lineEdit_num_bip.setText(self.spv.num_bip)

        # ComboBox
        gradesCombo = self.comboBox_grade
        grades = sorted(GRADES.items(), key=operator.itemgetter(1))  # on trie par rapport à la valeur des elmts
        # ATTENTION GRADES commence à -1
        for name, i in grades:
            gradesCombo.insertItem(i + 1, name)
        gradesCombo.setCurrentIndex(int(self.spv.grade) + 1)

        typeCombo = self.comboBox_type
        types = sorted(TYPES.items(), key=operator.itemgetter(1))  # on trie par rapport à la valeur des elmts
        # ATTENTION GRADES commence à -1
        for name, i in types:
            typeCombo.insertItem(i + 1, name)
        typeCombo.setCurrentIndex(int(self.spv.type) + 1)

        rankCombo = self.comboBox_rank
        rank = sorted(RANK.items(), key=operator.itemgetter(1))  # on trie par rapport à la valeur des elmts
        # ATTENTION GRADES commence à -1
        for name, i in rank:
            rankCombo.insertItem(i + 1, name)
        rankCombo.setCurrentIndex(int(self.spv.rank) + 1)

        hv_licence = self.spv.heavyVehicleLicenceDate
        if hv_licence is not None:
            self.dateEdit_heavyVehicleLicenceDate.setDate(
                QtCore.QDate(hv_licence.year, hv_licence.month, hv_licence.day))

        inv_date = self.spv.involvementDate
        if inv_date is not None:
            self.dateEdit_involvementDate.setDate(
                QtCore.QDate(inv_date.year, inv_date.month, inv_date.day))

    def set_edit_permissions(self):
        # Only Chef de Corps, Adjoint and SuperAdmin can
        if not self.editable or not fonctions.userHasPerm(4, allow_chef=True, allow_adjoint=True, allow_admin=True,
                                                          user=loginUser):
            self.comboBox_rank.setDisabled(True)
            self.comboBox_type.setDisabled(True)
            self.comboBox_grade.setDisabled(True)
            self.checkBox_driver_licence.setDisabled(True)
            self.checkBox_cod1.setDisabled(True)
            self.checkBox_cod2.setDisabled(True)
            self.dateEdit_involvementDate.setReadOnly(True)
            self.dateEdit_heavyVehicleLicenceDate.setReadOnly(True)
            self.lineEdit_first_name.setReadOnly(True)
            self.lineEdit_last_name.setReadOnly(True)
            self.spinBox_matricule.setReadOnly(True)
            self.spinBox_matricule_cs.setReadOnly(True)
            self.pushButton_change_password.setDisabled(True)


class Ui_MyDialogChangeUserPasswd(Ui_MyDialog, Ui_ChangePasswordDialog):
    keyPressed = QtCore.pyqtSignal(QtCore.QEvent)  # cf connexions

    # change le mot de passe de l'utilsateur courant
    # est aussi utilisé pour changer le mot de passe de la caserne

    def __init__(self, parent=None, user_to_change=None):

        super(Ui_MyDialogChangeUserPasswd, self).__init__(parent)

        self.setupUi(self)

        if user_to_change:
            self.user = user_to_change
        else:
            self.user = loginUser

    def closeEvent(self, event):
        event.accept()

    def checkLocks(self):
        if not self.NumLock or self.capsLock:
            self.lineEdit_oldPasswd.setStyleSheet("QLineEdit {background-color: orange}")
            self.lineEdit_oldPasswd.setToolTip("Attention : Verrouillage majuscule actif ou numérique non actif")
            self.lineEdit_newPasswd.setStyleSheet("QLineEdit {background-color: orange}")
            self.lineEdit_newPasswd.setToolTip("Attention : Verrouillage majuscule actif ou numérique non actif")
            self.lineEdit_newPasswd2.setStyleSheet("QLineEdit {background-color: orange}")
            self.lineEdit_newPasswd2.setToolTip("Attention : Verrouillage majuscule actif ou numérique non actif")
        else:
            self.lineEdit_oldPasswd.setStyleSheet("QLineEdit {background-color: white}")
            self.lineEdit_oldPasswd.setToolTip("Entrez votre mot de passe")
            self.lineEdit_newPasswd.setStyleSheet("QLineEdit {background-color: white}")
            self.lineEdit_newPasswd.setToolTip("Entrez votre mot de passe")
            self.lineEdit_newPasswd2.setStyleSheet("QLineEdit {background-color: white}")
            self.lineEdit_newPasswd2.setToolTip("Entrez votre mot de passe")

    def get_current_passwd(self):
        return self.lineEdit_oldPasswd.text()

    def get_new_password1(self):
        return self.lineEdit_newPasswd.text()

    def get_new_password2(self):
        return self.lineEdit_newPasswd2.text()

    def get_validated_new_passwd(self):
        return self.get_new_password1() if self.get_new_password1() == self.get_new_password2() else None

    def on_buttonBox_accepted(self):
        if self.validate():
            r = fonctions_self.changeUserPasswd(self, user_to_change=self.user, passwd=self.get_current_passwd(),
                                                new=self.get_validated_new_passwd())
            if r == 0:
                self.close()
            else:
                self.lineEdit_oldPasswd.setStyleSheet("QLineEdit {background-color: orange}")

    def on_buttonBox_rejected(self):
        self.close()

    def validate(self):
        self.lineEdit_oldPasswd.setStyleSheet("QLineEdit {background-color: white}")
        self.lineEdit_newPasswd.setStyleSheet("QLineEdit {background-color: white}")
        self.lineEdit_newPasswd2.setStyleSheet("QLineEdit {background-color: white}")
        code = self.get_new_password1() == self.get_new_password2() and len(self.get_new_password1()) > 0
        if not code:
            self.lineEdit_newPasswd.setStyleSheet("QLineEdit {background-color: orange}")
            self.lineEdit_newPasswd2.setStyleSheet("QLineEdit {background-color: orange}")
        return code


class Ui_Login(QDialog):
    keyPressed = QtCore.pyqtSignal(QtCore.QEvent)

    def __init__(self, parent=None):
        super(Ui_Login, self).__init__(parent)
        """
        afin de lister les spv dans l'ordre alphabétique et pouvoir récupérer le bon utilisateur il ne faut pas que
        l'ordre par nom/prénom ait changé entre le moment où l'utilisateur demande la connexion et le momement où il la
        valide. (ex: ajout d'un nouvel utilisateur, ce cas est quasi improbable mais pourrait se poser si utilisation
        de plusieurs utilisateurs, par ex avec la connexion à ditance et/ou django)
        listSpv est alors passée en argument à modification() (pour lister)
        et on écrit self.listSpv pour pouvoir récupérer la valeur en appelant Ui_login().listSpv
        dans login() (fonctions_self)

        On exclus l'entrée id==1 car celle-ci correspond au compte général (ou nobody) qui est affiché en premier pour
        des raisons d'ésthétique. Il faut donc le supprimer pour éviter d'avoir un doublon, le compte nobody étant
        traîté sépraément.
        """
        self.listSpv = Personnel.select().where(Personnel.id != 1).order_by(Personnel.lastName, Personnel.firstName)

        self.NumLock = True
        self.capsLock = False
        out = None
        try:
            out = subprocess.check_output(['xset q | grep "Caps"'], shell=True)
        except Exception as e:
            logger.exception("Cannot access to keyboard :")

        if out:
            out = out.decode("utf-8")  # because get binary
            if out[23] == "f":
                self.capsLock = False
            else:
                self.capsLock = True
            if out[47] == "f":
                self.NumLock = False
            else:
                self.NumLock = True
        self.setupUi(self)
        self.connexions()
        self.modifications(self.listSpv)
        global selfi
        selfi = self

    def connexions(self):
        self.buttonBox.rejected.connect(self.reject)
        self.keyPressed.connect(self.on_key)

    def keyPressEvent(self, event):
        logger.debug("[II] Event emit " + str(event))
        super(Ui_Login, self).keyPressEvent(event)
        self.keyPressed.emit(event)

    def on_buttonBox_accepted(self):
        global loginUser
        user = self.comboBox.currentIndex()  # récupère le rang de l'item (correspond à id-1 car id commence à 1)
        if user == 0:  # si connexion au compte général (toujours en premier)
            user = Personnel.get(id=1)
            loginUser = user
            self.accept()
        else:
            user = self.listSpv[user - 1]

            hashedPassword = self.lineEdit.text().encode()  # recupere et encode en bytes le mdp fournit
            # On vérifie si le hash du mot de passe fourni (généré sans doute avec le même salt que l'initial d'où le
            #  user.password.encode() ) est égal à la valeur stockée (on appelle .encore() pour que python prenne
            #  la chaine de caractère en bytes.
            if bcrypt.hashpw(hashedPassword, user.password.encode()) == user.password.encode():
                loginUser = user
                logger.info('Connecté en tant que : ' + str(loginUser.id) + " " + str(loginUser.firstName) + " " + str(
                    loginUser.lastName))

                self.accept()
            else:
                QMessageBox.critical(self, "Connexion échouée", "Le mot de passe rentré est invalide.")
                logger.debug("Mot de passe invalide")

    def on_key(self, event):
        if event.key() == QtCore.Qt.Key_CapsLock:
            self.capsLock = not self.capsLock
            self.checkLocks()
        elif event.key() == QtCore.Qt.Key_NumLock:
            self.NumLock = not self.NumLock
            self.checkLocks()

    def checkLocks(self):
        if not self.NumLock or self.capsLock:
            self.lineEdit.setStyleSheet("QLineEdit {background-color: orange}")
            self.lineEdit.setToolTip("Attention : Verrouillage majuscule actif ou numérique non actif")
        else:
            self.lineEdit.setStyleSheet("QLineEdit {background-color: white}")
            self.lineEdit.setToolTip("Entrez votre mot de passe")

    def modifications(self, listSpv):
        """
        Ajout de la liste des pompiers
        L'utilisateur d'id=1 Aucun Aucun sert comme compte général
        Dans la liste il est renommé en "Compte Caserne" cf première ligne
        :param listSpv:
        """
        self.comboBox.addItem("Compte Caserne")  # Ajoute le compte général
        for spv in listSpv:
            if spv.id != 1:  # Enlève le compte nobody cf ligne d'avant
                self.comboBox.addItem(spv.lastName + " " + spv.firstName)
        self.checkLocks()

    def setupUi(self, Login):
        Login.setObjectName("Login")
        Login.resize(330, 112)
        self.gridLayout_2 = QtWidgets.QGridLayout(Login)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_2 = QtWidgets.QLabel(Login)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)
        self.comboBox = QtWidgets.QComboBox(Login)
        self.comboBox.setObjectName("comboBox")
        self.gridLayout_2.addWidget(self.comboBox, 0, 1, 1, 1)
        self.label = QtWidgets.QLabel(Login)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(Login)
        self.lineEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout_2.addWidget(self.lineEdit, 1, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Login)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Abort | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_2.addWidget(self.buttonBox, 2, 1, 1, 1)

        self.retranslateUi(Login)
        QtCore.QMetaObject.connectSlotsByName(Login)

    def retranslateUi(self, Login):
        _translate = QtCore.QCoreApplication.translate
        Login.setWindowTitle(_translate("Login", "Connexion"))
        self.label_2.setText(_translate("Login", "Mot de passe"))
        self.label.setText(_translate("Login", "Utilisateur"))
        self.lineEdit.setToolTip(_translate("Login", "Entrez votre mot de passe"))


class Ui_MyInterListFPT(QDialog, Ui_Dialog_interFPT):
    def __init__(self, selfi, fpt_id_list: list = None, fpt_id=-1):
        QDialog.__init__(self, None)
        self.setupUi(self)

        # Define var
        if Ui_Dialog_interFPT:
            self.FptIdList = fpt_id_list
        else:
            self.FptIdList = []
        self.FptId = fpt_id

        self.selfi = selfi
        self.modifications()
        self.connexions()

    def connexions(self):
        self.buttonBox.accepted.connect(self.buttonBoxAccepted)

    def showIt(self):
        self.updateAvailableVehicles()
        self.show()

    # EVENTS

    def buttonBoxAccepted(self):
        # Append current fpt id_ to the inter_Window
        id_ = self.comboBox_type.currentData()  # Get the vehicle ID
        # If vehicle is edited, we musn't add again the id_ in the list
        if id_ not in self.FptIdList:
            if self.FptId > -1:
                # If the vehicle already exists, remove previous id_ from VlIdList
                self.FptIdList.remove(self.FptId)
            self.FptIdList.append(id_)

        # Update current vehicle ID with new one
        self.FptId = id_

        self.hide()

        # Update parent window
        vehicleName = self.comboBox_type.currentText()
        ca = self.comboBox_ca.currentText()
        con = self.comboBox_con.currentText()
        self.selfi.updateTableWidgetItem(self, id_, vehicleName, ca, con)

    # MODIFICATIONS
    def updateAvailableVehicles(self):
        list = VehicleFPT.filter(VehicleFPT.status == 1).where(VehicleFPT.id.not_in(self.FptIdList))
        currentItem = self.comboBox_type.currentText()  # Selected item
        currentItemData = self.comboBox_type.currentData()  # Select item id

        self.comboBox_type.clear()  # Remove all items

        i = 0  # i: position where to insert the currentItem
        for vehi in list:
            if vehi.toString() < currentItem:  # if we haven't yet reach the current item
                i += 1
            self.comboBox_type.addItem(vehi.toString(), vehi.id)

        self.comboBox_type.insertItem(i, currentItem, currentItemData)
        self.comboBox_type.setCurrentIndex(i)

    def modifications(self):
        # Append all "status == ok" vehicles
        list = VehicleFPT.filter(VehicleFPT.status == 1).where(VehicleFPT.casID.not_in(self.FptIdList))
        for fpt in list:
            self.comboBox_type.addItem(fpt.toString(), fpt.id)
        # Aucun Aucun
        spv_dummy = Personnel.get(id=1)
        # CON
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.COD1 == True))).order_by(Personnel.firstName)
        self.comboBox_con.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_con.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # CA
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 6))).order_by(
            Personnel.firstName)
        self.comboBox_ca.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_ca.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # CE BAT
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 4))).order_by(
            Personnel.firstName)
        self.comboBox_ceBat.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_ceBat.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # CE BAL
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 4))).order_by(
            Personnel.firstName)
        self.comboBox_ceBal.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_ceBal.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # EQU BAT
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 3))).order_by(
            Personnel.firstName)
        self.comboBox_equBat.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_equBat.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # EQU BAL
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 3))).order_by(
            Personnel.firstName)
        self.comboBox_equBal.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_equBal.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # STAG
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade <=2) & (Personnel.grade >= 0))).order_by(
            Personnel.firstName)
        self.comboBox_stag.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_stag.addItem(spv.firstName + " " + spv.lastName, spv.id)

    def disableModifications(self):
        self.comboBox_type.setEnabled(False)
        self.comboBox_ca.setEnabled(False)
        self.comboBox_con.setEnabled(False)
        self.comboBox_ceBat.setEnabled(False)
        self.comboBox_ceBal.setEnabled(False)
        self.comboBox_equBat.setEnabled(False)
        self.comboBox_equBal.setEnabled(False)
        self.comboBox_stag.setEnabled(False)


class Ui_MyInterListVL(QDialog, Ui_Dialog_interVL):
    def __init__(self, selfi, vl_id_list: list = None, vl_id=-1):
        """

        :param selfi: self of the parent widget
        :param vl_id_list:
        :param vl_id: id of the current (modified/viewed) vehicle (if id -1 then the vehicle is being created)
        :return:
        """
        QDialog.__init__(self, None)
        self.setupUi(self)

        # Def var
        if vl_id_list:
            self.VlIdList = vl_id_list
        else:
            self.VlIdList = []
        self.VlId = vl_id

        self.selfi = selfi
        self.modifications()

        self.connexions()

    def connexions(self):
        self.buttonBox.accepted.connect(self.buttonBoxAccepted)

    def showIt(self):
        self.updateAvailableVehicles()
        self.show()

    # EVENTS
    def updateAvailableVehicles(self):
        list = VehicleVL.filter(VehicleVL.status == 1).where(VehicleVL.id.not_in(self.VlIdList))
        currentItem = self.comboBox_type.currentText()  # Selected item
        currentItemData = self.comboBox_type.currentData()

        self.comboBox_type.clear()  # Remove all items

        i = 0  # i: position where to insert the currentItem
        for vehi in list:
            if vehi.toString() < currentItem:  # if we haven't yet reach the current item
                i += 1
            self.comboBox_type.addItem(vehi.toString(), vehi.id)

        self.comboBox_type.insertItem(i, currentItem, currentItemData)
        self.comboBox_type.setCurrentIndex(i)

    def buttonBoxAccepted(self):
        # Append current fpt id_ to the inter_Window
        id_ = self.comboBox_type.currentData()  # Get the vehicle ID
        # If vehicle is edited, we musn't add again the id_ in the list
        if id_ not in self.VlIdList:
            if self.VlId > -1:
                # If the vehicle already exists, remove previous id_ from VlIdList
                self.VlIdList.remove(self.VlId)
            self.VlIdList.append(id_)

        # Update new id_
        self.VlId = id_

        self.hide()

        # Update parent window
        vehicleName = self.comboBox_type.currentText()
        ca = self.comboBox_ca.currentText()
        con = self.comboBox_con.currentText()
        self.selfi.updateTableWidgetItem(self, id_, vehicleName, ca, con)

    # MODIFICATIONS
    def modifications(self):
        # Append all "status == ok" vehicles
        list = VehicleVL.filter(VehicleVL.status == 1).where(VehicleVL.casID.not_in(self.VlIdList))
        for vl in list:
            self.comboBox_type.addItem(vl.toString(), vl.id)
        # Aucun Aucun
        spv_dummy = Personnel.get(id=1)
        # CON
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.VL == True))).order_by(Personnel.firstName)
        self.comboBox_con.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_con.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # CA
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 4))).order_by(
            Personnel.firstName)
        self.comboBox_ca.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_ca.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # EQU
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 3))).order_by(
            Personnel.firstName)
        self.comboBox_equ.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_equ.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # STAG
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade <=2) & (Personnel.grade >= 0))).order_by(
            Personnel.firstName)
        self.comboBox_stag.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_stag.addItem(spv.firstName + " " + spv.lastName, spv.id)

    def disableModifications(self):
        self.comboBox_type.setEnabled(False)
        self.comboBox_ca.setEnabled(False)
        self.comboBox_con.setEnabled(False)
        self.comboBox_equ.setEnabled(False)
        self.comboBox_stag.setEnabled(False)


class Ui_MyInterListVTU(QDialog, Ui_Dialog_interVTU):
    def __init__(self, selfi, vtu_id_list: list = None, vtu_id=-1):
        QDialog.__init__(self, None)
        self.setupUi(self)

        # Def var
        if vtu_id_list:
            self.VtuIdList = vtu_id_list
        else:
            self.VtuIdList = []
        self.VtuId = vtu_id
        self.selfi = selfi

        self.modifications()
        self.connexions()

    def connexions(self):
        self.buttonBox.accepted.connect(self.buttonBoxAccepted)

    def showIt(self):
        self.updateAvailableVehicles()
        self.show()

    # EVENTS
    def updateAvailableVehicles(self):
        list = VehicleVTU.filter(VehicleVTU.status == 1).where(VehicleVTU.id.not_in(self.VtuIdList))
        currentItem = self.comboBox_type.currentText()  # Selected item
        currentItemData = self.comboBox_type.currentData()

        self.comboBox_type.clear()  # Remove all items

        i = 0  # i: position where to insert the currentItem
        for vehi in list:
            if vehi.toString() < currentItem:  # if we haven't yet reach the current item
                i += 1
            self.comboBox_type.addItem(vehi.toString(), vehi.id)

        self.comboBox_type.insertItem(i, currentItem, currentItemData)
        self.comboBox_type.setCurrentIndex(i)

    def buttonBoxAccepted(self):
        # Append current vehicle id_ to the inter_Window
        id_ = self.comboBox_type.currentData()  # Get the vehicle ID

        # If vehicle is edited, we musn't add again the id_ in the list
        if id_ not in self.VtuIdList:
            if self.VtuId > -1:
                # If the vehicle already exists, remove previous id_ from VlIdList
                self.VtuIdList.remove(self.VtuId)
            self.VtuIdList.append(id_)

        # Update current vehicle ID with new one
        self.VtuId = id_

        self.hide()

        # Update parent window
        vehicleName = self.comboBox_type.currentText()
        ca = self.comboBox_ca.currentText()
        con = self.comboBox_con.currentText()
        self.selfi.updateTableWidgetItem(self, id_, vehicleName, ca, con)

    def modifications(self):
        # Append all "status == ok" vehicles
        list = VehicleVTU.filter(VehicleVTU.status == 1).where(VehicleVTU.casID.not_in(self.VtuIdList))
        for vtu in list:
            self.comboBox_type.addItem(vtu.toString(), vtu.id)
        # Aucun Aucun
        spv_dummy = Personnel.get(id=1)
        # CON
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.VL == True))).order_by(Personnel.firstName)
        self.comboBox_con.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_con.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # CA
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 4))).order_by(
            Personnel.firstName)
        self.comboBox_ca.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_ca.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # EQU
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade >= 3))).order_by(
            Personnel.firstName)
        self.comboBox_equ.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_equ.addItem(spv.firstName + " " + spv.lastName, spv.id)
        # STAG
        list = Personnel.filter(Personnel.type <= 0, ((Personnel.id != spv_dummy.id) & (Personnel.grade <=2) & (Personnel.grade >= 0))).order_by(
            Personnel.firstName)
        self.comboBox_stag.addItem(spv_dummy.firstName + " " + spv_dummy.lastName, spv_dummy.id)
        for spv in list:
            self.comboBox_stag.addItem(spv.firstName + " " + spv.lastName, spv.id)

    def disableModifications(self):
        self.comboBox_type.setEnabled(False)
        self.comboBox_ca.setEnabled(False)
        self.comboBox_con.setEnabled(False)
        self.comboBox_equ.setEnabled(False)
        self.comboBox_stag.setEnabled(False)


class Ui_MyVehicleManagement(Ui_MyMainWindow, Ui_VehicleManagement):
    closed = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        if fonctions.userHasPerm(3, True, True, user=loginUser):
            self.setupUi(self)
            self.applyUserPreferences()
            self.showVehicleList()
            self.connexions()
            self.center()
        else:
            raise PermissionError

    def closeEvent(self, event):
        self.closed.emit()
        event.accept()

    def applyUserPreferences(self):
        # TODO : understand :: loginUser.UserPreferences_User is a list
        logger.debug("Préférences utilisateur ; " + str(loginUser.UserPreferences_User))
        user_pref = loginUser.UserPreferences_User
        if user_pref:
            user_pref = user_pref[0]
            show_all = user_pref.vehicleManagementShowAll
            self.checkBox_showDeactivatedVehicles.setChecked(show_all)
        else:
            QMessageBox.information(self, "Initialisation", "Vos préféreces n'ont pas été initialisées")
            UserPreferences.create(user=loginUser)

    def connexions(self):
        self.tableWidget_vehicle.cellChanged.connect(self.on_cellChanged)
        self.buttonBox.rejected.connect(self.close)
        self.pushButton_addVehicle.clicked.connect(self.createNewVehicle)
        self.pushButton_deactivateVehicle.clicked.connect(partial(self.deactivateVehicle, 0))
        self.pushButton_activateVehicle.clicked.connect(partial(self.deactivateVehicle, 1))
        self.comboBox_sortByType.currentIndexChanged.connect(self.showVehicleList)
        self.checkBox_showDeactivatedVehicles.toggled.connect(self.on_checkBox_showDeactivatedVehiclesToggled)

    # SIGNALS
    def on_checkBox_showDeactivatedVehiclesToggled(self):
        # Update user preferences
        user_pref = UserPreferences.get(user=loginUser)
        user_pref.vehicleManagementShowAll = self.checkBox_showDeactivatedVehicles.isChecked()
        user_pref.save()
        # update the view
        self.showVehicleList()

    def createNewVehicle(self):
        w = Ui_Dialog_VehicleTypeChoice()
        if w.exec_():
            try:
                if w.choice == "FPT" or w.choice == "FPTL":
                    logger.info("[II] Create new FPT*")
                    # Select fpt with only casID column
                    fpt = VehicleFPT.select(VehicleFPT.casID)
                    i = 0
                    for v in fpt:
                        # Search first available casID
                        if i == v.casID:
                            i += 1
                        else:
                            break

                    VehicleFPT.create(casID=i, status=1)
                elif w.choice == "VL":
                    logger.info("[II] Create new VL*")
                    # Select fpt with only casID column
                    vl = VehicleVL.select(VehicleVL.casID)
                    i = 0
                    for v in vl:
                        # Search first available casID
                        if i == v.casID:
                            i += 1
                        else:
                            break
                    VehicleVL.create(casID=i, status=1)
                elif w.choice == "VTU":
                    logger.info("[II] Create new VTU*")
                    # Select fpt with only casID column
                    vtu = VehicleVTU.select(VehicleVTU.casID)
                    i = 0
                    for v in vtu:
                        # Search first available casID
                        if i == v.casID:
                            i += 1
                        else:
                            break
                    VehicleVTU.create(casID=i, status=1)
                self.showVehicleList()
            except Exception as e:
                logger.exception("Impossible de créer un nouveau véhicule ")
                QMessageBox.critical(self, "Erreur", "Impossible de créer une nouvelle entitée :\n" + str(e))

    def deactivateVehicle(self, action):
        """
        Activate or Deactivate (depending on action value) the selected vehicles
        :param action: 0= deactivate, 1=activate
        :return:
        """
        selected_items = self.tableWidget_vehicle.selectedItems()  # selected items: mays have many for the same row
        deactivated_row = []  # store already treated rows
        for i in selected_items:
            row = i.row()
            if not (row in deactivated_row):  # if current row hasn't already been done
                deactivated_row.append(row)
                id_ = int(self.tableWidget_vehicle.item(row, 0).text())
                # we get the name column content then we through out all char after "-" in "FTP-y" "VL-x" ...
                # and we check if it's FPT, VL, VTU, ...
                type_ = self.tableWidget_vehicle.item(row, 2).text().split('-')[0]
                if type_ == "FPT":
                    vehi = VehicleFPT.get(id=id_)
                elif type_ == "VL":
                    vehi = VehicleVL.get(id=id_)
                elif type_ == "VTU":
                    vehi = VehicleVTU.get(id=id_)
                else:
                    vehi = None

                if vehi and vehi.status != action:
                    vehi.status = action
                    vehi.save()
        self.showVehicleList()  # update display

    def on_cellChanged(self):
        item = self.tableWidget_vehicle.currentItem()
        if item:
            row = item.row()
            col = item.column()
            newValue = item.text()
            id_ = int(self.tableWidget_vehicle.item(row, 0).text())
            # we get the name column content then we through out all char after "-" in "FTP-y" "VL-x" ...
            # and we check if it's FPT, VL, VTU, ...
            type = self.tableWidget_vehicle.item(row, 2).text().split('-')[0]

            vehi = None
            if type == "FPT":
                vehi = VehicleFPT.get(id=id_)
            elif type == "VL":
                vehi = VehicleVL.get(id=id_)
            elif type == "VTU":
                vehi = VehicleVTU.get(id=id_)

            if vehi:
                if col == 1:
                    save_cas_id = None
                    try:
                        # execption if casID already in use
                        save_cas_id = vehi.casID
                        vehi.casID = int(newValue)
                        vehi.save()
                        # Change the name of the vehicle
                        self.tableWidget_vehicle.item(row, 2).setText(vehi.toString())
                    except Exception as e:
                        QMessageBox.warning(self, "Opération impossible", "L'id_ " + newValue + " est déjà utilisé par"
                                                                                                " un autre véhicule du même type (" + type + "). Vous ne pouvez pas le réutiliser.\n" + str(
                            e))
                        logger.exception(self, "Failed to set vehicle Id")
                        # Restore default value
                        vehi.casID = save_cas_id
                        vehi.save()
                        item.setText(str(save_cas_id))
                elif col == 2:
                    pass  # cannot change name
                elif col == 3:
                    vehi.nickName = newValue
                elif col == 4:
                    vehi.status = int(newValue)
                elif col == 5:
                    vehi.km = int(newValue)
                elif col == 6:
                    vehi.currentKm = int(newValue)
                vehi.save()

    # MODIFICATIONS
    def showVehicleList(self):
        if self.comboBox_sortByType.currentText() == "FPT":
            if self.checkBox_showDeactivatedVehicles.isChecked():
                vehicleList = VehicleFPT.select()
            else:
                vehicleList = VehicleFPT.filter(VehicleFPT.status == 1)  # Show only "ok" vehicles
            self.tableWidget_vehicle.setRowCount(vehicleList.count())
            self.addVehicleItems(vehicleList, vehicleList.count())
        elif self.comboBox_sortByType.currentText() == "VL":
            if self.checkBox_showDeactivatedVehicles.isChecked():
                vehicleList = VehicleVL.select()
            else:
                vehicleList = VehicleVL.filter(VehicleVL.status == 1)  # Show only "ok" vehicles
            self.tableWidget_vehicle.setRowCount(vehicleList.count())
            self.addVehicleItems(vehicleList, vehicleList.count())
        elif self.comboBox_sortByType.currentText() == "VTU":
            if self.checkBox_showDeactivatedVehicles.isChecked():
                vehicleList = VehicleVTU.select()
            else:
                vehicleList = VehicleVTU.filter(VehicleVTU.status == 1)  # Show only "ok" vehicles
            self.tableWidget_vehicle.setRowCount(vehicleList.count())
            self.addVehicleItems(vehicleList, vehicleList.count())
        else:
            import itertools
            # Chain the querysets (DO NOT USE Vehicle.select() | Vehicle2.select() because only the method of the first are used
            if self.checkBox_showDeactivatedVehicles.isChecked():
                vFPT = VehicleFPT.select()
                vVL = VehicleVL.select()
                vVTU = VehicleVTU.select()
            else:  # Show only "ok" vehicles
                vFPT = VehicleFPT.filter(VehicleFPT.status == 1)
                vVL = VehicleVL.filter(VehicleVL.status == 1)
                vVTU = VehicleVTU.filter(VehicleVTU.status == 1)
            vCount = vFPT.count() + vVL.count() + vVTU.count()
            v = itertools.chain(vFPT, vVL, vVTU)
            self.tableWidget_vehicle.setRowCount(vCount)
            self.addVehicleItems(v, vCount)

    def addVehicleItems(self, vehicleList, vehicleListCount):
        for (row, vehicle) in zip(range(vehicleListCount), vehicleList):
            item = QtWidgets.QTableWidgetItem()
            item.setFlags(QtCore.Qt.ItemIsSelectable)
            item.setData(2, vehicle.id)
            self.tableWidget_vehicle.setItem(row, 0, item)
            item = QtWidgets.QTableWidgetItem()
            item.setData(2, vehicle.casID)
            self.tableWidget_vehicle.setItem(row, 1, item)
            item = QtWidgets.QTableWidgetItem()
            item.setData(2, vehicle.toString())
            self.tableWidget_vehicle.setItem(row, 2, item)
            item = QtWidgets.QTableWidgetItem()
            item.setData(2, vehicle.nickName)
            self.tableWidget_vehicle.setItem(row, 3, item)
            item = QtWidgets.QTableWidgetItem()
            item.setData(2, vehicle.status)
            self.tableWidget_vehicle.setItem(row, 4, item)
            item = QtWidgets.QTableWidgetItem()
            item.setData(2, vehicle.km)
            self.tableWidget_vehicle.setItem(row, 5, item)
            item = QtWidgets.QTableWidgetItem()
            item.setData(2, vehicle.currentKm)
            self.tableWidget_vehicle.setItem(row, 6, item)


class Ui_Dialog_VehicleTypeChoice(QDialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

    def on_buttonBox_accepted(self):
        self.closeIt()

    def on_buttonBox_rejected(self):
        self.close()

    def closeIt(self):
        # choice is used to get the selected  value from the user
        self.choice = self.comboBox.currentText()
        self.accept()

    def setupUi(self, Dialog_VehicleTypeChoice):
        Dialog_VehicleTypeChoice.setObjectName("Dialog_VehicleTypeChoice")
        Dialog_VehicleTypeChoice.resize(292, 108)
        self.gridLayout = QtWidgets.QGridLayout(Dialog_VehicleTypeChoice)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Dialog_VehicleTypeChoice)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.comboBox = QtWidgets.QComboBox(Dialog_VehicleTypeChoice)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.gridLayout.addWidget(self.comboBox, 1, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog_VehicleTypeChoice)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 1)

        self.retranslateUi(Dialog_VehicleTypeChoice)
        QtCore.QMetaObject.connectSlotsByName(Dialog_VehicleTypeChoice)

    def retranslateUi(self, Dialog_VehicleTypeChoice):
        _translate = QtCore.QCoreApplication.translate
        Dialog_VehicleTypeChoice.setWindowTitle(_translate("Dialog_VehicleTypeChoice", "Choix du type de véhicule"))
        self.label.setText(_translate("Dialog_VehicleTypeChoice", "Quel type de véhicules voulez-vous ajouter ?"))
        self.comboBox.setItemText(0, _translate("Dialog_VehicleTypeChoice", "FPT"))
        self.comboBox.setItemText(1, _translate("Dialog_VehicleTypeChoice", "VL"))
        self.comboBox.setItemText(2, _translate("Dialog_VehicleTypeChoice", "VTU"))


class fonctions_self:  # Fonctions faisant appel à self

    def migrate_databases(self, backup=True):
        """
        Migrate databases and make backup
        :return:
        """
        global db_is_up_to_date
        success = True
        if backup:
            Ui_MyDialogDB(self, mode=1).exec_()
        QMessageBox.information(self, "Migration", "La migration va démarrer. Une fenêtre apparaîtra lorsque la"
                                                   " migration sera terminée. Veuillez ne pas fermer PGLX en"
                                                   " attendant.")
        database.migrate_db(DB_VERSION)
        db_is_up_to_date = database.check_versions(DB_VERSION)
        if db_is_up_to_date:
            QMessageBox.information(self, "Migration terminée", "La migration s'est correctement terminée.")
        else:
            QMessageBox.critical(self, "Migration interrompue", "La migration ne s'est pas correctement terminée.")
            success = False

        return success

    def fmaSaveToDb(self: Ui_MyFMA):
        logger.debug('[II] Enregistrement des données dans la base de donnée en cours ...')

        theme_fma = str(self.lineEdit_formation.text())
        name = self.lineEdit_name.text()
        date_debut = QtCore.QDateTime(self.dateTimeEdit_start.dateTime()).toString(1).replace("T", " ")
        date_fin = QtCore.QDateTime(self.dateTimeEdit_end.dateTime()).toString(1).replace("T", " ")
        lieu = self.lineEdit_lieu.text()
        rapport = self.textEdit.toPlainText()
        type_ = self.comboBox_type.currentIndex()

        # reacteur définit directement dans create()

        formateurs = []
        row_count = self.tableWidget_trainer.rowCount()
        for i in range(0, row_count):
            # "prenom nom";"formation"
            formateurs.append(
                self.tableWidget_trainer.item(i, 0).text() + ';' + self.tableWidget_trainer.item(i, 1).text())

        spv_formes = []
        row_count = self.tableWidget_trainee.rowCount()
        for i in range(0, row_count):
            # The name is QLineEdit (for auto-completion)
            name = self.tableWidget_trainee.cellWidget(i, 0).text()
            if name != "":
                spv_formes.append(name + ';' + self.tableWidget_trainee.item(i, 1).text())

        vehicules = []
        row_count = self.tableWidget_vehicle.rowCount()
        for i in range(0, row_count):
            vehicules.append(
                self.tableWidget_vehicle.item(i, 0).text() + ';' + self.tableWidget_vehicle.item(i, 1).text())

            logger.debug('écriture dans la base de données')
        if modeAccesFma[0] == 1:
            fma = Fma.get(id=modeAccesFma[1])
            fma.name = name
            fma.type = type_
            fma.dateDebut = date_debut
            fma.dateFin = date_fin
            # fma.dateRedaction=datetime.datetime.now() todo: utiliser dateModification
            fma.theme = theme_fma
            fma.rapport = rapport
            fma.lieu = lieu
            fma.vehicules = vehicules
            fma.autresFormateurs = formateurs
            fma.autresSpvFormes = spv_formes
            fma.save()

            #  fma.nFma isn't changed, but needed for the redaction
            nb_fma = fma.nFma
        else:
            # If creation > set nbFame, otherwise, doesn't change
            nb_fma = Fma.filter(Fma.dateDebut.year == datetime.datetime.now().year).count() + 1
            fma = Fma.create(nFma=nb_fma, dateDebut=date_debut, dateFin=date_fin, dateRedaction=datetime.datetime.now(),
                             theme=theme_fma, name=name, rapport=rapport, lieu=lieu, vehicules=vehicules,
                             autresFormateurs=formateurs, autresSpvFormes=spv_formes, redacteur=loginUser, type=type_)

        global self_ui_inter
        self_Ui_Inter = self_ui_inter
        fonctions_self.close_window(self, self_Ui_Inter)

        fonctions.redigerFma(self, fma)

    def login(self: Ui_MyPompierGLX):
        global loginUser  # permet de modifier loginUser (sinon ne marche pas)
        logger.debug('[II] loginUser.id ' + str(loginUser.id))
        if loginUser.id == 1:  # si l'utilisateur est co sous le compte général >> veut se connecter
            dialog = Ui_Login()
            a = dialog.exec_()
            if a:
                self.modifications()

        else:  # si l'utilisateur est déjà connecté >> il veut se déconnecter
            loginUser = Personnel.get(id=1)  # l'utilisateur n'est plus connecté
            self.modifications()  # on actualise le texte du boutton de connexion/déconnexion

    def changeUserPasswd(self, user_to_change, passwd, new):
        """
        # explication du codage dans editStaffMemberPassword
        :param user_to_change:
        :param passwd: current passwd given
        :param new: new desired password
        :return: 0 if OK, else 1 or 2 (if try to edit basic user)
        """
        code = 1
        user = loginUser
        user_passwd = user.password
        if not isinstance(user_passwd, bytes):
            user_passwd = user_passwd.encode()
        passwd = passwd.encode()

        if (fonctions.userHasPerm(3, user=loginUser) or loginUser == user_to_change) and bcrypt.hashpw(passwd,
                                                                                                       user_passwd) == user_passwd:
            if user_to_change.id == 1 and fonctions.userHasPerm(3, user=loginUser):
                QMessageBox.warning(self, 'Permissions refusée',
                                    'Seuls les administrateurs peuvent changer le mot de passe du compte Caserne')
                code = 2
            else:
                hash = bcrypt.hashpw(new.encode(), bcrypt.gensalt())  # hash
                user_to_change.password = hash
                user_to_change.save()
                code = 0
        else:
            QMessageBox.warning(self, 'Échec de la vérification', 'Le mot de pase actuel saisie n\'est pas correcte')

        return code

    def fma(self: Ui_MyMainWindow):
        global self_ui_inter
        """ Si l'utilisateur a le droit d'écriture ou si il a que le droit de lecture et veux lire """
        logger.debug("Rang utilisateur, modeAccesFma " + str(loginUser.rank) + " " + str(modeAccesFma))
        self_ui_inter = self
        logger.debug("FMA")
        self.centralwidget.setEnabled(False)
        try:
            mySW = Ui_MyFMA(self)
            mySW.show()
        except Exception as e:
            QMessageBox.critical(self, "Erreur", "Une erreur est survenue :\n" + str(e))
            logger.exception("Failed to show/edit inter")
            # RETURN
            pass
        except PermissionError:
            QMessageBox.warning(self, "Accès interdit", "Vous n'avez par les permissions suffisantes pour rédiger un "
                                                    "rapport de fma.\nVeuillez vériifier que vous êtes connecté sur "
                                                    "votre compte")

        except BaseException:
            # BaseException == user do not want to write report with sys account
            pass

        self.centralwidget.setEnabled(True)

    def DialogChangeUserPasswd(self):
        # Change mdp de l'utilisateur courant

        if loginUser.id == 1:
            QMessageBox.warning(self, 'Permissions refusée',
                                'Seuls les administrateurs peuvent changer le mot de passe du compte Caserne')
        else:
            mySW = Ui_MyDialogChangeUserPasswd()
            mySW.exec_()

    def cancel_RapportInterEdit(self, self_ui_inter):
        global modeAccesRapport
        """
        Fonction crée car self_ui_inter est modifié et donc
        self_ui_inter.centralWidget.setEnable(True) ne marche plus
        """
        modeAccesRapport = [2, -1]  # réinitialise pour éviter les boucles
        fonctions_self.close_window(self, self_ui_inter)

    def cancel_RapportFmaEdit(self, self_ui_inter):
        global modeAccesFma
        """
        Fonction crée car self_ui_inter est modifié et donc
        self_ui_inter.centralWidget.setEnable(True) ne marche plus
        """
        modeAccesFma = [2, -1]  # réinitialise pour éviter les boucles
        fonctions_self.close_window(self, self_ui_inter)

    def newInterPath(self, cas: Caserne):
        global path_to_rinter
        path = QtWidgets.QFileDialog.getExistingDirectory(self, "Open", cas.pathInter)

        check_and_create_report_dir(path, cas.pathInterByYear)

        if os.path.exists(path):
            logger.info('[II] Nouveau chemin pour les interventions : ' + str(path))
            cas.pathInter = path

            # Si pas d'erreur on change le chemin
            # ATTENTION: ici on ne met pas l'année en plus si rangée par année car fait dans UpdateVar()
            # Deplus, dans la base de donnée le chemin ne doit JAMAIS être avec l'année (booléen pour cela)
            path_to_rinter = path
            cas.save()

    def newFmaPath(self, cas: Caserne):
        global path_to_rfma
        path = QtWidgets.QFileDialog.getExistingDirectory(self, "Open", path_to_rfma)

        check_and_create_report_dir(path, cas.pathFmaByYear)

        if os.path.exists(path):
            logger.info('[II] Nouveau chemin pour les FMA :' + str(path))
            cas.pathFma = path

            # Si pas d'erreur on change le chemin
            # ATTENTION: ici on ne met pas l'année en plus si rangée par année car fait dans UpdateVar()
            # Deplus, dans la base de donnée le chemin ne doit JAMAIS être avec l'année (booléen pour cela)
            path_to_rfma = path
            cas.save()

    def newCasernementPath(self, cas: Caserne):
        global path_to_rcasernement
        path = QtWidgets.QFileDialog.getExistingDirectory(self, "Open", path_to_rcasernement)

        check_and_create_report_dir(path, cas.pathCasernementByYear)

        if os.path.exists(path):
            logger.info('[II] Nouveau chemin pour les casernements : ' + str(path))
            cas.pathCasernement = path

            # Si pas d'erreur on change le chemin
            # ATTENTION: ici on ne met pas l'année en plus si rangée par année car fait dans UpdateVar()
            # Deplus, dans la base de donnée le chemin ne doit JAMAIS être avec l'année (booléen pour cela)
            path_to_rcasernement = path
            cas.save()

    def addStaffMember(self):
        # Check whether the default matriculeCS is already used
        if Personnel.filter(Personnel.matriculeCS == 0):
            QMessageBox.warning(self, "Impossible de créer un nouvel utilisateur",
                                "Un utilisateur possède déjà le matricule de caserne 0.\nVeuillez changer son "
                                "matricule avant d'ajouter un nouveau personnnel.")
        else:
            # définir le mot de passe
            dialog = Ui_MyDialogNewUser()

            # Send to the dialog the lists of the matricule and matriculeCS used
            list = Personnel.select(Personnel.matricule).distinct()
            for m in list:
                dialog.matriculeList.append(m.matricule)
            list = Personnel.select(Personnel.matriculeCS).distinct()
            for m in list:
                dialog.matriculeCsList.append(m.matriculeCS)

            if dialog.exec_():  # exécute la fenêtre de mot de passe
                password = dialog.lineEdit_passwd.text().encode()  # encode en bytes
                if password:
                    firstName = dialog.getFirstName()
                    lastName = dialog.getLastName()
                    grade = dialog.getGrade()
                    rank = dialog.getRank()
                    vl = dialog.getVl()
                    cod1 = dialog.getCod1()
                    cod2 = dialog.getCod2()
                    matricule = dialog.getMatricule()
                    matriculeCS = dialog.getMatriculeCs()
                    involment_date = dialog.get_involvement_date()
                    num_bip = dialog.getNumBip()

                    hash = bcrypt.hashpw(password, bcrypt.gensalt())  # hash

                    try:
                        fireman = Personnel.create(grade=grade, matricule=matricule, type=0, firstName=firstName,
                                                   lastName=lastName, COD1=cod1, COD2=cod2, num_bip=num_bip,
                                                   VL=vl, password=hash, matriculeCS=matriculeCS, rank=rank)
                        fireman.set_involvement_date(involment_date, save=True)
                        self.add_spv(Personnel.get(id=fireman.get_id()))
                    except Exception as e:
                        QMessageBox.critical(self, "Impossible de créer un nouvel utilisateur",
                                             "Erreur détaillée: " + str(e))
                        logger.exception("Impossible de créer un nouvel utilisateur :")

    def load_to_file(self):  # ATTENTION A PRECISER LE CHEMIN DU FICHER SI IL EST DIFFERENT QUE LE CHEMIN VERS VARS

        fileName = QtWidgets.QFileDialog.getOpenFileName(self, "Open", path_to_rinter, '')

        if fileName:
            file = open(fileName[0], 'r')  # prend la 1er valeur de fileName (cf getOpenFileName)
            contenue = file.read()
            self.textEdit.setText(contenue)
            self.textEdit.setReadOnly(True)

    def staffMemberRankEdited(self: Ui_MyManageStaff, column):
        # On récupère le widget surlequel on a cliqué (et non l'item car on peut pas)
        clickme = QtWidgets.QApplication.focusWidget()
        if clickme:
            # If user clicked (otherwise, data changed by pglx itself)
            item = self.tableWidget_staff.itemAt(clickme.pos())  # On récupère l'item ayant la même position
            row = item.row()
            newGrade = self.tableWidget_staff.cellWidget(row,
                                                         column).currentIndex() - 1  # On enlève 1 car dic commence à -1

            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
            spv = Personnel.get(Personnel.id == id_)
            if spv.id != 1 and spv.id != 2:  # empêche la modification du spv nobody et administrateur
                spv.grade = newGrade
                spv.save()
            else:
                QMessageBox.warning(self, "Modification impossible", 'Vous ne pouvez pas modifier cet utilisateur.')
        else:
            logger.debug("cell edited by prog")

    def staffMemberTypeEdited(self: Ui_MyManageStaff, column):
        # On récupère le widget surlequel on a cliqué (et non l'item car on peut pas)
        clickme = QtWidgets.QApplication.focusWidget()
        if clickme:
            item = self.tableWidget_staff.itemAt(clickme.pos())  # On récupère l'item ayant la même position
            row = item.row()
            newType = self.tableWidget_staff.cellWidget(row,
                                                        column).currentIndex() - 1  # On enlève 1 car dic commence à -1

            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
            spv = Personnel.get(Personnel.id == id_)
            if spv.id != 1 and spv.id != 2:  # empêche la modification du spv nobody et administrateur
                spv.type = newType
                spv.save()
            else:
                QMessageBox.warning(self, "Modification impossible", 'Vous ne pouvez pas modifier cet utilisateur.')
        else:
            logger.debug("cell changed prgrammaticaly")

    def staffMemberInvolvmentDateEdited(self: Ui_MyManageStaff, column):
        # On récupère le widget surlequel on a cliqué (et non l'item car on peut pas)
        clickme = QtWidgets.QApplication.focusWidget()
        if clickme:
            item = self.tableWidget_staff.itemAt(clickme.pos())  # On récupère l'item ayant la même position
            row = item.row()
            involvement_date = self.tableWidget_staff.cellWidget(row,
                                                                 column).date()  # On enlève 1 car dic commence à -1
            involvement_date = involvement_date.toString("yyyy-M-d")
            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
            spv = Personnel.get(Personnel.id == id_)
            if spv.id != 1 and spv.id != 2:  # empêche la modification du spv nobody et administrateur
                spv.involvementDate = involvement_date
                spv.save()
            else:
                QMessageBox.warning(self, "Modification impossible", 'Vous ne pouvez pas modifier cet utilisateur.')

    def staffMemberPLDateEdited(self: Ui_MyManageStaff, column):
        # On récupère le widget surlequel on a cliqué (et non l'item car on peut pas)
        clickme = QtWidgets.QApplication.focusWidget()
        if clickme:
            item = self.tableWidget_staff.itemAt(clickme.pos())  # On récupère l'item ayant la même position
            row = item.row()
            heavy_vehicle_licence_date = self.tableWidget_staff.cellWidget(row,
                                                                           column).date()  # On enlève 1 car dic commence à -1
            heavy_vehicle_licence_date = heavy_vehicle_licence_date.toString("yyyy-M-d")
            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
            spv = Personnel.get(Personnel.id == id_)
            if spv.id != 1 and spv.id != 2:  # empêche la modification du spv nobody et administrateur
                spv.heavyVehicleLicenceDate = heavy_vehicle_licence_date
                spv.save()
            else:
                QMessageBox.warning(self, "Modification impossible", 'Vous ne pouvez pas modifier cet utilisateur.')

    def staffMemberEdited(self: Ui_MyManageStaff):
        # TODO: ne devrait pas exister mais être remplacé par persoSavetoDB
        # Avec liste des cellules changées pour les perfs
        item = self.tableWidget_staff.currentItem()
        if item:
            value = item.text()
            row = item.row()
            col = item.column()
            # récupération de l'id. Toujours en dernière colonne
            id_ = int(self.tableWidget_staff.item(row, self.tableWidget_staff.columnCount() - 1).text())
            spv = Personnel.get(Personnel.id == id_)
            if spv.id != 1 and spv.id != 2:  # empêche la modification du spv nobody et administrateur
                if col == 0:
                    spv.matricule = int(value)
                elif col == 1:
                    # On vérifie que le matricule n'est pas déjà utilisé par un autre spv du CS
                    if Personnel.select().where(Personnel.matriculeCS == int(value),
                                                Personnel.id != spv.id).count() != 0:
                        self.tableWidget_staff.item(row, col).setData(2, spv.matriculeCS)
                        QMessageBox.warning(self, "Modification impossible",
                                            "Le matricule est déjà utilisé par un autre sapeur")
                    else:
                        spv.matriculeCS = int(value)
                elif col == 2:
                    spv.lastName = value
                elif col == 3:
                    spv.firstName = value
                elif col == 7:
                    spv.COD1 = fonctions.str2bool(value)
                elif col == 8:
                    spv.COD2 = fonctions.str2bool(value)
                elif col == 9:
                    spv.VL = fonctions.str2bool(value)
                elif col == 12:
                    spv.rank = int(value)
                spv.save()
            else:
                QMessageBox.warning(self, "Modification impossible", 'Vous ne pouvez pas modifier cet utilisateur.')

    def close_window(self,
                     *self_ui):  # supprime les *args DONNER LE CHEMIN | ATTENTION ne pas utiliser '/' pour les nom la focntion del appelle os.chdir(path)
        self.close()
        try:
            for i in self_ui:  # réactive si besoin la fenêtre mère
                i.centralwidget.setEnabled(True)
        except Exception as e:
            logger.exception("Failed to renable central widget")
            pass

    def graphicDataFma(self):
        mois = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        current_year = datetime.datetime.now().year
        a = Fma.get(id=1)
        # for inter in (Interventions.select().where(Interventions.date_depart.year == 2015)):
        for fma in Fma.filter(Fma.dateDebut.year == current_year):
            m = fma.dateDebut.month
            mois[m - 1] += 1

            logger.debug('Graphique 1 généré: mois' + str(mois))

        fonctions_self.showFmaGraphics(self, mois)

    def showInterGraphics(self, data):
        interventions = []
        for i in data:
            interventions.append(int(i))

        x = np.arange(12)

        fig, ax = plt.subplots()
        ax.set_ylabel('Interventions')
        ax.set_title('Nombre d\'interventions par mois')
        ya = ax.get_yaxis()
        ya.set_major_locator(MaxNLocator(integer=True))

        plt.bar(x, interventions, color='g')
        plt.xticks(x + 0.5, (
        'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre',
        'Décembre'))
        plt.show()

    def showFmaGraphics(self, data):
        fma = []
        for i in data:
            fma.append(int(i))

        x = np.arange(12)

        fig, ax = plt.subplots()
        ax.set_ylabel('FMA')
        ax.set_title('Nombre de FMA par mois')
        ya = ax.get_yaxis()
        ya.set_major_locator(MaxNLocator(integer=True))

        plt.bar(x, fma, color='g')
        plt.xticks(x + 0.5, (
        'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre',
        'Décembre'))
        plt.show()


class fonctions:
    @staticmethod
    def userHasPerm(perms, allow_chef=False, allow_adjoint=False, user=None, caserne=None, allow_admin=False):
        """
        Return True if user has perms ( >= )
        If allow_chef > by-pass perms for chef
        If allow_adjoint > by-pass perms for adjoint
        :type perms: see models.py Personnel
        """
        if not user:
            user = loginUser
        if not caserne:
            caserne = currentCaserne

        return False if not user else user.rank >= perms or (allow_admin and user == Personnel.get(id=2)) or (
                    allow_chef and user.id == caserne.chef.id) or (allow_adjoint and user.id == caserne.adjoint.id)

    @staticmethod
    def init_vars():
        global loginUser, currentCaserne, path_to_pglx, path_to_rinter, path_to_rfma, path_to_rcasernement

        if not db_is_up_to_date:
            reply = QMessageBox.question(None, "Mise à jour de la base de données.",
                                         "Pour continuer il est nécesaire de mettre à jour la base de données.\n"
                                         "Avant de poursuivre une sauvegarde de la base de données sera réalisée.\n"
                                         "Procéder à la mise à jour ?")
            if reply == QMessageBox.Yes:
                fonctions_self.migrate_databases(None)

        loginUser = Personnel.get(id=1)  # utilisateur connecté (si il existe, sinon aucun utilisateur)

        currentCaserne = Caserne.select()[0]

        path_to_pglx = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
        logger.info("PGLX installation path " + path_to_pglx)

        path_to_data = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")

        path_to_rinter = currentCaserne.pathInter
        path_to_rfma = currentCaserne.pathFma
        path_to_rcasernement = currentCaserne.pathCasernement

        if currentCaserne.pathInterByYear:
            path_to_rinter += '/' + str(datetime.date.today().year)
        if currentCaserne.pathFmaByYear:
            path_to_rfma += '/' + str(datetime.date.today().year)
        if currentCaserne.pathCasernementByYear:
            path_to_rcasernement += '/' + str(datetime.date.today().year)

    @staticmethod
    def update_vars():  # met à jour les divers variables (ex: chemins, nom caserne ...)
        global NCODISMAX, path_to_pglx, path_to_rinter, path_to_rfma, path_to_rcasernement, self_ui_inter, currentCaserne
        logger.debug("var update ...")
        currentCaserne = Caserne.select()[0]
        path_to_rinter = currentCaserne.pathInter
        path_to_rfma = currentCaserne.pathFma
        path_to_rcasernement = currentCaserne.pathCasernement

        if currentCaserne.pathInterByYear:
            path_to_rinter += '/' + str(datetime.date.today().year)

        if currentCaserne.pathFmaByYear:
            path_to_rfma += '/' + str(datetime.date.today().year)

        if currentCaserne.pathCasernementByYear:
            path_to_rcasernement += '/' + str(datetime.date.today().year)

        check_and_create_report_dir(path_to_rinter, currentCaserne.pathInterByYear)
        check_and_create_report_dir(path_to_rfma, currentCaserne.pathFmaByYear)
        check_and_create_report_dir(path_to_rcasernement, currentCaserne.pathCasernementByYear)

    @staticmethod
    def openwebbrowser(url, mode):
        webbrowser.open(url, new=mode)

    @staticmethod
    def str2bool(v):
        return v.lower() in ("yes", "true", "t", "1")

    @staticmethod
    def redigerFma(self, fma):
        global modeAccesFma, self_ui_inter
        logger.info("Création du rapport de FMA en cours ...")
        theme_fma = fma.theme
        date_debut = fma.dateDebut
        date_fin = fma.dateFin
        Name = fma.name
        n_fma = fma.nFma
        type_ = fma.type
        lieux = fma.lieu
        rapport = fma.rapport

        formateurs = ''
        for formateur in fma.autresFormateurs:
            f = formateur.split(';')
            if f[0] != '':  # s i le personnel est renseigné
                formateurs += (str(f[0]) + ' ' + f[1] + '\n')

        logger.debug("Formateurs:" + str(formateurs))

        personnel = ''
        for spv in fma.autresSpvFormes:
            f = spv.split(';')
            if f[0] != '':  # si le personnel est renseigné
                personnel += (str(f[0]) + ' ' + f[1] + '\n')

        logger.debug("Personnels formés : " + str(personnel))

        vehicules = ''  # et matériel #ATTENTION COMBOBOX
        for vehicle in fma.vehicules:
            f = vehicle.split(';')
            if f[0] != '':  # si le personnel est renseigné
                vehicules += (str(f[0]) + ' ' + f[1] + '\n')

        logger.debug("Véhicules : " + str(vehicules))

        logger.debug("Lieu(x) de formation : " + str(lieux))

        starLine = fonctions.create_line(80, "*")
        dashLine = fonctions.create_line(80, '-')

        if modeAccesFma[0] == 1:
            logger.debug('[II] écriture du rapport dans le fichier d\'origine (mode édition)')
            name = Fma.get(id=modeAccesFma[1]).name
            if not name:
                logger.warning("There is no name for the edited FMA. Generate a new one.")
                name = "Rapport_FMA-" + str(datetime.datetime.now())
        else:
            name = "Rapport_FMA-" + str(datetime.datetime.now())
            fma.name = name
            fma.save()

        try:
            fonctions.write_function(path_to_rfma, name, "N__ FMA : " + str(n_fma), '\n', starLine, '\n', name, '\n',
                                     "Formation: ", theme_fma, '\n', type_, '\n',
                                     "Date de début: ", date_debut, '\n', " Date de fin:", date_fin, '\n', dashLine,
                                     '\n',
                                     "FORMATTEURS:", '\n', formateurs, '\n', dashLine, '\n', "SPV Formés:", '\n',
                                     personnel,
                                     '\n', dashLine, '\n', "Véhicules:", '\n', vehicules, '\n', dashLine, '\n',
                                     "Lieu(x):",
                                     '\n', lieux, '\n', starLine, '\n', "RAPPORT:", '\n', rapport)

            logger.debug("Fait")
            modeAccesFma = [2, -1]
            subprocess.call(["xdg-open", os.path.join(path_to_rfma, name)])

        except Exception as e:
            QMessageBox.warning(None, "Écriture impossible", "Impossible d'écrire le rapport dans le dossier:\n" +
                                str(path_to_rfma) + "\nLog : " + str(e))
            logger.exception("Impossible d'écrire le rapport dans le dossier:\n" + str(path_to_rfma) + "\nLog : ")

        self_ui_inter.centralwidget.setEnabled(True)
        self.close()
        global self_Ui_PompierGLX

    @staticmethod
    def deleteInter(inter):
        """
        Supprime de la base de donnée le rapport d'intervention.
        ATTENTION, les liaisons (si elles existen) avec rapports perso et stats ne sont pas effacées
        """
        logger.info('[II] suppression de l\'intervention : ' + str(inter))
        q = FPTL.delete().where(FPTL.inter == inter)
        q.execute()
        q = VL.delete().where(FPTL.inter == inter)
        q.execute()
        q = VTU.delete().where(FPTL.inter == inter)
        q.execute()
        try:
            inter.renforts.delete_instance()
        except Exception as e:
            logger.exception('[EE] impossible de supprimer la relation renforts :')
            QMessageBox.critical(None, "Erreur critique", str(e))
        try:
            inter.premierDepart.delete_instance()
        except Exception as e:
            logger.exception('[EE] impossible de supprimer la relation premierDepart :')
            QMessageBox.critical(None, "Erreur critique", str(e))

        try:
            logger.info("Delete intervention report text file: " + inter.name)
            os.remove(path_to_rinter + "/" + inter.name)
        except Exception as e:
            logger.exception("[WW] Impossible de supprimer le fichier de rapport : " + str(e))
            QMessageBox.warning(None, "Erreur", "Impossible de supprimer le fichier texte du rapport :")
        inter.delete_instance()

    @staticmethod
    def deleteFma(inter):
        """
        Supprime de la base de donnée le rapport de FMA.
        ATTENTION, les liaisons (si elles existen) avec rapports perso et stats ne sont pas effacées
        """
        logger.warning('[II] suppression de la fma : ' + str(inter))
        inter.delete_instance()

    @staticmethod
    def write_function(path, name, *args, **keywords):
        logger.info("[II] Ecriture de " + str(name))

        dest_file = os.path.join(path, name)
        with open(dest_file, 'w') as file:
            for i in args:
                logger.debug("Ecrit " + str(i))
                file.write(str(i))

    @staticmethod
    def create_line(length, symbol):
        line = ""
        for i in range(0, length):
            line += str(symbol)
            logger.debug("create_line" + str(line))
        return line


def start():
    app = QtWidgets.QApplication(sys.argv)

    logger.info("------------------------------------------ Started ------------------------------------------\n")

    fonctions.init_vars()
    # fonctions.update_vars() done in MyPompierGLX
    mySW = Ui_MyPompierGLX()
    mySW.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    logger.info("------------------------------------------ Started ------------------------------------------\n")

    fonctions.update_vars()
    mySW = Ui_MyPompierGLX()
    mySW.show()
    sys.exit(app.exec_())

"""
    Pompier-GLX 4.9 Beta
    Copyright (C) 2013-2018  Sydney Rodolphe Torcuato Gems

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
